@echo off
cls
echo Select platform:
echo  1. PC
echo  2. Android
echo  3. iOS
echo.

:select
set /p INPUT=Enter number (negate for pack): 

set FMT=PVRTC1_4_RGB
set PACK=..\src\ios\data.xpk
if /I '%INPUT%'=='3' goto :convert
if /I '%INPUT%'=='-3' goto :pack

set FMT=ETC1
set PACK=..\src\android\res\raw\data.jet
if /I '%INPUT%'=='2' goto :convert
if /I '%INPUT%'=='-2' goto :pack

set FMT=BC1
set PACK=data.xpk
if /I '%INPUT%'=='1' goto :convert
if /I '%INPUT%'=='-1' goto :pack

goto :pack

:convert
set CONV=PVRTexToolCLI -m -mfilter cubic -flip y
set CONV_RGBA=%CONV% -f R8G8B8A8
set CONV_ALPHA=%CONV% -f A8
set CONV_COMP=%CONV% -f %FMT%

set IN_RGBA=conv\rgba\
set IN_ALPHA=conv\alpha\
set IN_COMP=conv\comp\
set OUT=data\

rem -i%IN_RGBA%%%~nxi -o%OUT%%%~ni

echo.
echo convert to RGBA:
for /r %IN_RGBA% %%i in (*.tga, *.png) do %CONV_RGBA% -i %%i -o %OUT%%%~ni.pvr

echo.
echo convert to ALPHA:
for /r %IN_ALPHA% %%i in (*.tga, *.png) do %CONV_ALPHA% -i %%i -o %OUT%%%~ni.pvr

echo.
echo convert to %FMT%:
for /r %IN_COMP% %%i in (*.tga, *.png) do %CONV_COMP% -i %%i -o %OUT%%%~ni.pvr

:pack
echo.
xpack data/ %PACK%
echo.
