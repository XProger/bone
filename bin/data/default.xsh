#ifndef MODE_SHADOW
	varying vec3 vViewVec;
	varying vec3 vNormal;
	varying vec4 vTangent;
	varying vec3 vLightVec;	

	varying vec4 vPos;		
	#ifdef MAT_SHADOW_RECV
		varying	vec4 vLightProj;
	#endif
#endif

#if !defined(MODE_SHADOW) || defined(MAT_OPACITY)
	varying vec4 vTexCoord;
#endif

#ifdef VERTEX
	attribute vec4 aCoord;
	#if defined(MAT_SKINNING) || !defined(MODE_SHADOW)	// aNormal.w = joint weight
		attribute vec4 aNormal;
		attribute vec4 aTangent;		
	#endif
	
	#ifndef MODE_SHADOW
		uniform vec3 uViewPos;
		uniform vec3 uLightPos;		
		#ifdef MAT_SHADOW_RECV
			uniform mat4 uLightViewProj;
		#endif
	#endif

	#if !defined(MODE_SHADOW) || defined(MAT_OPACITY)	
		#ifndef MAT_ENV_SPHERE
			attribute vec4	aTexCoord;
			
			uniform vec4	uTexScale;
			uniform vec4	uTexBias;
		#endif
	#endif
	
	uniform mat4 uViewProj;
	uniform mat4 uModel;
	uniform vec3 uGeomScale;
	uniform vec3 uGeomBias;

	#ifdef MAT_SKINNING
		uniform vec4 uJoint[80 * 2];
		
		vec3 quatMul(in vec4 q, in vec3 v) { 
			return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + v * q.w);
		}

		vec3 dquatMul(in vec4 real, in vec4 dual, in vec3 v) {
			return 2.0 * (dual.xyz * real.w - real.xyz * dual.w + cross(real.xyz, dual.xyz)) + quatMul(real, v);
		}
	#endif

	void main() {
		#ifndef MODE_SHADOW
			vec3 rNormal = aNormal.xyz * 2.0 - 1.0;
			vec4 rTangent = aTangent * 2.0 - 1.0;			
		#endif
		
		vec4 rCoord = vec4(aCoord.xyz * uGeomScale + uGeomBias, 1.0);

		#ifdef MAT_SKINNING
			vec2 rWeight = vec2(aNormal.w, 1.0 - aNormal.w);
			
			float j = aCoord.w / 256.0;
			ivec2 rJoint = ivec2(j, fract(j) * 256.0);		
			
			vec4 r0 = uJoint[rJoint.x];
			vec4 d0 = uJoint[rJoint.x + 1];
			vec4 r1 = uJoint[rJoint.y];
			vec4 d1 = uJoint[rJoint.y + 1];
			
			float d = dot(r0.xyz, r1.xyz) + r0.w * r1.w; // Adreno 330 BUG dot(vec4, vec4) == 0.0
			if (d < 0.0) rWeight.y *= -1.0;
			vec4 real = r0 * rWeight.x + r1 * rWeight.y;
			vec4 dual = d0 * rWeight.x + d1 * rWeight.y;
					
			float len = 1.0 / length(real);
			real *= len;
			dual *= len;
		
			rCoord.xyz = dquatMul(real, dual, rCoord.xyz);
			
			#ifndef MODE_SHADOW
				rNormal = quatMul(real, rNormal);
				rTangent = vec4(quatMul(real, rTangent.xyz), rTangent.w);
			#endif
		#endif
		
		rCoord		= uModel * rCoord;
		gl_Position	= uViewProj * rCoord;
		
		#ifndef MODE_SHADOW
			vPos = rCoord;
			
			mat3 rModel = mat3(uModel[0].xyz, uModel[1].xyz, uModel[2].xyz);
			vNormal		= rModel * rNormal;
			vTangent	= vec4(rModel * rTangent.xyz, rTangent.w);
			
			vLightVec	= uLightPos - rCoord.xyz;
			vViewVec	= rCoord.xyz - uViewPos;
			#ifdef MAT_SHADOW_RECV
				vLightProj	= uLightViewProj * rCoord;
				//vLightProj.z -= 0.0025 * vLightProj.w;
				vLightProj.z -= 0.001 * vLightProj.w;
			#endif

			#ifdef MAT_ENV_SPHERE
				vec3 r = reflect(normalize(vViewVec), normalize(vNormal));
				vec3 p = vec3(r.x, r.y, 1.0 - r.z);
				vTexCoord = vec4(p.xy / (length(p) * 2.0) + 0.5, 0.0, 0.0);
			/*
				float oneMRZ = 1.0f - r.z;	// TODO
				float invLength = 1.0f / sqrt(r.x * r.x + r.y * r.y + oneMRZ * oneMRZ);
				vTexCoord = 0.5f * (r.xy * invLength + 1.0f);
			*/
			#else
				vTexCoord = aTexCoord * uTexScale + uTexBias;
			#endif
		#else
			#ifdef MAT_OPACITY
				vTexCoord = aTexCoord * uTexScale + uTexBias;
			#endif
		#endif
	}
#endif

#ifdef FRAGMENT
	#if !defined(MODE_SHADOW) || defined(MAT_OPACITY)
		#ifdef MAT_DIFFUSE
			uniform vec4 uDiffuse;
		#else
			uniform sampler2D sDiffuse;
		#endif
	#endif
	
	#ifdef MAT_NORMAL	
		uniform sampler2D sNormal;
	#endif
	
	#ifdef MAT_LIGHT
		uniform sampler2D sLight;
	#endif
	
	#ifndef MODE_SHADOW
		uniform vec3 uLightDir;
		uniform vec3 uAmbient;
	#endif	
	
	#ifdef MODE_SHADOW
		#ifdef SHADOW_COLOR
			vec4 pack(in float value) {
				vec4 bitSh = vec4(256.0*256.0*256.0, 256.0*256.0, 256.0, 1.0);
				vec4 bitMsk = vec4(0.0, 1.0/256.0, 1.0/256.0, 1.0/256.0);
				vec4 res = fract(value * bitSh);
				res -= res.xxyz * bitMsk;
				return res;
			}
		#endif
	#elif defined(MAT_SHADOW_RECV)
		#ifdef SHADOW_SAMPLER
			uniform sampler2DShadow sShadow;
			#ifdef MOBILE
				#define SHADOW(V) (shadow2DEXT(sShadow, V))				
			#else
				#define SHADOW(V) (shadow2D(sShadow, V).x)
			#endif
		#else
			uniform sampler2D sShadow;
			#define CMP(a,b) float(a > b)
			//#define CMP(a,b) clamp((a - b) * 1000000.0, 0.0, 1.0)
			#ifdef SHADOW_DEPTH
				#define SHADOW(V) CMP(texture2D(sShadow, (V).xy).x, p.z)
			#elif defined(SHADOW_COLOR)
				float unpack(vec4 value) {
					vec4 bitSh = vec4(1.0/(256.0*256.0*256.0), 1.0/(256.0*256.0), 1.0/256.0, 1.0);
					return dot(value, bitSh);
				}
				#define SHADOW(V) CMP(unpack(texture2D(sShadow, (V).xy)), p.z)				
			#endif
		#endif
	
		float getShadow(in vec4 lightProj) {
			vec3 p = lightProj.xyz / lightProj.w;
		//	return SHADOW(p);
			const float tx = 1.0 / 2048.0;
			vec3 dx = vec3(tx, 0.0, 0.0);
			vec3 dy = vec3(0.0, tx, 0.0);
			vec3 dxdyp = dx + dy;
			vec3 dxdyn = dx - dy;
			float rShadow = 
				SHADOW(p + dx) +
				SHADOW(p - dx) + 
				SHADOW(p + dy) + 
				SHADOW(p - dy) + 
				SHADOW(p + dxdyp) + 
				SHADOW(p - dxdyp) + 
				SHADOW(p + dxdyn) + 
				SHADOW(p - dxdyn);
			return rShadow * 0.125;
		}
	#endif

	void main() {
		#ifdef MODE_SHADOW
			#ifdef MAT_OPACITY
				#ifndef MAT_DIFFUSE
					if (texture2D(sDiffuse, vTexCoord.xy).a < 0.25)
						discard;
				#endif
			#endif
			
			#ifdef SHADOW_COLOR
				gl_FragColor = pack(gl_FragCoord.z);
			#else
				gl_FragColor = vec4(1.0);
			#endif
		#else
			#ifdef MAT_DIFFUSE
				vec4 color = uDiffuse;
			#else
				vec4 color = texture2D(sDiffuse, vTexCoord.xy);
				#ifdef MAT_OPACITY
					if (color.a < 0.25)
						discard;
				#endif
				
				color.xyz = pow(color.xyz, vec3(2.2));

				#ifdef MAT_LIGHT
					//color.xyz *= texture2D(sLight, vTexCoord.zw).xyz;
					//#ifdef MAT_SHADOW_RECV
					//	color.xyz *= min(vec3(1.0), getShadow(vLightProj) + uAmbient);
					//#endif
				#endif
			#endif

			float rSpecular = 0.0;
			vec3 rNormal = normalize(vNormal);
			#ifdef MAT_NORMAL
				vec3 rTangent = normalize(vTangent.xyz);
				vec3 rBinormal = normalize(cross(rNormal, rTangent) * vTangent.w);
				vec3 t = texture2D(sNormal, vTexCoord.xy).xyz;
				t.xy = t.xy * 2.0 - 1.0;
				rSpecular = t.z;
				vec3 tNormal = vec3(t.x, t.y, sqrt(1.0 - t.x * t.x - t.y * t.y));
				rNormal = normalize( tNormal.x * rTangent + tNormal.y * rBinormal + tNormal.z * rNormal );
			//	rNormal = normalize(mat3(rTangent, rBinormal, rNormal) * tNormal);
			#endif
			
			#ifdef MAT_TWO_SIDE
				if (!gl_FrontFacing)
					rNormal *= -1.0;
			#endif
			
			#ifndef MODE_DEFERRED
				vec3 rLightVec = normalize(vLightVec);
				
				//#ifndef MAT_LIGHT
					//float lum = dot(rNormal, uLightDir);
					float lum = dot(rNormal, rLightVec);
					#ifdef MAT_SHADOW_RECV
						lum = min(getShadow(vLightProj), lum);
					#endif
					color.xyz *= max(0.0, lum) + uAmbient;
					//color.xyz *= max(0.0, lum);
				//#endif
				
				vec3 r = reflect(normalize(vViewVec), rNormal);
	//			color.xyz *= dot(rNormal, -normalize(vViewVec)) * 0.5 + 0.5;
				color.xyz += pow(max(0.0, dot(r, rLightVec)), 64.0) * rSpecular;
			/*	
				vec3 fogColor = uAmbient;
				float fog = gl_FragCoord.z / gl_FragCoord.w;
				fog = 1.0 / exp(fog * 0.02);
				fog = clamp(fog, 0.0, 1.0);
				color.xyz = mix(fogColor, color.xyz, fog);
			*/
				color.xyz = pow(color.xyz, vec3(1.0/2.2));
				gl_FragColor = color;//vec4(rNormal * 0.5 + 0.5, 1.0);//
			#else
				#ifdef MAT_DIFFUSE
					color.w = 1.0;
				#else
					color.w = 0.0;
				#endif
				gl_FragData[0] = color;
				gl_FragData[1] = vec4(rNormal * 0.5 + 0.5, rSpecular);
			#endif
		#endif
	}
#endif
