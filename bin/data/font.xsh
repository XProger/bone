varying vec2 vTexCoord;

#ifdef VERTEX
	attribute vec2 aCoord;
	attribute vec2 aTexCoord;
	uniform mat4 uViewProj;

	void main() {
		vTexCoord = aTexCoord;
		vec4 rCoord = vec4(aCoord.xy, 0.0, 1.0);
		gl_Position	= uViewProj * rCoord;		
	}
#endif

#ifdef FRAGMENT
	uniform sampler2D sDiffuse;
	uniform vec4 uDiffuse;

	void main() {
		gl_FragColor = vec4(vec3(1.0), texture2D(sDiffuse, vTexCoord).w) * uDiffuse;
	}
#endif
