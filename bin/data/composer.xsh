#extension GL_ARB_texture_rectangle : enable

varying vec3 vViewVec;

#ifdef VERTEX
	attribute vec4 aCoord;
	attribute vec4 aTexCoord;
	
	void main() {
		vViewVec = aCoord.xyz;
		gl_Position = vec4(aTexCoord.xy, 0.0, 1.0);
	}
#else
	uniform sampler2D sDiffuse;
	uniform sampler2D sNormal;
	uniform sampler2D sDepth;
	
	uniform mat4 uViewProj;
	uniform mat4 uLightViewProj;	// inverse uViewProj
	
	uniform vec3 uViewPos;
	uniform vec3 uLightPos;	
	uniform vec3 uProjRatio;	
	
	vec3 applyLight(vec4 diffuse, vec4 normal, vec3 pos) {
	// lambert
		vec3 rLightVec = uLightPos - pos;
		float rLightAtt = dot(rLightVec, rLightVec);
		rLightAtt = clamp(1.0 -  rLightAtt / (40.0 * 40.0), 0.0, 1.0);	
		rLightVec = normalize(rLightVec) * rLightAtt;

		float lum = max(0.0, dot(normal.xyz, rLightVec));
		vec3 color = diffuse.xyz * max(lum, diffuse.w);
	// specular
		vec3 rViewVec = normalize(pos - uViewPos);
		vec3 r = reflect(rViewVec, normal.xyz);
		color += pow(max(0.0, dot(r, rLightVec)), 64.0) * normal.w;
		return color;
	}
	
	float toLinearDepth(float depth) {
		return 1.0 / (uProjRatio.x * depth + uProjRatio.y);
	}
	
	vec3 toWorld(vec3 pos) {	
		vec4 p = uLightViewProj * vec4(pos, 1.0);
		return p.xyz / p.w;
	}
	
	vec3 toScreen(vec3 pos) {
		vec4 p = uViewProj * vec4(pos, 1.0);
		return p.xyz / p.w;
	}
	
	vec3 SSLR(vec3 pos, vec3 dir) {
		float dist = 1.0, ref;
		vec3 uv, p = pos;
		float step = 0.1;
		
		vec3 st = vec3(0.0);
		
		for (int i = 0; i < 10; i++) {
			p = pos + dir * dist;
			uv = toScreen(p);
			ref = uv.z;
			uv.z = texture2D(sDepth, uv.xy * 0.5 + 0.5).x * 2.0 - 1.0;
			dist = length(pos - toWorld(uv));
		}
		
		p = pos + dir * dist;
		uv = toScreen(p);	
		
		const float f = 0.75;
		float fade = 1.0;
		fade *= smoothstep(-1.0, -f, uv.x);			// left
		fade *= smoothstep(-1.0, -f, uv.y);			// top
		fade *= 1.0 - smoothstep(f, 1.0, uv.x);		// right
		fade *= 1.0 - smoothstep(f, 1.0, uv.y);		// bottom		
		
		uv = uv * 0.5 + 0.5;
		
		vec4 d = texture2D(sDiffuse, uv.xy);		
		vec4 n = texture2D(sNormal, uv.xy);
		n.xyz = normalize(n.xyz * 2.0 - 1.0);
		
		return applyLight(d, n, p) * fade;
	}
	
	void main() {
		vec2 uv = gl_FragCoord.xy / vec2(1280.0, 720.0);
		
		vec4 rDiffuse	= texture2D(sDiffuse, uv.xy);
		vec4 rNormal	= texture2D(sNormal, uv.xy);
		float rDepth	= texture2D(sDepth, uv.xy).x;
		
		rNormal.xyz = normalize(rNormal.xyz * 2.0 - 1.0);
		
	// world space position
		vec3 rPos = toWorld(vec3(uv, rDepth) * 2.0 - 1.0);
	// alternative
	//	rDepth	= 1.0 / (uProjRatio.x * (rDepth * 2.0 - 1.0) + uProjRatio.y);
	//	vec3 rPos	= uViewPos + vViewVec * rDepth;

		vec3 color = applyLight(rDiffuse, rNormal, rPos);
	
	// SSLR
		vec3 rViewVec = normalize(rPos - uViewPos);
		vec3 r = reflect(rViewVec, rNormal.xyz);
			
		float fresnel = 1.0 + dot(rViewVec, rNormal.xyz);
		color += SSLR(rPos, r) * (fresnel * rNormal.w);
		
	// gamma	
		color = pow(color.xyz, vec3(1.0/2.2));
	//vec3 p = vec3(gl_FragCoord.xy / vec2(1280.0, 720.0), rDepth) * 2.0 - 1.0;
	//vec3 p = toScreen(rPos);
	
		gl_FragColor = vec4(color, 1.0);
	}
#endif