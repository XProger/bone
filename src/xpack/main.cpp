#ifdef _DEBUG
	#include "crtdbg.h"
#endif

#include <stdio.h>
#include <windows.h>
#include <lzo/lzoconf.h>
#include <lzo/lzo1x.h>
/*
#include "pvr/PVRTexture.h"
#include "pvr/PVRTextureUtilities.h"

using namespace pvrtexture;
*/
#define IMB			1.0f/(1024*1024)
#define BAR_WIDTH	62

void progress(int x, int n, int size) {
	float p = x / (float)n;
	int   c = int(p * BAR_WIDTH);
	printf("\r %3d%% ", int(p * 100));
	for (int i = 0; i < BAR_WIDTH; i++) 
		printf("%c", char(i < c ? 219 : 196));
	printf(" %.2f mb", size * IMB);
}

struct FileItem {
	FileItem *next;
	char name[64];
	unsigned long size, csize, offset;
};

FileItem *getFiles(const char *dir) {
	char path[255];
	strcpy(path, dir);
	strcat(path, "*");	// all files in directory

	FileItem *files = NULL;

	WIN32_FIND_DATA fd;
	HANDLE h = FindFirstFile(path, &fd);
	if (h != INVALID_HANDLE_VALUE) {
		do {
			if ((fd.dwFileAttributes & (FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_DIRECTORY)))
				continue;
			FileItem *item = new FileItem();
			strcpy(item->name, fd.cFileName);
			item->size = fd.nFileSizeLow;
			item->next = files;
			files = item;
		} while (FindNextFile(h, &fd));
		FindClose(h);
	}
	return files;
}

unsigned char* compress(unsigned char *data, unsigned long size, unsigned long &csize, void *wmem) {
	csize = size + size / 16 + 64 + 3;
	unsigned char *cdata = new unsigned char[csize];
	lzo1x_999_compress(data, size, cdata, &csize, wmem);
	return cdata;
}

void packFiles(FileItem *files, const char *dir, const char *path) {
	int count, size, csize, cur, hsize = sizeof(count);

	count = size = csize = cur = 0;

	FileItem *item = files;
	while (item) {
		count++;
		hsize += sizeof(item->size) + sizeof(item->csize) + sizeof(item->offset) + (strlen(item->name) + 1);
		size  += item->size;
		item = item->next;
	}

	printf("compressing...\n");
	lzo_init();
	char *wmem = new char[LZO1X_999_MEM_COMPRESS];

	FILE *fout = fopen(path, "wb");
	fseek(fout, hsize, SEEK_SET);

	char name[255];
	item = files;
	while (item) {
		strcpy(name, dir);
		strcat(name, item->name);
	// input
		FILE *fin = fopen(name, "rb");
		unsigned char *data = new unsigned char[item->size];
		fread(data, 1, item->size, fin);
		fclose(fin);
	// output
		unsigned char *cdata = compress(data, item->size, item->csize, wmem);

		if (item->csize >= item->size) { // save uncompressed
			item->csize = item->size;
			fwrite(data, 1, item->size, fout);
		} else
			fwrite(cdata, 1, item->csize, fout);

		delete cdata;
		delete data;

		item->offset = csize + hsize;

		cur   += item->size;
		csize += item->csize;

		progress(cur, size, csize);
		item = item->next;
	}
	delete wmem;

	printf("\n%.2f mb -> %.2f mb (%d%%)\n", size * IMB, csize * IMB, int(csize * 100.0f / size));

// output file table
	fseek(fout, 0, SEEK_SET);
	fwrite(&count, 1, sizeof(count), fout);
	item = files;
	while (item) {
		fwrite(&item->size, 1, sizeof(item->size), fout);
		fwrite(&item->csize, 1, sizeof(item->csize), fout);
		fwrite(&item->offset, 1, sizeof(item->offset), fout);
		
		unsigned char len = strlen(item->name);
		fwrite(&len, 1, sizeof(len), fout);
		fwrite(item->name, 1, len, fout);

		files = item;
		item = item->next;
		delete files;
	}
	fclose(fout);
}

/*
const size_t CPVRTString::npos = -1;

void convert(const char *from, const char *to) {
	// Open and reads a pvr texture from the file location specified by filePath
	CPVRTexture cTexture(from);
	Flip(cTexture, ePVRTAxisY);
	GenerateMIPMaps(cTexture, eResizeLinear);
	PixelType ptFormat (ePVRTPF_DXT1); //(ePVRTPF_PVRTCI_4bpp_RGB);
	Transcode(cTexture, ptFormat, ePVRTVarTypeUnsignedInteger, ePVRTCSpacelRGB);
	// Save the file
	if (!cTexture.saveFile(to))
		printf("error!\n");
}
*/

int main(int argc, char *argv[]) {
//	convert("grid.pvr", "out.pvr");
#ifdef _DEBUG
	_CrtMemState _ms;
	_CrtMemCheckpoint(&_ms);
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
#endif

	if (argc < 3) {
		printf("use: xpack [input dir] [output_file]\n");
		return 0;
	}

	char *in = argv[1];
	char *out = argv[2];

	FileItem *files = getFiles(in);
	if (files == NULL) {
		printf("no files found\n");
		return 0;
	}

	packFiles(files, in, out);

#ifdef _DEBUG
	_CrtMemDumpAllObjectsSince(&_ms);
	system("pause");
#endif

	return 0;
}