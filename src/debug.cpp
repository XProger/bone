#include "debug.h"

namespace Debug {

	namespace Draw {

		void begin() {
			glMatrixMode(GL_PROJECTION);
			glLoadMatrixf((GLfloat*)&Core::mProj);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glLoadMatrixf((GLfloat*)&Core::mView);

			glLineWidth(3);
			glPointSize(32);

			glUseProgram(0);
			Core::active.shader = NULL;
		}

		void end() {
			//
		}

		void box(const Box &box) {
			const vec3 &min = box.min;
			const vec3 &max = box.max;
			glBegin(GL_LINES);
				glVertex3f(min.x, min.y, min.z);
				glVertex3f(max.x, min.y, min.z);
				glVertex3f(min.x, max.y, min.z);
				glVertex3f(max.x, max.y, min.z);

				glVertex3f(min.x, min.y, max.z);
				glVertex3f(max.x, min.y, max.z);
				glVertex3f(min.x, max.y, max.z);
				glVertex3f(max.x, max.y, max.z);

				glVertex3f(min.x, min.y, min.z);
				glVertex3f(min.x, min.y, max.z);
				glVertex3f(min.x, max.y, min.z);
				glVertex3f(min.x, max.y, max.z);

				glVertex3f(max.x, min.y, min.z);
				glVertex3f(max.x, min.y, max.z);
				glVertex3f(max.x, max.y, min.z);
				glVertex3f(max.x, max.y, max.z);

				glVertex3f(min.x, min.y, min.z);
				glVertex3f(min.x, max.y, min.z);

				glVertex3f(max.x, min.y, min.z);
				glVertex3f(max.x, max.y, min.z);
				glVertex3f(min.x, min.y, min.z);
				glVertex3f(min.x, max.y, min.z);

				glVertex3f(max.x, min.y, max.z);
				glVertex3f(max.x, max.y, max.z);
				glVertex3f(min.x, min.y, max.z);
				glVertex3f(min.x, max.y, max.z);
			glEnd();
		}

		void sphere(const Sphere &sphere) {
			const float k = PI * 2.0f / 18.0f;

			glBegin(GL_LINE_STRIP);
			for (int j = 0; j < 3; j++)
				for (int i = 0; i < 19; i++) {
					vec3 p = vec3(sinf(i * k), cosf(i * k), 0.0f) * sphere.radius + sphere.center;
					glVertex3f(p[j], p[(j + 1) % 3], p[(j + 2) % 3]);
				}
			glEnd();
		}

		void mesh(vec3 *vertices, Index *indices, int iCount) {
			glBegin(GL_LINES);
			for (int i = 0; i < iCount; i += 3) {
				vec3 &a = vertices[indices[i + 0]];
				vec3 &b = vertices[indices[i + 1]];
				vec3 &c = vertices[indices[i + 2]];
				glVertex3fv((GLfloat*)&a);
				glVertex3fv((GLfloat*)&b);

				glVertex3fv((GLfloat*)&b);
				glVertex3fv((GLfloat*)&c);

				glVertex3fv((GLfloat*)&c);
				glVertex3fv((GLfloat*)&a);
			}
			glEnd();
		}

		void curve(Curve *curve) {
			glBegin(GL_LINE_STRIP);
				for (int i = 0; i < curve->count - 1; i += 3) {
					for (int j = 0; j < 20; j++) {
						float t = j * (1.0f / 19.0f);
						vec3 p = curve->eval(i, t);
						glVertex3f(p.x, p.y, p.z);
					}
				}
			glEnd();
		}

		void axes(float size) {
			glBegin(GL_LINES);
				glColor3f(1, 0, 0); glVertex3f(0, 0, 0); glVertex3f(size,    0,    0);
				glColor3f(0, 1, 0); glVertex3f(0, 0, 0); glVertex3f(   0, size,    0);
				glColor3f(0, 0, 1); glVertex3f(0, 0, 0); glVertex3f(   0,    0, size);
			glEnd();
		}

		void point(const vec3 &p, const vec3 &color) {
			glColor3f(color.x, color.y, color.z);
			glBegin(GL_POINTS);
				glVertex3f(p.x, p.y, p.z);
			glEnd();
		}

		void test1() {
			glDisable(GL_DEPTH_TEST);
			glMatrixMode(GL_PROJECTION);
			glPushMatrix();
			glLoadIdentity();
			glOrtho(0.0, Core::width, Core::height, 0.0, 0.0, 1.0);
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadIdentity();

			vec3 p = vec3(Input::mouse.pos.x, Input::mouse.pos.y, 0.0f);

			Curve *c = new Curve();
			c->count = 7;
			c->points = new vec3[c->count];
			
			c->points[0] = vec3(100.0f, Core::height - 100.0f, 0.0f);
			c->points[1] = vec3(100.0f, 100.0f, 0.0f);

			c->points[2] = vec3(Core::width * 0.5f, 100.0f + 100.0f, 0.0f);
			
			c->points[3] = vec3(Core::width * 0.5f, 100.0f, 0.0f);
			c->points[4] = vec3(Core::width * 0.5f, 100.0f - 100.0f, 0.0f);

			c->points[5] = vec3(Core::width * 0.75f , 100.0f, 0.0f);
			c->points[6] = vec3(Core::width - 100.0f, Core::height * 0.75f, 0.0f);

			glColor3f(1, 0, 0);
			curve(c);

			glPointSize(16.0f);
			float t = c->closest(p);
			vec3 q = c->eval(t);
		//	LOG("c: %f\n", t);

		//	5.0 * 6382095.0 * x^4 - 4.0f * 14474640 * x^3 + 3.0 * 11798742 * x^2 + 2.0 * 4014903.0 * x + 521415.0 = 0

//			float e[5];
//			int eCount = solve(6382095.0f, -14474640.0f, 11798742.0f, -4014903.0f, 521415.0f, -22587.0f, 64, 0.0001f, e);
//			int eCount = solve(588000.0f, -1596000.0f, 2395200.0f, -67500.0f, -1612560.0f, 160200.0f, 64, 0.0001f, e);
//			LOG("\n");
//			int eCount = solve(6382095.0f, -14474640.0f, 11798742.0f, -3816990.0f, 181107.0f, 65610.0f, 64, 0.001f, e);
	//*x^5-*x^4+*x^3-*x^2-*x+=0
//			LOG("%d = [ ", eCount);
//			for (int i = 0; i < eCount; i++)
//				LOG("%f ", e[i]);
//			LOG("]\n");

//			LOG("%f %f %f %f %f\n", e[0], e[1], e[2], e[3], e[4]);
//			0.033727 -0.240043 0.728303 4.104264 3.310639
//			x = 0.464457

			glPointSize(8.0f);
			point(p, vec3(1.0f, 0.0f, 0.0f));
			point(q, vec3(0.0f, 0.0f, 0.0f));

			delete c;

			glMatrixMode(GL_PROJECTION);
			glPopMatrix();
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
			glEnable(GL_DEPTH_TEST);
		}
	}
}