#include "utils.h"

float _min(float a, float b) { return a < b ? a : b; };

float _max(float a, float b) { return a > b ? a : b; }

float sign(float x) { 
	return fabsf(x) < EPS ? 0.0f : (x < 0.0f ? -1.0f : 1.0f);
//	return x < 0.0f ? -1.0f : (x > 0.0f ? 1.0f : 0.0f); 
}
float hsign(float x) { 
	return fabsf(x) < EPS ? 0.0f : (x < 0.0f ? -0.5f : 0.5f);
//	return x < 0.0f ? -1.0f : (x > 0.0f ? 1.0f : 0.0f); 
}
float clamp(float x, float a, float b) { return x < a ? a : (x > b ? b : x); }

int _min(int a, int b) { return a < b ? a : b; }
int _max(int a, int b) { return a > b ? a : b; }
int sign(int x) { return x < 0.0f ? -1 : (x > 0 ? 1 : 0); }
int clamp(int x, int a, int b) { return x < a ? a : (x > b ? b : x); }

float power(float x, float p) {
	return x > 0.0f ? expf(logf(x) * p) : (x < 0.0f ? -expf(logf(-x) * p) : 0.0f);
}

float clampAngle(float a) {
	return a < -PI ? a + PI2 : (a >= PI ? a - PI2 : a);
}

float shortAngle(float a, float b) {
	float n = b - a;
	n -= int(n / PI2) * PI2;
	return clampAngle(n);
}

float lerpAngle(float a, float b, float t) {
	return clampAngle(a + shortAngle(a, b) * t);
}

int int_floor(float x) {
	int i = (int)x;
	return i - (i > x);
}

int swap(int x) {
	// bswap eax
	return	((x & 0xFF) << 24) | (((x >> 8) & 0xFF) << 16) | (((x >> 16) & 0xFF) << 8) | ((x >> 24) & 0xFF);
}

void swap(int &a, int &b) {
	int t = a;
	a = b;
	b = t;
}

void swap(float &a, float &b) {
	float t = a;
	a = b;
	b = t;
}

float lerp(float a, float b, float t) {
	if (t <= 0.0f) return a;
	if (t >= 1.0f) return b;
	if (a == b) return a;
	return a + (b - a) * t;
}

int solve(float a, float b, float *x) {
	if (fzero(a))
		return 0;
	x[0] = -b / a;
	return 1;
}

int solve(float a, float b, float c, float *x) {
	if (fzero(a))
		return solve(b, c, x);
	b /= a; if (fzero(b)) b = 0.0f;
	c /= a; if (fzero(c)) c = 0.0f;

	float d = b * b - 4 * c;
	if (fzero(d)) {
		x[0] = -b * 0.5f;
		return 1;
	}
	if (d > 0.0f) {
		d = sqrtf(d);
		x[0] = -(d + b) * 0.5f;
		x[1] =  (d - b) * 0.5f;
		return 2;
	}
	return 0;
}

int solve(float a, float b, float c, float d, float *x) {
	if (fzero(a))
		return solve(b, c, d, x);
	b /= a; if (fzero(b)) b = 0.0f;
	c /= a; if (fzero(c)) c = 0.0f;
	d /= a; if (fzero(d)) d = 0.0f;

	float p = c - b * b / 3.0f;
	float q = b * b * b * (2.0f / 27.0f) - b * c / 3.0f + d;
	float r = q * q / 4.0f + p * p * p / 27.0f;

	float t;
	if (r >= 0.0f) {
		r = sqrtf(r);
		t = power(-q * 0.5f + r, 1.0f / 3.0f) + power(-q * 0.5f - r, 1.0f / 3.0f) - b / 3.0f;
	} else
		t = 2.0f * sqrtf(-p / 3.0f) * cosf(atan2(-q * 0.5f, sqrtf(fabsf(r))) / 3.0f) - b / 3.0f;

	int i = solve(1, t + b, t * (t + b) + c, x);
	if (i == 0 || (i == 1 && x[0] != t) || (i == 2 && x[0] != t && x[1] != t))
		x[i++] = t;
	return i;
}

int solve(float a, float b, float c, float d, float e, float *x) {
	if (fzero(a))
		return solve(b, c, d, e, x);
	b /= a; if (fzero(b)) b = 0.0f;
	c /= a; if (fzero(c)) c = 0.0f;
	d /= a; if (fzero(d)) d = 0.0f;
	e /= a; if (fzero(e)) e = 0.0f;

	solve(1.0f, -c, b * d - 4.0f * e, (4 * c - b * b) * e - d * d, x);
	float cs = x[0], cs2 = cs * 0.5f;

	float m = b * b * 0.25f - c + cs;
	float n = b * cs2 - d;
	float k = cs2 * cs2 - e;
	if (fzero(m)) m = 0.0f;
	if (fzero(n)) k = 0.0f;
	if (fzero(k)) k = 0.0f;

	if (m >= 0.0f && k >= 0.0f) {
		m = sqrtf(m);
		k = sqrtf(k);
		float c1, c2;
		c1 = b * 0.5f - m;
		c2 = cs2 + (n > 0.0f ? -k : k);
		int i = solve(1, c1, c2, x);

		c1 = b * 0.5f + m;
		c2 = cs2 + (n > 0.0f ? k : -k);

		return i + solve(1, c1, c2, &x[i]);
	}
	return 0;
}

float equation4(float a, float b, float c, float d, float e, float x) {
	float x2 = x * x;
	return a * x2 * x2 + b * x2 * x + c * x2 + d * x + e;
}

float equation5(float a, float b, float c, float d, float e, float f, float x) {
	float x2 = x * x;
	float x3 = x2 * x;
	return a * x2 * x3 + b * x2 * x2 + c * x3 + d * x2 + e * x + f;
}


float signEps(float a, float eps) {
	if (fabsf(a) < eps) 
		return 0;

}

int solve(float a, float b, float c, float d, float e, float f, int steps, float epsilon, float *x) {
	float ext[7];
	int eCount = solve(5.0f * a, 4.0f * b, 3.0f * c, 2.0f * d, e, &ext[0]);
	eCount += solve(20.0f * a, 12.0f * b, 6.0f * c, 2.0f * d, &ext[eCount]);

	if (eCount == 0)
		return 0;

	if (eCount > 1)
		if (eCount == 2) {
			if (ext[0] > ext[1])
				swap(ext[0], ext[1]);
		} else
			for (int i = 0; i < eCount - 1; i++) {
				bool swapped = false;
				for (int j = 0; j < eCount - i - 1; j++)
					if (ext[j] > ext[j + 1]) {
						swap(ext[j], ext[j + 1]);
						swapped = true;
					}
				if (!swapped)
					break;
			}
		
	int rCount = 0;
	float left = 0.0f, right;

	for (int i = 0; i <= eCount; i++) {
		right = i < eCount ? _min(ext[i], 1.0f) : 1.0f;
		if (right < 0.0f)
			continue;

		float L = left;
		float R = right;

		float fL = equation5(a, b, c, d, e, f, L);
		float fR = equation5(a, b, c, d, e, f, R);

		if (sign(fL) != sign(fR)) {
			int itr = 0;
			while (fabsf(R - L) > epsilon) {
				itr++;

				float t = (L + R) * 0.5f;
				float q = equation5(a, b, c, d, e, f, t);
				if (sign(q) == sign(fL)) {
					L = t;
					fL = q;
				} else {
					R = t;
					fR = q;
				}

			/*
				L -= (R - L) * fL / (fR - fL);
				fL = equation5(a, b, c, d, e, f, L);
				R -= (L - R) * fR / (fL - fR);
				fR = equation5(a, b, c, d, e, f, R);
			*/
			}
	
			LOG("%d\n", itr);
			x[rCount++] = R;
		}


		if ( (left = right) >= 1.0f)
			break;
	}

	return rCount;
}

/*
// Newton 5th degree equation solver
int solve(float a, float b, float c, float d, float e, float f, int steps, float epsilon, float *x) {
	int rCount = 0;

	for (int j = 0; j < 5; j++) {
		float t = 0.5f;

		for (int i = 0; i < steps; i++) {
			float fx = equation5(a, b, c, d, e, f, t);								// f(t)
			float fd = equation4(5.0f * a, 4.0f * b, 3.0f * c, 2.0f * d, e, t);		// f'(t)
		
			float s = 1.0f;
			switch (j) {
				case 4 : s *= (t - x[3]);
				case 3 : s *= (t - x[2]);
				case 2 : s *= (t - x[1]);
				case 1 : s *= (t - x[0]);
			}

			fx /= s;
			fd /= s;

			switch (j) {
				case 4 : fd -= fx / (t - x[3]);
				case 3 : fd -= fx / (t - x[2]);
				case 2 : fd -= fx / (t - x[1]);
				case 1 : fd -= fx / (t - x[0]);
			}
		
			if ((fabs(fx) < epsilon) || fzero(fd))
				break;

			t = t - fx / fd;
		}

		x[j] = t;
	}

	return 5;
}
*/


/*
int solve(float a, float b, float c, float d, float e, float f, int steps, float epsilon, float *x) {
	const float fsteps = float(steps);
	float t = 0.5f;
		
	while (steps-- > 0) {		
		float fx =        a*t*t*t*t*t +    b*t*t*t*t +    c*t*t*t +    d*t*t + e*t + f;	// f(x)
		float fx1 =  5.0f*a*t*t*t*t + 4.0f*b*t*t*t + 3.0f*c*t*t + 2.0f*d*t +   e;		// f'(x)
		float fx2 = 20.0f*a*t*t*t +  12.0f*b*t*t +   6.0f*c*t +   2.0f*d;				// f''(x)
		
		if (fcmp(fx, epsilon))
			break;
		
		float G = fx1 / fx;
		float H = G * G - (fx2 / fx);
		
		float aDiv = fabsf( (fsteps - 1.0f) * (fsteps * H - G * G) );
		//aDiv = aDiv <= 0.0f ? -sqrtf(-aDiv) : sqrtf(aDiv);
		aDiv = power(aDiv, 0.5f);
		float aDenom = (fabsf(G + aDiv) > fabsf(G - aDiv)) ? (G + aDiv) : (G - aDiv);
		
		float k = fsteps / aDenom;
		
		if (fcmp(k, epsilon))
			break;
		
		t -= k;
	}
	
	LOG("steps: %d %f\n", steps, t);
	x[0] = t;
	return 1;
}
*/
uint32 Part1By1(uint32 n) {
	n = (n ^ (n << 8)) & 0x00ff00ff;
	n = (n ^ (n << 4)) & 0x0f0f0f0f;
	n = (n ^ (n << 2)) & 0x33333333;
	n = (n ^ (n << 1)) & 0x55555555;
	return n;
}

uint32 Part1By2(uint32 n) {
	n = (n ^ (n << 16)) & 0xff0000ff;
	n = (n ^ (n <<  8)) & 0x0300f00f;
	n = (n ^ (n <<  4)) & 0x030c30c3;
	n = (n ^ (n <<  2)) & 0x09249249;
	return n;
}

uint32 Morton2(uint32 x, uint32 y) {
	return (Part1By1(y) << 1) + Part1By1(x);
}

uint32 Morton3(uint32 x, uint32 y, uint32 z) {
	return (Part1By2(z) << 2) + (Part1By2(y) << 1) + Part1By2(x);
}

// vec2
vec2::vec2() {};
vec2::vec2(float value) : x(value), y(value) {};
vec2::vec2(float x, float y) : x(x), y(y) {};

bool vec2::operator == (const vec2 &v) const {
	return fcmp(x, v.x) && fcmp(y, v.y);
}

bool vec2::operator != (const vec2 &v) const {
	return !(*this == v);
}

float& vec2::operator [] (int index) const {
	return ((float*)this)[index];
}

vec2& vec2::operator += (const vec2 &v) { 
	x += v.x; 
	y += v.y; 
	return *this; 
}

vec2& vec2::operator -= (const vec2 &v) {
	x -= v.x;
	y -= v.y;
	return *this;
}

vec2& vec2::operator *= (const vec2 &v) { 
	x *= v.x;
	y *= v.y;
	return *this;
}

vec2& vec2::operator /= (const vec2 &v) {
	x /= v.x;
	y /= v.y;
	return *this;
}

vec2& vec2::operator *= (const float s) {
	x *= s;
	y *= s;
	return *this;
}

vec2& vec2::operator /= (const float s) {
	float p = 1.0f / s;
	x *= p;
	y *= p;
	return *this;
}

vec2 vec2::operator + (const vec2 &v) const { 
	return vec2(x + v.x, y + v.y); 
}

vec2 vec2::operator - (const vec2 &v) const { 
	return vec2(x - v.x, y - v.y); 
}

vec2 vec2::operator * (const vec2 &v) const {
	return vec2(x * v.x, y * v.y);
}

vec2 vec2::operator / (const vec2 &v) const { 
	return vec2(x / v.x, y / v.y);
}

vec2 vec2::operator - () const {
	return vec2(-x, -y); 
}

vec2 vec2::operator * (const float s) const {
	return vec2(x * s, y * s);
}

vec2 vec2::operator / (const float s) const {
	float p = 1.0f / s; 
	return vec2(x * p, y * p);
}

bool vec2::equal(const vec2 &v, float eps) const {
	return fabsf(x - v.x) <= eps && fabsf(y - v.y) <= eps;
}

void vec2::normalize() {
	*this = normal();
}

vec2 vec2::normal() const {	
	float len = length(); 
	len = len == 0.0f ? 0.0f : 1.0f / len; 
	return (*this * len);
}

vec2 vec2::lerp(const vec2 &v, const float t) const {
	return *this + (v - *this) * t; 
}

vec2 vec2::reflect(const vec2 &n) const {
	return *this - n * (dot(n) * 2.0f);
}

vec2 vec2::refract(const vec2 &n, float f) const {
	float d = dot(n);
	float s = (1.0f - f * f) * (1.0f - d * d);
	return s < 0.0f ? reflect(n) : (*this * f - n * (sqrtf(s) + d*f));
}

vec2 vec2::rotate(const float angle) const {
	float s = sinf(angle), c = cosf(angle); 
	return vec2(x*c - y*s, x*s + y*c);
}

vec2 vec2::min(const vec2 &v) const { 
	return vec2(_min(x, v.x), _min(y, v.y));
}

vec2 vec2::max(const vec2 &v) const {
	return vec2(_max(x, v.x), _max(y, v.y));
}

float vec2::cross(const vec2 &v) const {
	return x*v.y-y*v.x;
}

float vec2::dot(const vec2 &v) const { 
	return x * v.x + y * v.y; 
}

float vec2::length2() const { 
	return dot(*this); 
}

float vec2::length() const { 
	return sqrtf(length2());
}

// vec3
const vec3 vec3::ZERO(0.0f);
const vec3 vec3::HALF(0.5f);
const vec3 vec3::ONE(1.0f);
const vec3 vec3::X(1.0f, 0.0f, 0.0f);
const vec3 vec3::Y(0.0f, 1.0f, 0.0f);
const vec3 vec3::Z(0.0f, 0.0f, 1.0f);

vec3::vec3() {};
vec3::vec3(float value) : x(value), y(value), z(value) {}
vec3::vec3(float x, float y, float z) : x(x), y(y), z(z) {}
vec3::vec3(const vec2 &xy, float z) : x(xy.x), y(xy.y), z(z) {}

bool vec3::operator == (const vec3 &v) const {
	return fcmp(x, v.x) && fcmp(y, v.y) && fcmp(z, v.z);
}

bool vec3::operator != (const vec3 &v) const {
	return !(*this == v);
}

float& vec3::operator [] (int index) const {
	return ((float*)this)[index];
}

vec3& vec3::operator += (const vec3 &v) { 
	x += v.x; 
	y += v.y; 
	z += v.z;
	return *this; 
}

vec3& vec3::operator -= (const vec3 &v) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

vec3& vec3::operator *= (const vec3 &v) { 
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

vec3& vec3::operator /= (const vec3 &v) {
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

vec3& vec3::operator *= (const float s) {
	x *= s;
	y *= s;
	z *= s;
	return *this;
}

vec3& vec3::operator /= (const float s) {
	float p = 1.0f / s;
	x *= p;
	y *= p;
	z *= p;
	return *this;
}

vec3 vec3::operator + (const vec3 &v) const { 
	return vec3(x + v.x, y + v.y, z + v.z); 
}

vec3 vec3::operator - (const vec3 &v) const { 
	return vec3(x - v.x, y - v.y, z - v.z); 
}

vec3 vec3::operator * (const vec3 &v) const {
	return vec3(x * v.x, y * v.y, z * v.z);
}

vec3 vec3::operator / (const vec3 &v) const { 
	return vec3(x / v.x, y / v.y, z / v.z);
}

vec3 vec3::operator - () const {
	return vec3(-x, -y, -z); 
}

vec3 vec3::operator * (const float s) const {
	return vec3(x * s, y * s, z * s);
}

vec3 vec3::operator / (const float s) const {
	float p = 1.0f / s;
	return vec3(x * p, y * p, z * p);
}

bool vec3::equal(const vec3 &v, float eps) const {
	return fabsf(x - v.x) <= eps && fabsf(y - v.y) <= eps && fabsf(z - v.z) <= eps;
}

void vec3::normalize() {
	*this = normal();
}

vec3 vec3::normal() const {	
	float len = length(); 
	len = len == 0.0f ? 0.0f : 1.0f / len; 
	return (*this * len);
}

vec3 vec3::cross(const vec3 &v) const {
	return vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
}

vec3 vec3::lerp(const vec3 &v, const float t) const {
	return *this + (v - *this) * t; 
}

vec3 vec3::reflect(const vec3 &n) const {
	return *this - n * (dot(n) * 2.0f);
}

vec3 vec3::refract(const vec3 &n, float f) const {
	float d = dot(n);
	float s = (1.0f - f * f) * (1.0f - d * d);
	return s < 0.0f ? reflect(n) : (*this * f - n * (sqrtf(s) + d*f));
}

vec3 vec3::rotate(const float angle, const vec3 &axis) const {
	float s = sinf(angle), c = cosf(angle);
	vec3 v0 = axis * dot(axis),
			v1 = *this - v0,
			v2 = axis.cross(v1);
	return vec3(v0.x + v1.x * c + v2.x * s,
				v0.y + v1.y * c + v2.y * s,
				v0.z + v1.z * c + v2.z * s);
}

vec3 vec3::axis() const {
	float ax = fabsf(x);
	float ay = fabsf(y);
	float az = fabsf(z);

	return ax < ay ? (ax < az ? X_AXIS : Z_AXIS) : (ay < az ? Y_AXIS : Z_AXIS);
}

vec3 vec3::min(const vec3 &v) const { 
	return vec3(_min(x, v.x), _min(y, v.y), _min(z, v.z));
}

vec3 vec3::max(const vec3 &v) const { 
	return vec3(_max(x, v.x), _max(y, v.y), _max(z, v.z));
}

float vec3::dot(const vec3 &v) const { 
	return x * v.x + y * v.y + z * v.z; 
}

float vec3::length2() const { 
	return dot(*this); 
}

float vec3::length() const { 
	return sqrtf(length2());
}

// vec4
vec4::vec4() {}
vec4::vec4(float value) : x(value), y(value), z(value), w(value) {};
vec4::vec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {};
vec4::vec4(const vec2 &xy, vec2 &zw) : x(xy.x), y(xy.y), z(zw.x), w(zw.y) {};
vec4::vec4(const vec3 &xyz, float w) : x(xyz.x), y(xyz.y), z(xyz.z), w(w) {};

float& vec4::operator [] (int index) const {
	return ((float*)this)[index];
}

// quat
const quat quat::IDENTITY(0.0f, 0.0f, 0.0f, 1.0f);
const quat quat::ZERO(0.0f, 0.0f, 0.0f, 0.0f);

quat::quat() {}
quat::quat(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

quat::quat(const vec3 &axis, float angle) {
	angle *= 0.5f;
	float s = sinf(angle);
	x = axis.x * s;
	y = axis.y * s;
	z = axis.z * s;
	w = cosf(angle);
}

quat::quat(const vec3 &angle) {
	vec3 a = angle * 0.5f;
	vec3 s(sinf(a.x), sinf(a.y), sinf(a.z));
	vec3 c(cosf(a.x), cosf(a.y), cosf(a.z));

	quat qPitch (0.0f,  s.x, 0.0f, c.x);
	quat qYaw   (0.0f, 0.0f,  s.y, c.y);
	quat qRoll  ( s.z, 0.0f, 0.0f, c.z);
	*this = qYaw * qPitch * qRoll;
}

quat::quat(float lat, float lng, float angle) {
	angle *= 0.5f;
	vec3 s(sinf(lng), sinf(lat), sinf(angle));
	vec3 c(cosf(lng), cosf(lat), cosf(angle));

	x = s.z * c.y * s.x;
	y = s.z * s.y;
	z = s.z * s.y * c.x;
	w = c.z;
}

quat::quat(const vec3 &from, const vec3 &to) {
	float c = from.dot(to);
	float d = sqrtf(from.length2() + to.length2());
 
	if (c / d == -1.0f) {
		xyz = from.cross(from.axis()).normal();
		w = 0.0f;
	} else {
		xyz = from.cross(to);
		w = c + d;
		normalize();
	}
}

float& quat::operator [] (int index) const {
	return ((float*)this)[index];
}

bool quat::operator == (const quat &q) const {
	return fcmp(x, q.x) && fcmp(y, q.y) && fcmp(z, q.z) && fcmp(w, q.w);
}

bool quat::operator != (const quat &q) const {
	return !(*this == q);
}

quat& quat::operator += (const quat &q) {
	x += q.x;
	y += q.y;
	z += q.z;
	w += q.w; 
	return *this; 
}

quat& quat::operator -= (const quat &q) {
	x -= q.x;
	y -= q.y;
	z -= q.z;
	w -= q.w; 
	return *this; 
}

quat& quat::operator *= (const float s) {
	x *= s; 
	y *= s; 
	z *= s; 
	w *= s; 
	return *this; 
}

quat quat::operator - () const {
	return quat(-x, -y, -z, -w);
}

quat quat::operator + (const quat &q) const { 
	return quat(x + q.x, y + q.y, z + q.z, w + q.w); 
}

quat quat::operator - (const quat &q) const { 
	return quat(x - q.x, y - q.y, z - q.z, w - q.w); 
}

quat quat::operator * (const float s) const { 
	return quat(x * s, y * s, z * s, w * s); 
}

quat quat::operator * (const quat &q) const {
	return quat(w * q.x + x * q.w + y * q.z - z * q.y,
				w * q.y + y * q.w + z * q.x - x * q.z,
				w * q.z + z * q.w + x * q.y - y * q.x,
				w * q.w - x * q.x - y * q.y - z * q.z);
}

vec3 quat::operator * (const vec3 &v) const {
//	return v + xyz.cross(xyz.cross(v) + v * w) * 2.0f;
	return (*this * quat(v.x, v.y, v.z, 0) * inverse()).xyz;
}

bool quat::equal(const quat &q, float eps) const {
	return fabsf(x - q.x) <= eps && fabsf(y - q.y) <= eps && fabsf(z - q.z) <= eps && fabsf(w - q.w) <= eps;
}

void quat::normalize() {
	*this = normal();
}

quat quat::normal() const {
	return *this * (1.0f / length());
}

quat quat::conjugate() const {
	return quat(-x, -y, -z, w);
}

quat quat::inverse() const {
	return conjugate() * (1.0f / length2());
}

quat quat::lerp(const quat &q, float t) const {
	if (t <= 0.0f) return *this;
	if (t >= 1.0f) return q;

	return dot(q) < 0 ? (*this - (q + *this) * t) : 
						(*this + (q - *this) * t);
}

quat quat::slerp(const quat &q, float t) const {
	if (t <= 0.0f) return *this;
	if (t >= 1.0f) return q;

	quat temp;
	float omega, cosom, sinom, scale0, scale1;

	cosom = dot(q);
	if (cosom < 0.0f) {
		temp = -q;
		cosom = -cosom;
	} else
		temp = q;

	if (1.0f - cosom > EPS) {
		omega = acos(cosom);
		sinom = 1.0f / sin(omega);
		scale0 = sin((1.0f - t) * omega) * sinom;
		scale1 = sin(t * omega) * sinom;
	} else {
		scale0 = 1.0f - t;
		scale1 = t;
	}

	return *this * scale0 + temp * scale1;
}

float quat::dot(const quat &q) const {
	return x * q.x + y * q.y + z * q.z + w * q.w;
}

float quat::length2() const { 
	return dot(*this); 
}

float quat::length() const { 
	return sqrtf(length2());
}

void quat::recalc() {
	w = sqrtf(_max(0.0f, 1.0f - xyz.length2()));
}

// quat2
const quat2 quat2::IDENTITY(quat::IDENTITY, quat::ZERO);

quat2::quat2() {}
quat2::quat2(const quat &real, const quat &dual) : real(real), dual(dual) {}
quat2::quat2(const quat &rot, const vec3 &pos) {
	setRot(rot);
	setPos(pos);
}

bool quat2::operator == (const quat2 &dq) const {
	return real == dq.real && dual == dq.dual;
}

bool quat2::operator != (const quat2 &dq) const {
	return !(*this == dq);
}

quat2 quat2::operator * (const quat2 &dq) const {
	return quat2(real * dq.real, real * dq.dual + dual * dq.real);
}

vec3 quat2::operator * (const vec3 &v) const {
	return real * v + ((dual.xyz * real.w - real.xyz * dual.w + real.xyz.cross(dual.xyz)) * 2.0f);
}

quat2 quat2::inverse() const {
	return quat2(real.inverse(), dual.conjugate());
}

quat2 quat2::lerp(const quat2 &dq, float t) const {
	if (t <= 0.0f) return *this;
	if (t >= 1.0f) return dq;
	return quat2(real.slerp(dq.real, t), dual.lerp(dq.dual, t));
//	return real.dot(q.real) < 0 ? quat2(real - (q.real + real) * t, dual - (q.dual + dual) * t) :
//								  quat2(real + (q.real - real) * t, dual + (q.dual - dual) * t);
}

quat quat2::getRot() const {
	return real;
}

void quat2::setRot(const quat &rot) {
	real = rot;
}

vec3 quat2::getPos() const {
	return (dual * 2.0f * real.conjugate()).xyz;
}

void quat2::setPos(const vec3 &pos) {
	dual = quat(pos.x, pos.y, pos.z, 0.0f) * real * 0.5f;
}

// mat4
mat4::mat4() {}

mat4::mat4(const quat &rot, const vec3 &pos) {
	setRot(rot);
	setPos(pos);
	e30 = e31 = e32 = 0.0f;
	e33 = 1.0f;
}

mat4::mat4(const quat2 &dq) {
	setRot(dq.getRot());
	setPos(dq.getPos());
	e30 = e31 = e32 = 0.0f;
	e33 = 1.0f;
}

mat4::mat4(float l, float r, float b, float t, float znear, float zfar) {
	identity();
	e00 = 2.0f / (r - l);
	e11 = 2.0f / (t - b);
	e22 = 2.0f / (znear - zfar);
	e03 = (l + r) / (l - r);
	e13 = (b + t) / (b - t);
	e23 = (zfar + znear) / (znear - zfar);
}

mat4::mat4(float FOV, float aspect, float znear, float zfar) {
	float k = 1.0f / tanf(FOV * 0.5f * DEG2RAD);
	identity();
	e00 = k / aspect;
	e11 = k;
	e22 = (znear + zfar) / (znear - zfar);
	e33 = 0.0f;
	e32 = -1.0f;
	e23 = 2.0f * zfar * znear / (znear - zfar);
}

mat4::mat4(const vec3 &from, const vec3 &at, const vec3 &up) {
	vec3 r, u, d;
	d = (from - at).normal();
	r = up.cross(d).normal();
	u = d.cross(r);

	this->right		= vec4(r, 0.0f);
	this->up		= vec4(u, 0.0f);
	this->dir		= vec4(d, 0.0f);
	this->offset	= vec4(from, 1.0f);
}

mat4::mat4(float e00, float e10, float e20, float e30,
		   float e01, float e11, float e21, float e31,
		   float e02, float e12, float e22, float e32,
		   float e03, float e13, float e23, float e33) : 
		   e00(e00), e10(e10), e20(e20), e30(e30),
		   e01(e01), e11(e11), e21(e21), e31(e31),
		   e02(e02), e12(e12), e22(e22), e32(e32),
		   e03(e03), e13(e13), e23(e23), e33(e33) {}

mat4::mat4(const vec4 &a, const vec4 &b, const vec4 &c, const vec4 &d) : 
		   e00(a.x), e10(a.y), e20(a.z), e30(a.w),
		   e01(b.x), e11(b.y), e21(b.z), e31(b.w),
		   e02(c.x), e12(c.y), e22(c.z), e32(c.w),
		   e03(d.x), e13(d.y), e23(d.z), e33(d.w) {}

void mat4::identity() {
	e10 = e20 = e30 = e01 = e21 = e31 = e02 = e12 = e32 = e03 = e13 = e23 = 0.0f;
	e00 = e11 = e22 = e33 = 1.0f;
}

vec4& mat4::operator [] (int index) const {
	return ((vec4*)this)[index];
}

mat4 mat4::operator * (const mat4 &m) const {
	mat4 r;
	r.e00 = e00 * m.e00 + e01 * m.e10 + e02 * m.e20 + e03 * m.e30;
	r.e10 = e10 * m.e00 + e11 * m.e10 + e12 * m.e20 + e13 * m.e30;
	r.e20 = e20 * m.e00 + e21 * m.e10 + e22 * m.e20 + e23 * m.e30;
	r.e30 = e30 * m.e00 + e31 * m.e10 + e32 * m.e20 + e33 * m.e30;
	r.e01 = e00 * m.e01 + e01 * m.e11 + e02 * m.e21 + e03 * m.e31;
	r.e11 = e10 * m.e01 + e11 * m.e11 + e12 * m.e21 + e13 * m.e31;
	r.e21 = e20 * m.e01 + e21 * m.e11 + e22 * m.e21 + e23 * m.e31;
	r.e31 = e30 * m.e01 + e31 * m.e11 + e32 * m.e21 + e33 * m.e31;
	r.e02 = e00 * m.e02 + e01 * m.e12 + e02 * m.e22 + e03 * m.e32;
	r.e12 = e10 * m.e02 + e11 * m.e12 + e12 * m.e22 + e13 * m.e32;
	r.e22 = e20 * m.e02 + e21 * m.e12 + e22 * m.e22 + e23 * m.e32;
	r.e32 = e30 * m.e02 + e31 * m.e12 + e32 * m.e22 + e33 * m.e32;
	r.e03 = e00 * m.e03 + e01 * m.e13 + e02 * m.e23 + e03 * m.e33;
	r.e13 = e10 * m.e03 + e11 * m.e13 + e12 * m.e23 + e13 * m.e33;
	r.e23 = e20 * m.e03 + e21 * m.e13 + e22 * m.e23 + e23 * m.e33;
	r.e33 = e30 * m.e03 + e31 * m.e13 + e32 * m.e23 + e33 * m.e33;
	return r;
}

vec3 mat4::operator * (const vec3 &v) const {
	return vec3(
		e00 * v.x + e01 * v.y + e02 * v.z + e03,
		e10 * v.x + e11 * v.y + e12 * v.z + e13,
		e20 * v.x + e21 * v.y + e22 * v.z + e23);
}

vec4 mat4::operator * (const vec4 &v) const {
	return vec4(
		e00 * v.x + e01 * v.y + e02 * v.z + e03 * v.w,
		e10 * v.x + e11 * v.y + e12 * v.z + e13 * v.w,
		e20 * v.x + e21 * v.y + e22 * v.z + e23 * v.w,
		e30 * v.x + e31 * v.y + e32 * v.z + e33 * v.w);
}

void mat4::rotateX(float value) {
	mat4 m;
	m.identity();
	float s = sinf(value), c = cosf(value);
	m.e11 = c;	m.e21 = s;
	m.e12 = -s;	m.e22 = c;
	*this = *this * m;
}

void mat4::rotateY(float value) {
	mat4 m;
	m.identity();
	float s = sinf(value), c = cosf(value);
	m.e00 = c;	m.e20 = -s;
	m.e02 = s;	m.e22 = c;
	*this = *this * m;
}

void mat4::rotateZ(float value) {
	mat4 m;
	m.identity();
	float s = sinf(value), c = cosf(value);
	m.e00 = c;	m.e01 = -s;
	m.e10 = s;	m.e11 = c;
	*this = *this * m;
}

void mat4::translate(const vec3 &value) {
	mat4 m;
	m.identity();
	m.setPos(value);
	*this = *this * m;
};

void mat4::scale(const vec3 &value) {
	mat4 m;
	m.identity();
	m.e00 = value.x;
	m.e11 = value.y;
	m.e22 = value.z;
	*this = *this * m;
}

float mat4::det() const {
	return	e00 * (e11 * (e22 * e33 - e32 * e23) - e21 * (e12 * e33 - e32 * e13) + e31 * (e12 * e23 - e22 * e13)) -
			e10 * (e01 * (e22 * e33 - e32 * e23) - e21 * (e02 * e33 - e32 * e03) + e31 * (e02 * e23 - e22 * e03)) +
			e20 * (e01 * (e12 * e33 - e32 * e13) - e11 * (e02 * e33 - e32 * e03) + e31 * (e02 * e13 - e12 * e03)) -
			e30 * (e01 * (e12 * e23 - e22 * e13) - e11 * (e02 * e23 - e22 * e03) + e21 * (e02 * e13 - e12 * e03));
}

mat4 mat4::inverse() const {
	float idet = 1.0f / det();
	mat4 r;
	r.e00 =  (e11 * (e22 * e33 - e32 * e23) - e21 * (e12 * e33 - e32 * e13) + e31 * (e12 * e23 - e22 * e13)) * idet;
	r.e01 = -(e01 * (e22 * e33 - e32 * e23) - e21 * (e02 * e33 - e32 * e03) + e31 * (e02 * e23 - e22 * e03)) * idet;
	r.e02 =  (e01 * (e12 * e33 - e32 * e13) - e11 * (e02 * e33 - e32 * e03) + e31 * (e02 * e13 - e12 * e03)) * idet;
	r.e03 = -(e01 * (e12 * e23 - e22 * e13) - e11 * (e02 * e23 - e22 * e03) + e21 * (e02 * e13 - e12 * e03)) * idet;
	r.e10 = -(e10 * (e22 * e33 - e32 * e23) - e20 * (e12 * e33 - e32 * e13) + e30 * (e12 * e23 - e22 * e13)) * idet;
	r.e11 =  (e00 * (e22 * e33 - e32 * e23) - e20 * (e02 * e33 - e32 * e03) + e30 * (e02 * e23 - e22 * e03)) * idet;
	r.e12 = -(e00 * (e12 * e33 - e32 * e13) - e10 * (e02 * e33 - e32 * e03) + e30 * (e02 * e13 - e12 * e03)) * idet;
	r.e13 =  (e00 * (e12 * e23 - e22 * e13) - e10 * (e02 * e23 - e22 * e03) + e20 * (e02 * e13 - e12 * e03)) * idet;
	r.e20 =  (e10 * (e21 * e33 - e31 * e23) - e20 * (e11 * e33 - e31 * e13) + e30 * (e11 * e23 - e21 * e13)) * idet;
	r.e21 = -(e00 * (e21 * e33 - e31 * e23) - e20 * (e01 * e33 - e31 * e03) + e30 * (e01 * e23 - e21 * e03)) * idet;
	r.e22 =  (e00 * (e11 * e33 - e31 * e13) - e10 * (e01 * e33 - e31 * e03) + e30 * (e01 * e13 - e11 * e03)) * idet;
	r.e23 = -(e00 * (e11 * e23 - e21 * e13) - e10 * (e01 * e23 - e21 * e03) + e20 * (e01 * e13 - e11 * e03)) * idet;
	r.e30 = -(e10 * (e21 * e32 - e31 * e22) - e20 * (e11 * e32 - e31 * e12) + e30 * (e11 * e22 - e21 * e12)) * idet;
	r.e31 =  (e00 * (e21 * e32 - e31 * e22) - e20 * (e01 * e32 - e31 * e02) + e30 * (e01 * e22 - e21 * e02)) * idet;
	r.e32 = -(e00 * (e11 * e32 - e31 * e12) - e10 * (e01 * e32 - e31 * e02) + e30 * (e01 * e12 - e11 * e02)) * idet;
	r.e33 =  (e00 * (e11 * e22 - e21 * e12) - e10 * (e01 * e22 - e21 * e02) + e20 * (e01 * e12 - e11 * e02)) * idet;
	return r;
}

mat4 mat4::inverseFast() const { 
	mat4 m;
	m.identity();
	for (int i = 0; i < 3; i++) {
		m[i][0] = (*this)[0][i];
		m[i][1] = (*this)[1][i];
		m[i][2] = (*this)[2][i];
	}
	m.translate(vec3(-e03, -e13, -e23));
	return m;
}

mat4 mat4::transpose() const {
	mat4 r;
	r.e00 = e00; r.e10 = e01; r.e20 = e02; r.e30 = e03;
	r.e01 = e10; r.e11 = e11; r.e21 = e12; r.e31 = e13;
	r.e02 = e20; r.e12 = e21; r.e22 = e22; r.e32 = e23;
	r.e03 = e30; r.e13 = e31; r.e23 = e32; r.e33 = e33;
	return r;
}

quat mat4::getRot() const {
	float t, s;
	t = 1.0f + e00 + e11 + e22;
	if (t > EPS) {
		s = 0.5f / sqrtf(t);
		return quat((e21 - e12) * s, (e02 - e20) * s, (e10 - e01) * s, 0.25f / s);
	} else
		if (e00 > e11 && e00 > e22) {
			s = 0.5f / sqrtf(1.0f + e00 - e11 - e22);
			return quat(0.25f / s, (e01 + e10) * s, (e02 + e20) * s, (e21 - e12) * s);
		} else
			if (e11 > e22) {
				s = 0.5f / sqrtf(1.0f - e00 + e11 - e22);
				return quat((e01 + e10) * s, 0.25f / s, (e12 + e21) * s, (e02 - e20) * s);
			} else {
				s = 0.5f / sqrtf(1.0f - e00 - e11 + e22);
				return quat((e02 + e20) * s, (e12 + e21) * s, 0.25f / s, (e10 - e01) * s);
			}
}

void mat4::setRot(const quat &rot) {
	float	sx = rot.x * rot.x,
			sy = rot.y * rot.y,
			sz = rot.z * rot.z,
			sw = rot.w * rot.w,
			inv = 1.0f / (sx + sy + sz + sw);

	e00 = ( sx - sy - sz + sw) * inv;
	e11 = (-sx + sy - sz + sw) * inv;
	e22 = (-sx - sy + sz + sw) * inv;
	inv *= 2.0f;

	float t1 = rot.x * rot.y;
	float t2 = rot.z * rot.w;
	e10 = (t1 + t2) * inv;
	e01 = (t1 - t2) * inv;

	t1 = rot.x * rot.z;
	t2 = rot.y * rot.w;
	e20 = (t1 - t2) * inv;
	e02 = (t1 + t2) * inv;

	t1 = rot.y * rot.z;
	t2 = rot.x * rot.w;
	e21 = (t1 + t2) * inv;
	e12 = (t1 - t2) * inv;
}

vec3 mat4::getPos() const {
	return offset.xyz;
}

void mat4::setPos(const vec3 &pos) {
	offset.xyz = pos;
}

quat2 mat4::getBasis() {
	return quat2(getRot(), getPos());
}


// Segment
Segment::Segment() {}
Segment::Segment(const vec3 &a, const vec3 &b) : a(a), b(b) {}

vec3 Segment::closestPoint(const vec3 &p, float &t) const {
	vec3 ab = b - a;
	t = ab.dot(p - a);
	if (t <= 0.0f) {
		t = 0.0f;
		return a;
	}
	
	float d = ab.dot(ab);
	if (t >= d) {
		t = 1.0f;
		return b;
	}
		
	t /= d;
	return a + ab * t;
}

float Segment::closestPoint(const Segment &segment, vec3 &p1, vec3 &p2) const {
	float s, t;

	vec3 d1 = b - a;
	vec3 d2 = segment.b - segment.a;
	vec3 r = a - segment.a;

	float t1 = d1.dot(d1),
		  t2 = d2.dot(d2),
		  f  = d2.dot(r);

	if (t1 <= EPS && t2 <= EPS) {
		s = t = 0.0f;
		p1 = a;
		p2 = segment.a;
		return (p1 - p2).length2();
	}

	if (t1 <= EPS) {
		s = 0.0f;
		t = f / t2;
		t = clamp(t, 0.0f, 1.0f);
	} else {
		float c = d1.dot(r);
		if (t2 <= EPS) {
			t = 0.0f;
			s = clamp(-c / t1, 0.0f, 1.0f);
		} else {
			float d = d1.dot(d2);
			float denom = t1 * t2 - d * d;
			if (denom != 0.0f)
				s = clamp((d * f - c * t2) / denom, 0.0f, 1.0f);
			else 
				s = 0.0f;

			t = (d * s + f) / t2;

			float tnom = d * s + f;
			if (tnom < 0.0f) {
				t = 0.0f;
				s = clamp(-c / t1, 0.0f, 1.0f);
			} else 
				if (tnom > t2) {
					t = 1.0f;
					s = clamp((d - c) / t1, 0.0f, 1.0f);
				} else
					t = tnom / t2;
		}
	}

	p1 = a + d1 * s;
	p2 = segment.a + d2 * t;
	return (p1 - p2).length2();
}

float Segment::dist2(const vec3 &p) const {
	vec3 ab = b - a,
		 ap = p - a,
		 bp = p - b;

	float e = ap.dot(ab);
	if (e <= 0.0f)	return ap.dot(ap);
	float f = ab.dot(ab);
	if (e >= f)		return bp.dot(bp);
	return ap.dot(ap) - e * e / f;
}

float Segment::dist2(const Segment &segment) const {
	vec3 p1, p2;
	return closestPoint(segment, p1, p2);
}

// Line
Segment2D::Segment2D() {}
Segment2D::Segment2D(const vec2 &a, const vec2 &b) : a(a), b(b) {}

bool Segment2D::intersect(const Segment2D &line, vec2 &p) const {
	vec2 s10 = b - a;
	vec2 s32 = line.b - line.a;

	float d = s10.cross(s32);
	if (d == 0)
		return false;
	bool dp = d > 0;

	vec2 s02 = a - line.a;
	float s_numer = s10.cross(s02);
	if ((s_numer < 0) == dp)
		return false;

	float t_numer = s32.cross(s02);
	if ((t_numer < 0) == dp)
		return false;

	if (((s_numer > d) == dp) || ((t_numer > d) == dp))
		return false;
		
	float t = t_numer / d;
	p = a + s10 * t;

	return true;
}

// Circle
Circle::Circle() {}
Circle::Circle(const vec2 &center, float radius) : center(center), radius(radius) {}

bool Circle::intersect(const Circle &circle, vec2 &n, float &t) const {
	vec2 delta = center - circle.center;
	float dist = delta.length2();
	float r = radius + circle.radius;

	if (dist < EPS) {
		n = vec2(0, 1);
		t = r;
		return true;
	}

	if (dist > r * r)
		return false;
	
	dist = sqrtf(dist);
	n = delta * (1.0f / dist);
	t = r - dist;

	return true;
}

bool Circle::intersect(const Segment2D &segment, vec2 &n, float &t) const {
	vec2 ap = center - segment.a;
	vec2 ab = segment.b - segment.a;

	float dab = ab.dot(ab);
	if (dab == 0.0f) return false;

	t = clamp(ap.dot(ab) / dab, 0.0f, 1.0f);
	n = center - (segment.a + ab * t);
	t = n.length2();
	if (t > radius * radius)
		return false;
	n = n.normal();
	t = radius - sqrtf(t);
	return true;
}

// Plane
Plane::Plane() {}

Plane::Plane(float x, float y, float z, float w) : n(x, y, z), d(w) {
	float len = 1.0f / n.dot(n);
	n *= len;
	d *= -len;
}

Plane::Plane(const vec3 &n, const float d) : n(n), d(d) {}

Plane::Plane(const vec3 &a, const vec3 &b, const vec3 &c) {
	vec3 ba = b - a;
	vec3 ca = c - a;
	n = ba.cross(ca).normal();
	d = n.dot(a);
}

float Plane::dist(const vec3 &p) const {
	return n.dot(p) - d;
}

vec3 Plane::closestPoint(const vec3 &p) const {
	return p - n * dist(p);
}

bool Plane::intersect(const vec3 &rayPos, const vec3 &rayDir, float &t) const {
	t = (d - n.dot(rayPos)) / n.dot(rayDir);
	return t >= 0.0f;
}

bool Plane::intersect(const Segment &segment, float &t) const {
	vec3 ab = segment.b - segment.a;
	t = (d - n.dot(segment.a)) / n.dot(ab);
	return t >= 0.0f && t <= 1.0f;
}


bool Plane::intersect(const Plane &plane, vec3 &rayPos, vec3 &rayDir) const {
	rayDir = n.cross(plane.n);
	float denom = rayDir.dot(rayDir);
	if (denom < EPS) return false;
	rayPos = (plane.n * d - n * plane.d).cross(rayDir) * (1.0f / denom);
	return true;
}

// Triangle
Triangle::Triangle() {}
Triangle::Triangle(const vec3 &a, const vec3 &b, const vec3 &c) : a(a), b(b), c(c) {}

vec3& Triangle::operator [] (int index) const {
	return ((vec3*)this)[index];
}

float Triangle::area() const {
	vec3 ab = b - a;
	vec3 ac = c - a;
	return ab.cross(ac).length() * 0.5f;
}

vec3 Triangle::normal() const {
	vec3 ab = b - a;
	vec3 ac = c - a;
	return ab.cross(ac).normal();
}

vec3 Triangle::barycentric(const vec3 &p) const {
	vec3	v0 = b - a, 
			v1 = c - a, 
			v2 = p - a;

	float	d00 = v0.dot(v0),
			d01 = v0.dot(v1),
			d11 = v1.dot(v1),
			d20 = v2.dot(v0),
			d21 = v2.dot(v1),
			d = 1.0f / (d00 * d11 - d01 * d01),
			v = (d11 * d20 - d01 * d21) * d,
			w = (d00 * d21 - d01 * d20) * d,
			u = 1.0f - v - w;

	return vec3(u, v, w);
}

vec3 Triangle::closestPoint(const vec3 &p) const {
	vec3 ab = b - a;
	vec3 ac = c - a;
	vec3 ap = p - a;
	float d1 = ab.dot(ap);
	float d2 = ac.dot(ap);
	if (d1 <= 0.0f && d2 <= 0.0f) return a; // barycentric coordinates (1,0,0)

	// Check if P in vertex region outside B
	vec3 bp = p - b;
	float d3 = ab.dot(bp);
	float d4 = ac.dot(bp);
	if (d3 >= 0.0f && d4 <= d3) return b; // barycentric coordinates (0,1,0)

	float vc = d1*d4 - d3*d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
		float v = d1 / (d1 - d3);
		return a + ab * v; // barycentric coordinates (1-v,v,0)
	}

	vec3 cp = p - c;
	float d5 = ab.dot(cp);
	float d6 = ac.dot(cp);
	if (d6 >= 0.0f && d5 <= d6) return c; // barycentric coordinates (0,0,1)

	float vb = d5*d2 - d1*d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
		float w = d2 / (d2 - d6);
		return a + ac * w; // barycentric coordinates (1-w,0,w)
	}

	float va = d3*d6 - d5*d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + (c - b) * w; // barycentric coordinates (0,1-w,w)
	}

	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	return a + ab * v + ac * w;
}

float Triangle::closestPoint(const Segment &segment, vec3 &p1, vec3 &p2) const {
	float dMin = segment.closestPoint(Segment(a, b), p1, p2);

	vec3 p[8];
	float d[4];

	d[0] = segment.closestPoint(Segment(b, c), p[0], p[1]);
	d[1] = segment.closestPoint(Segment(c, a), p[2], p[3]);

	p[4] = segment.a;
	p[5] = closestPoint(p[4]);
	d[2] = (p[4] - p[5]).length2();

	p[6] = segment.b;
	p[7] = closestPoint(p[6]);
	d[3] = (p[6] - p[7]).length2();

	int idx = -1;
	for (int i = 0; i < 4; i++)
		if (d[i] < dMin) {
			idx = i;
			dMin = d[i];
		}

	if (idx > -1) {
		p1 = p[idx * 2 + 0];
		p2 = p[idx * 2 + 1];
	}

	return dMin;
}

bool Triangle::intersect(const vec3 &p) const {
	vec3 pa = a - p;
	vec3 pb = b - p;
	vec3 pc = c - p;

	float ab = pa.dot(pb);
	float ac = pa.dot(pc);
	float bc = pb.dot(pc);
	float cc = pc.dot(pc);

	if (bc * ac - cc * ab < 0.0f) return false;
	float bb = pb.dot(pb);
	if (ab * bc - ac * bb < 0.0f) return false;
	return true;
}

bool Triangle::intersect(const vec3 &rayPos, const vec3 &rayDir, bool twoSide, float &u, float &v, float &t) const {
	vec3 e1 = b - a;
	vec3 e2 = c - a;
	vec3 p = rayDir.cross(e2);
	float d = p.dot(e1);
	vec3 q;

	if (d > EPS) {
		vec3 r = rayPos - a;
		u = p.dot(r);
		if (u >= 0 && u <= d) {
			q = r.cross(e1);
			v = rayDir.dot(q);
			if (v < 0 || u + v > d) 
				return false;
		} else
			return false;
	} else
		if (d < -EPS && twoSide) {
			vec3 r = rayPos - a;
			u = p.dot(r);
			if (u <= 0 && u >= d) {
				q = r.cross(e1);
				v = rayDir.dot(q);
				if (v > 0 || u + v < d)
					return false;
			} else
				return false;
		} else
			return false;

	d = 1.0f / d;
	t = q.dot(e2);

	if (t > EPS) {
		u *= d;
		v *= d;
		t *= d;
		return true;
	}
	return false;
}

// Sphere
Sphere::Sphere() {}
Sphere::Sphere(const vec3 &center, float radius) : center(center), radius(radius) {}
Sphere::Sphere(float radius) : center(0.0f), radius(radius) {}

bool Sphere::intersect(const vec3 &p) const {
	return (center - p).length2() < radius * radius;
}

bool Sphere::intersect(const vec3 &rayPos, const vec3 &rayDir, float &tMin, float &tMax) const {
	vec3 v = rayPos - center;
	float h = -v.dot(rayDir);
	float d = h * h + radius * radius - v.length2();

	if (d > 0.0f) {
		d = sqrtf(d);
		float tMin = h - d;
		float tMax = h + d;
		if (tMax > 0.0f) {
			if (tMin < 0.0f)
				tMin = 0.0f;
			return true;
		}
	}
	return false;
}

bool Sphere::intersect(const Plane &plane, const vec3 &vel, vec3 &p, float &t) const {
	float d = plane.dist(center);
	if (fabsf(d) <= radius) {
		t = 0.0f;
		p = plane.closestPoint(center);
		return true;
	}
	float denom = vel.dot(plane.n);
	if (denom * d >= 0.0f) return false;
	float r = d > 0.0f ? radius : -radius;
	t = (r - d) / denom;
	p = center + vel * t - plane.n * r;
	return true;
}

bool Sphere::intersect(const Triangle &triangle, vec3 &n, float &t) const {
	vec3 p = triangle.closestPoint(center);
	vec3 v = center - p;

	float d = v.length2();
	if (d > radius * radius)
		return false;
	n = v.normal();
	t = radius - sqrtf(d);
	return true;
}

bool Sphere::intersect(const Sphere &sphere) const {
	float r = radius + sphere.radius;
	return (center - sphere.center).length2() <= r * r;
}

bool Sphere::intersect(const Sphere &sphere, vec3 &n, float &t) const {
	vec3 delta = center - sphere.center;
	float dist = delta.length2();
	float r = radius + sphere.radius;

	if (dist < EPS) {
		n = vec3(0, 1, 0);
		t = r;
		return true;
	}

	if (dist > r * r)
		return false;
	
	dist = sqrtf(dist);
	n = delta * (1.0f / dist);
	t = r - dist;

	return true;
}

bool Sphere::intersect(const Sphere &sphere, const vec3 &vel, float &t) const {
	vec3 s = sphere.center - center;
	float r = sphere.radius + radius;
	float c = s.dot(s) - r * r;
	if (c < 0.0f) {
		t = 0.0f;
		return true;
	}
	float a = vel.dot(vel);
	if (a < EPS) return false;
	float b = vel.dot(s);
	if (b >= 0.0f) return false;
	float d = b * b - a * c;
	if (d < 0.0f) return false;
	t = (-b - sqrtf(d)) / a;
	return true;
}

// Box
Box::Box() {}
Box::Box(const vec3 &min, const vec3 &max) : min(min), max(max) {}
Box::Box(const vec3 &size) : min(-size), max(size) {}

float& Box::operator [] (int index) { 
	return ((float*)this)[index]; 
}

vec3 Box::getCenter() const {
	return (min + max) * 0.5f;
}

vec3 Box::closestPoint(const vec3 &p) const {
	return vec3(clamp(p.x, min.x, max.x),
				clamp(p.y, min.y, max.y),
				clamp(p.z, min.z, max.z));
//	return max.min(min.max(p));
}

float Box::dist2(const vec3 &p) const {
	float d = 0.0f;
	for (int i = 0; i < 3; i++) {
		float v = p[i];
		if (v < min[i]) d += (min[i] - v) * (min[i] - v);
		if (v > max[i]) d += (v - max[i]) * (v - max[i]);
	}
	return d;
}

bool Box::intersect(const vec3 &p) const {
	return	p.x >= min.x && p.x <= max.x && 
			p.y >= min.y && p.y <= max.y && 
			p.z >= min.z && p.z <= max.z;
}

bool Box::intersect(const vec3 &rayPos, const vec3 &rayDir, float &t) const {
	float t1 = INF, t0 = -t1;

	for (int i = 0; i < 3; i++) 
		if (rayDir[i] != 0) {
			float lo = (min[i] - rayPos[i]) / rayDir[i];
			float hi = (max[i] - rayPos[i]) / rayDir[i];
			t0 = _max(t0, _min(lo, hi));
			t1 = _min(t1, _max(lo, hi));
		} else
			if (rayPos[i] < min[i] || rayPos[i] > max[i])
				return false;
	t = t0;
	return (t0 <= t1) && (t1 > 0);
}

bool Box::intersect(const Box &box) const {
	return !((max.x < box.min.x || min.x > box.max.x) || (max.y < box.min.y || min.y > box.max.y) || (max.z < box.min.z || min.z > box.max.z));
}

bool Box::intersect(const Sphere &sphere, vec3 &n, float &t) const {
	if (intersect(sphere.center))
		return true; // outNormal?
	vec3 p = closestPoint(sphere.center);
	n = sphere.center - p;
	t = n.length2();
	if (t > sphere.radius * sphere.radius)
		return false;
	n = n.normal(); 
	t = sphere.radius - sqrtf(t);
	return true;
}

bool Box::intersect(const quat2 &basis, const Sphere &sphere, vec3 &n, float &t) const {
	vec3 center = basis.inverse() * sphere.center;
	if (intersect(Sphere(center, sphere.radius), n, t)) {
		n = basis.getRot() * n;
		return true;
	}
	return false;
}

Curve::Curve() : count(0), points(NULL) {}
Curve::Curve(Stream *stream) {;
	points = stream->readArray<vec3>(count = stream->read<int>());
}

Curve::~Curve() {
	delete[] points;
}

vec3 Curve::eval(int index, float t) {
	vec3 &pa = points[index + 0];
	vec3 &pb = points[index + 1];
	vec3 &pc = points[index + 2];
	vec3 &pd = points[index + 3];

	vec3 a = (pb - pc) * 3.0f + pd - pa;
	vec3 b = (pa + pc) * 3.0f - pb * 6.0f;
	vec3 c = (pb - pa) * 3.0f;

	float t2 = t * t;

	return a * (t2 * t) + b * t2 + c * t + pa;
}

vec3 Curve::eval(float t) {
	int c = (count + 2) / 3 - 1;
	t *= c;
	int i = (int)t;
	if (i == c)
		i--;
	return eval(i * 3, t - (float)i);
}

vec3 Curve::evalTangent(int index, float t) {
	vec3 &pa = points[index + 0];
	vec3 &pb = points[index + 1];
	vec3 &pc = points[index + 2];
	vec3 &pd = points[index + 3];

	vec3 a = (pb - pc) * 3.0f + pd - pa;
	vec3 b = (pa + pc) * 3.0f - pb * 6.0f;
	vec3 c = (pb - pa) * 3.0f;

	return a * (3.0f * t * t) + b * (2.0f * t) + c;
}

vec3 Curve::evalTangent(float t) {
	int c = (count + 2) / 3;
	t *= c - 1;
	int i = (int)t;
	return evalTangent(i * 3, t - (float)i);
}

#include "debug.h"

float Curve::closest(int index, const vec3 &p) {
	vec3 &p0 = points[index + 0];
	vec3 &p1 = points[index + 1];
	vec3 &p2 = points[index + 2];
	vec3 &p3 = points[index + 3];

	vec3 a = (p1 - p2) * 3.0f + p3 - p0;
	vec3 b = (p0 + p2 - p1 * 2.0f) * 3.0f;
	vec3 c = (p1 - p0) * 3.0f;
	vec3 d = p0 - p;

	float dA = 3.0f * a.dot(a);
	float dB = 5.0f * a.dot(b);
	float dC = 4.0f * a.dot(c) + 2.0f * b.dot(b);
	float dD = 3.0f * (a.dot(d) + b.dot(c));
	float dE = 2.0f * b.dot(d) + c.dot(c);
	float dF = c.dot(d);

	float dMin, tMin = -1.0f;

	float d0 = (p - p0).length2(),
		  d1 = (p - p3).length2();

	if (d0 < d1) {
		dMin = d0;
		tMin = 0.0f;
	} else {
		dMin = d1;
		tMin = 1.0f;
	}

	float ext[5];
	int eCount = solve(dA, dB, dC, dD, dE, dF, 16, 0.001f, ext);

	if (eCount == 0)
		return tMin;

	for (int i = 0; i < eCount; i++) {
		if (ext[i] != ext[i]) continue;

		float t = clamp(ext[i], 0.0f, 1.0f);

	//	Debug::Draw::point(eval(index, t), vec3(1.0f, 0.0f, 1.0f));

		float d = (eval(index, t) - p).length2();

		if (tMin < 0.0f) {
			tMin = t;
			dMin = d;
			continue;
		}

		if (d < dMin) {
			dMin = d;
			tMin = t;
		}
	}

	return tMin;
}

float Curve::closest(const vec3 &p) {
	float tMin = closest(0, p),
		  dMin = (p - eval(0, tMin)).length2();
	tMin *= 3.0f;

	for (int i = 3; i < count - 1; i += 3) {
		float t = closest(i, p);
		float d = (p - eval(i, t)).length2();

		if (d < dMin) {
			dMin = d;
			tMin = i + t * 3.0f;
		}
	}
	return tMin / (count - 1);
}

float Curve::offset(float t, float dist) {
	return t;
}


/*
float bezierPoint(float t, float a, float b, float c, float d)
     {
     float C1 = ( d - (3.0 * c) + (3.0 * b) - a );
     float C2 = ( (3.0 * c) - (6.0 * b) + (3.0 * a) );
     float C3 = ( (3.0 * b) - (3.0 * a) );
     float C4 = ( a );
 
     return ( C1*t*t*t + C2*t*t + C3*t + C4  );
     }
 
 float bezierTangent(float t, float a, float b, float c, float d)
     {
     float C1 = ( d - (3.0 * c) + (3.0 * b) - a );
     float C2 = ( (3.0 * c) - (6.0 * b) + (3.0 * a) );
     float C3 = ( (3.0 * b) - (3.0 * a) );
     float C4 = ( a );
 
     return ( ( 3.0 * C1 * t* t ) + ( 2.0 * C2 * t ) + C3 );
     }
*/


/*
struct CResult {
	float sqrDistance;
	float lineParameter;
	float distance;
};

void Face(int i0, int i1, int i2, vec3 &pnt, vec3 const &dir, vec3 const &PmE, vec3 const &boxExtent, CResult &result)
{
    vec3 PpE;
    float lenSqr, inv, tmp, param, t, delta;

    PpE[i1] = pnt[i1] + boxExtent[i1];
    PpE[i2] = pnt[i2] + boxExtent[i2];
    if (dir[i0] * PpE[i1] >= dir[i1] * PmE[i0])
    {
        if (dir[i0] * PpE[i2] >= dir[i2] * PmE[i0])
        {
            // v[i1] >= -e[i1], v[i2] >= -e[i2] (distance = 0)
            pnt[i0] = boxExtent[i0];
            inv = 1.0f / dir[i0];
            pnt[i1] -= dir[i1] * PmE[i0] * inv;
            pnt[i2] -= dir[i2] * PmE[i0] * inv;
            result.lineParameter = -PmE[i0] * inv;
        }
        else
        {
            // v[i1] >= -e[i1], v[i2] < -e[i2]
            lenSqr = dir[i0] * dir[i0] + dir[i2] * dir[i2];
            tmp = lenSqr*PpE[i1] - dir[i1] * (dir[i0] * PmE[i0] +
                dir[i2] * PpE[i2]);
            if (tmp <= 2.0f*lenSqr*boxExtent[i1])
            {
                t = tmp / lenSqr;
                lenSqr += dir[i1] * dir[i1];
                tmp = PpE[i1] - t;
                delta = dir[i0] * PmE[i0] + dir[i1] * tmp + dir[i2] * PpE[i2];
                param = -delta / lenSqr;
                result.sqrDistance += PmE[i0] * PmE[i0] + tmp*tmp +
                    PpE[i2] * PpE[i2] + delta*param;

                result.lineParameter = param;
                pnt[i0] = boxExtent[i0];
                pnt[i1] = t - boxExtent[i1];
                pnt[i2] = -boxExtent[i2];
            }
            else
            {
                lenSqr += dir[i1] * dir[i1];
                delta = dir[i0] * PmE[i0] + dir[i1] * PmE[i1] + dir[i2] * PpE[i2];
                param = -delta / lenSqr;
                result.sqrDistance += PmE[i0] * PmE[i0] + PmE[i1] * PmE[i1]
                    + PpE[i2] * PpE[i2] + delta*param;

                result.lineParameter = param;
                pnt[i0] = boxExtent[i0];
                pnt[i1] = boxExtent[i1];
                pnt[i2] = -boxExtent[i2];
            }
        }
    }
    else
    {
        if (dir[i0] * PpE[i2] >= dir[i2] * PmE[i0])
        {
            // v[i1] < -e[i1], v[i2] >= -e[i2]
            lenSqr = dir[i0] * dir[i0] + dir[i1] * dir[i1];
            tmp = lenSqr*PpE[i2] - dir[i2] * (dir[i0] * PmE[i0] +
                dir[i1] * PpE[i1]);
            if (tmp <= 2.0f*lenSqr*boxExtent[i2])
            {
                t = tmp / lenSqr;
                lenSqr += dir[i2] * dir[i2];
                tmp = PpE[i2] - t;
                delta = dir[i0] * PmE[i0] + dir[i1] * PpE[i1] + dir[i2] * tmp;
                param = -delta / lenSqr;
                result.sqrDistance += PmE[i0] * PmE[i0] + PpE[i1] * PpE[i1] +
                    tmp*tmp + delta*param;

                result.lineParameter = param;
                pnt[i0] = boxExtent[i0];
                pnt[i1] = -boxExtent[i1];
                pnt[i2] = t - boxExtent[i2];
            }
            else
            {
                lenSqr += dir[i2] * dir[i2];
                delta = dir[i0] * PmE[i0] + dir[i1] * PpE[i1] + dir[i2] * PmE[i2];
                param = -delta / lenSqr;
                result.sqrDistance += PmE[i0] * PmE[i0] + PpE[i1] * PpE[i1] +
                    PmE[i2] * PmE[i2] + delta*param;

                result.lineParameter = param;
                pnt[i0] = boxExtent[i0];
                pnt[i1] = -boxExtent[i1];
                pnt[i2] = boxExtent[i2];
            }
        }
        else
        {
            // v[i1] < -e[i1], v[i2] < -e[i2]
            lenSqr = dir[i0] * dir[i0] + dir[i2] * dir[i2];
            tmp = lenSqr*PpE[i1] - dir[i1] * (dir[i0] * PmE[i0] +
                dir[i2] * PpE[i2]);
            if (tmp >= 0.0f)
            {
                // v[i1]-edge is closest
                if (tmp <= 2.0f*lenSqr*boxExtent[i1])
                {
                    t = tmp / lenSqr;
                    lenSqr += dir[i1] * dir[i1];
                    tmp = PpE[i1] - t;
                    delta = dir[i0] * PmE[i0] + dir[i1] * tmp + dir[i2] * PpE[i2];
                    param = -delta / lenSqr;
                    result.sqrDistance += PmE[i0] * PmE[i0] + tmp*tmp +
                        PpE[i2] * PpE[i2] + delta*param;

                    result.lineParameter = param;
                    pnt[i0] = boxExtent[i0];
                    pnt[i1] = t - boxExtent[i1];
                    pnt[i2] = -boxExtent[i2];
                }
                else
                {
                    lenSqr += dir[i1] * dir[i1];
                    delta = dir[i0] * PmE[i0] + dir[i1] * PmE[i1]
                        + dir[i2] * PpE[i2];
                    param = -delta / lenSqr;
                    result.sqrDistance += PmE[i0] * PmE[i0] + PmE[i1] * PmE[i1]
                        + PpE[i2] * PpE[i2] + delta*param;

                    result.lineParameter = param;
                    pnt[i0] = boxExtent[i0];
                    pnt[i1] = boxExtent[i1];
                    pnt[i2] = -boxExtent[i2];
                }
                return;
            }

            lenSqr = dir[i0] * dir[i0] + dir[i1] * dir[i1];
            tmp = lenSqr*PpE[i2] - dir[i2] * (dir[i0] * PmE[i0] +
                dir[i1] * PpE[i1]);
            if (tmp >= 0.0f)
            {
                // v[i2]-edge is closest
                if (tmp <= 2.0f*lenSqr*boxExtent[i2])
                {
                    t = tmp / lenSqr;
                    lenSqr += dir[i2] * dir[i2];
                    tmp = PpE[i2] - t;
                    delta = dir[i0] * PmE[i0] + dir[i1] * PpE[i1] + dir[i2] * tmp;
                    param = -delta / lenSqr;
                    result.sqrDistance += PmE[i0] * PmE[i0] + PpE[i1] * PpE[i1] +
                        tmp*tmp + delta*param;

                    result.lineParameter = param;
                    pnt[i0] = boxExtent[i0];
                    pnt[i1] = -boxExtent[i1];
                    pnt[i2] = t - boxExtent[i2];
                }
                else
                {
                    lenSqr += dir[i2] * dir[i2];
                    delta = dir[i0] * PmE[i0] + dir[i1] * PpE[i1]
                        + dir[i2] * PmE[i2];
                    param = -delta / lenSqr;
                    result.sqrDistance += PmE[i0] * PmE[i0] + PpE[i1] * PpE[i1]
                        + PmE[i2] * PmE[i2] + delta*param;

                    result.lineParameter = param;
                    pnt[i0] = boxExtent[i0];
                    pnt[i1] = -boxExtent[i1];
                    pnt[i2] = boxExtent[i2];
                }
                return;
            }

            // (v[i1],v[i2])-corner is closest
            lenSqr += dir[i2] * dir[i2];
            delta = dir[i0] * PmE[i0] + dir[i1] * PpE[i1] + dir[i2] * PpE[i2];
            param = -delta / lenSqr;
            result.sqrDistance += PmE[i0] * PmE[i0] + PpE[i1] * PpE[i1]
                + PpE[i2] * PpE[i2] + delta*param;

            result.lineParameter = param;
            pnt[i0] = boxExtent[i0];
            pnt[i1] = -boxExtent[i1];
            pnt[i2] = -boxExtent[i2];
        }
    }
}

void Case0(int i0, int i1, int i2, vec3 &pnt, vec3 const &dir, vec3 const &boxExtent, CResult &result)
{
    float PmE0 = pnt[i0] - boxExtent[i0];
    float PmE1 = pnt[i1] - boxExtent[i1];
    float prod0 = dir[i1] * PmE0;
    float prod1 = dir[i0] * PmE1;
    float delta, invLSqr, inv;

    if (prod0 >= prod1)
    {
        // line intersects P[i0] = e[i0]
        pnt[i0] = boxExtent[i0];

        float PpE1 = pnt[i1] + boxExtent[i1];
        delta = prod0 - dir[i0] * PpE1;
        if (delta >= 0.0f)
        {
            invLSqr = 1.0f / (dir[i0] * dir[i0] + dir[i1] * dir[i1]);
            result.sqrDistance += delta*delta*invLSqr;
            pnt[i1] = -boxExtent[i1];
            result.lineParameter = -(dir[i0] * PmE0 + dir[i1] * PpE1)*invLSqr;
        }
        else
        {
            inv = 1.0f / dir[i0];
            pnt[i1] -= prod0*inv;
            result.lineParameter = -PmE0*inv;
        }
    }
    else
    {
        // line intersects P[i1] = e[i1]
        pnt[i1] = boxExtent[i1];

        float PpE0 = pnt[i0] + boxExtent[i0];
        delta = prod1 - dir[i1] * PpE0;
        if (delta >= 0.0f)
        {
            invLSqr = 1.0f / (dir[i0] * dir[i0] + dir[i1] * dir[i1]);
            result.sqrDistance += delta*delta*invLSqr;
            pnt[i0] = -boxExtent[i0];
            result.lineParameter = -(dir[i0] * PpE0 + dir[i1] * PmE1)*invLSqr;
        }
        else
        {
            inv = 1.0f / dir[i1];
            pnt[i0] -= prod1*inv;
            result.lineParameter = -PmE1*inv;
        }
    }

    if (pnt[i2] < -boxExtent[i2])
    {
        delta = pnt[i2] + boxExtent[i2];
        result.sqrDistance += delta*delta;
        pnt[i2] = -boxExtent[i2];
    }
    else if (pnt[i2] > boxExtent[i2])
    {
        delta = pnt[i2] - boxExtent[i2];
        result.sqrDistance += delta*delta;
        pnt[i2] = boxExtent[i2];
    }
}

void Case00(int i0, int i1, int i2, vec3 &pnt, vec3 const &dir, vec3 const &boxExtent, CResult &result)
{
    float delta;

    result.lineParameter = (boxExtent[i0] - pnt[i0]) / dir[i0];

    pnt[i0] = boxExtent[i0];
	int idx[] = { i1, i2 };

	for (int i = 0; i < 2; i++) {
		int j = idx[i];
		if (pnt[j] < -boxExtent[j])
		{
			delta = pnt[j] + boxExtent[j];
			result.sqrDistance += delta*delta;
			pnt[j] = -boxExtent[j];
		}
		else if (pnt[j] > boxExtent[j])
		{
			delta = pnt[j] - boxExtent[j];
			result.sqrDistance += delta*delta;
			pnt[j] = boxExtent[j];
		}
	}
}

void Case000(vec3 &pnt, vec3 const &boxExtent, CResult &result)
{
    float delta;

	for (int i = 0; i < 3; i++)
		if (pnt[i] < -boxExtent[i])
		{
			delta = pnt[i] + boxExtent[i];
			result.sqrDistance += delta*delta;
			pnt[i] = -boxExtent[i];
		}
		else if (pnt[i] > boxExtent[i])
		{
			delta = pnt[i] - boxExtent[i];
			result.sqrDistance += delta*delta;
			pnt[i] = boxExtent[i];
		}
}

float Box::closestPoint(const Segment &segment, vec3 &cp1, vec3 &cp2) const {
	vec3  sc = (segment.a + segment.b) * 0.5f;
	vec3  sd = (segment.b - segment.a).normal();
	float se = sd.length() * 0.5f;

	vec3  bc = getCenter();
	vec3  be = (max - min) * 0.5f;

	vec3 p = sc - bc;
	vec3 d = sd;

	bool  r[3];

	for (int i = 0; i < 3; i++)
		if (d[i] < 0.0f) {
			p[i] = -p[i];
			d[i] = -d[i];
			r[i] = true;
		} else
			r[i] = false;

	#define __ 0
	#define MX 1
	#define MY 2
	#define MZ 4

	CResult result;
	result.lineParameter = 0.0f;
	result.sqrDistance = 0.0f;

	int mask =	(d.x > 0.0f ? MX : 0) | 
				(d.y > 0.0f ? MY : 0) | 
				(d.z > 0.0f ? MZ : 0);

	switch (mask) {
		case MX | MY | MZ : {
			vec3 PmE = p - be;
			float xy = d.x * PmE.y;
			float yx = d.y * PmE.x;
			float zx, xz, zy, yz;

			if (yx >= xy) {
				zx = d.z * PmE.x;
				xz = d.x * PmE.z;
				if (zx >= xz)	// line intersects x = e0
					Face(0, 1, 2, p, d, PmE, be, result);
				else			// line intersects z = e2
					Face(2, 0, 1, p, d, PmE, be, result);
			} else {
				zy = d.z * PmE.y;
				yz = d.x * PmE.z;
				if (zy >= yz)	// line intersects y = e1
					Face(1, 2, 0, p, d, PmE, be, result);
				else			// line intersects z = e2
					Face(2, 0, 1, p, d, PmE, be, result);
			}
			break;
		}
		case MX | MY | __ :
			Case0(0, 1, 2, p, d, be, result);
			break;
		case MX | __ | MZ : 
			Case0(0, 2, 1, p, d, be, result);
			break;
		case MX | __ | __ :
			Case00(0, 1, 2, p, d, be, result);
			break;
		case __ | MY | MZ :
			Case0(1, 2, 0, p, d, be, result);
			break;
		case __ | MY | __ :
			Case00(1, 0, 2, p, d, be, result);
			break;
		case __ | __ | MZ :
			Case00(2, 0, 1, p, d, be, result);
			break;
		case __ | __ | __ :
			Case000(p, be, result);
			break;
	}

	for (int i = 0; i < 3; ++i)
		if (r[i])
			p[i] = -p[i];

	result.distance = sqrtf(result.sqrDistance);

	if (result.lineParameter >= -se) 
		if (result.lineParameter <= se) {
			cp1 = sc + d * result.lineParameter;
			cp2 = bc + p;
		} else {
			cp1 = segment.b;
			cp2 = closestPoint(segment.b);
		}
	else {
		cp1 = segment.a;
		cp2 = closestPoint(segment.a);
	}

	return (cp2 - cp1).length();
}

// Capsule
Capsule::Capsule() {}

Capsule::Capsule(float radius, float height) : radius(radius), height(height) {}

// Convex
vec3& Convex::operator [] (int index) const {
	return vertex[index];
}

Capsule::Capsule(const vec3 &a, const vec3 &b, const float radius) : a(a), b(b), radius(radius) {}

bool Capsule::intersect(const Sphere &sphere) const {
	float dist = Segment(a, b).dist2(sphere.center);
	float r = radius + sphere.radius;
	return dist <= r * r;
}

bool Capsule::intersect(const Box &box, vec3 &n, float &t) const {
	vec3 c1, c2;
	float d = box.closestPoint(Segment(a, b), c1, c2);
	if (d > radius)
		return false;
	t = radius - d;
	n = (c1 - c2).normal();
	return true;
}

bool Capsule::intersect(const Capsule &capsule) const {
	float dist = Segment(a, b).dist2(Segment(capsule.a, capsule.b));
	float r = radius + capsule.radius;
	return dist <= r * r;
}
*/
