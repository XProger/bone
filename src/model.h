#ifndef H_MODEL
#define H_MODEL

#include "utils.h"
#include "core.h"
#include "material.h"

struct Animation {

	struct Joint {
		quat2	*basis;
		float	*scale;
		Joint(Stream *stream);
		~Joint();
	};

	char			*name;
	int				frames;
	Array<Joint>	joints;
	int				channels;

	Animation(Stream *stream);
	~Animation();
	bool getBasis(int jIndex, float time, quat2 &basis, vec3 &scale);
};

struct Skeleton {
	
	struct Joint {
		char	*name;
		int		parent;
		quat2	basis;

		Joint(Stream *stream);
		~Joint();
	};

	Array<Joint> joints;

	Skeleton(Stream *stream);
	int getJointIndex(const char *jName);
};

struct NavMesh {

	struct Path {
		int  count;
		vec3 *point;
		vec3 target;

		Path(int count);
		~Path();			
		void add(const vec3 &p);		
		bool next();
	};

	struct Face {
		Index idx[3];
		Index adj[3];
	} *face;

	struct Edge {
		Index idx[2];
	} *edge;

	vec3	*vertex;
	Index	*stack;
	Index	*parent;
	int		vCount, fCount, eCount;
	int		pathLen;

	NavMesh(Stream *stream);
	~NavMesh();

	Index getFaceIndex(const vec3 &pos);
	Path* findPath(const vec3 &start, const vec3 &end);
	void collide(vec3 &pos, float radius);
//	void debug(Index idx, Path *path);
};


struct Collider {
	enum Type { BOX, SPHERE, MESH, CONTOUR };

	quat2	basis;
	int		type;
	float	maxRadius2;
	union {
		struct { vec3 size; };
		struct { float radius, height; };
		struct { int iCount, vCount; Index *indices; vec3 *mVertices; vec2 *cVertices; };
	};

	Collider(Stream *stream);
	~Collider();
	bool collide(const Sphere &sphere, vec3 &n, float &t);
};

struct MeshPart {
	int materialID;
	int iStart;
	int iCount;
};

struct Model {

	struct Resource : ResObject {

		struct Object {
			enum ObjectType  { OBJ_GEOMETRY = 0x1, OBJ_TRIGGER = 0x2, OBJ_LIGHT = 0x4, OBJ_SOUND = 0x8, OBJ_CAMERA = 0x10 };
			enum TriggerType { TRIG_POINT = 0x10000, TRIG_BOX = 0x20000, TRIG_SPHERE = 0x40000};
			enum CameraType  { CAM_STATIC = 0x10000, CAM_TARGET = 0x20000, CAM_FOLLOW = 0x40000, CAM_CURVE = 0x80000};

			int		id;
			int		type;
			int		flags;
			int		parentID;
			int		animationID;
			float	time;

			quat2	basis;
			vec3	scale;

			char	*name;

			union {
				struct {		// OBJ_GEOMETRY
					Box bbox; 

					struct {
						vec3 scale, bias;
					} geom;

					struct {
						vec4 scale, bias;
					} tex;

					int		meshID;

					int		pCount;
					MeshPart *parts;

					int		jCount;
					int		*joints;

					Array<Collider>	*colliders;
				};

				struct {		// OBJ_TRIGGER
					union {
						struct { vec3 size; };
						struct { float radius; };
					};
					int	lCount;
					int	*links;
					int damage;
					int camera;
				};

				struct {		// OBJ_CAMERA
					float fov;
					float znear;
					float zfar;
					Curve *curve;
				};
			};

			float	viewDist;

			Object(Stream *stream);
			~Object();
		};
	
		int					cameraID;
		Array<Mesh>			meshes;
		Array<Material>		materials;
		Array<Animation>	animations;
		Array<Object>		objects;
		Skeleton			*skeleton;
		NavMesh				*navmesh;

		Resource(Stream *stream, int param);
		virtual ~Resource();
	} *res;

	struct State {
		enum { FLAG_VISIBLE = 1, FLAG_PLAYING = 2, FLAG_LOOP = 4, FLAG_PONG = 8, FLAG_REWIND = 16, FLAG_ACTION = 32, FLAG_DEACTIVATE = 64 };

		struct Object {			

			float	time;
			int		flags;
			quat2	basis;
			vec3	scale;

			Object() : time(0.0f), flags(FLAG_VISIBLE) {}
		} *objects;

		struct Animation {
			int		id;
			float	time;
			float	blendTimeCur;
			float	blendTimeMax;
			float	weight;
			int		flags;

			bool	isPlaying() { return (flags & FLAG_PLAYING) > 0; }

			Animation() : id(-1), time(0.0f), weight(0.0f), flags(0) {}
		} *animations;

		struct Joint {
			quat2	relative;
			quat2	absolute;
			bool	updated;
		} *joints;

		int	*order;

		State(Resource *res);
		~State();

	} state;

	quat2 basis;

	Model(const char *name);
	~Model();
	
	void play(const char *name, float blendTime, float weight, bool loop);
	bool isPlaying(float offset);
	Resource::Object* getObject(const char *name);
	quat2& getRelative(int jIndex);
	quat2& getAbsolute(int jIndex);
	void sort();
	void update();
	void render();
};
#endif