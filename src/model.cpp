#include "core.h"

#define DIFF_RX 1
#define DIFF_RY 2
#define DIFF_RZ 4
#define DIFF_PX 8
#define DIFF_PY 16
#define DIFF_PZ 32
#define DIFF_SC 64

#define CHANNEL_ROT		(DIFF_RX | DIFF_RY | DIFF_RZ)
#define CHANNEL_POS		(DIFF_PX | DIFF_PY | DIFF_PZ)
#define CHANNEL_SCALE	DIFF_SC

// Animation
Animation::Animation(Stream *stream) : name(stream->readStr()), frames(stream->read<int>()), joints(stream), channels(stream->read<int>()) {}

Animation::~Animation() {
	delete[] name;
}

#define INV_SHORT (1.0f / 32767.0f)

// Animation::Joint
Animation::Joint::Joint(Stream *stream) : basis(NULL), scale(NULL) {
	int count = stream->read<int>();
	if (count == 0)
		return;

	basis = new quat2[count];
	scale = new float[count];

	char *diff = stream->readArray<char>(count);

	vec3 aScale = stream->read<vec3>(),
		 aBias  = stream->read<vec3>();

	quat	rot(0.0f, 0.0f, 0.0f, 1.0f);
	vec3	pos = aBias;
	float	scxyz = 1.0f;

	for (int i = 0; i < count; i++) {
		char d = diff[i];

		if (!d) {
			if (i) {
				basis[i] = basis[i - 1];
				scale[i] = scale[i - 1];
			} else {
				basis[i] = quat2::IDENTITY;
				scale[i] = 1.0f;
			}
			continue;
		}
		
		if (d & DIFF_RX) rot.x = stream->read<short>() * INV_SHORT;
		if (d & DIFF_RY) rot.y = stream->read<short>() * INV_SHORT;
		if (d & DIFF_RZ) rot.z = stream->read<short>() * INV_SHORT;
		if (d & DIFF_PX) pos.x = stream->read<short>() * aScale.x + aBias.x;
		if (d & DIFF_PY) pos.y = stream->read<short>() * aScale.y + aBias.y;
		if (d & DIFF_PZ) pos.z = stream->read<short>() * aScale.z + aBias.z;
		if (d & DIFF_SC) scxyz = stream->read<float>();
		
		if (d & CHANNEL_ROT)
			rot.recalc();
		
		basis[i] = quat2(rot, pos);
		scale[i] = scxyz;
	}

	delete[] diff;
}

Animation::Joint::~Joint() {
	delete[] basis;
	delete[] scale;
}

bool Animation::getBasis(int jIndex, float time, quat2 &basis, vec3 &scale) {
	Joint *joint = joints[jIndex];
	if (!joint->basis)
		return false;

	int cf = int(time) % frames;
	int nf = (cf + 1) % frames;
	quat2 &q1 = joint->basis[cf];
	quat2 &q2 = joint->basis[nf];
	float s1 = joint->scale[cf];
	float s2 = joint->scale[nf];

	float t = time - (int)time;
	basis = q1.lerp(q2, t);
	scale = vec3(s1 + (s2 - s1) * t);
	return true;
}

// Skeleton
Skeleton::Skeleton(Stream *stream) : joints(stream) {
	LOG("joints: %d\n", joints.count);
}

int Skeleton::getJointIndex(const char *jName) {
	for (int i = 0; i < joints.count; i++)
		if (String::equal(joints[i]->name, jName))
			return i;
	return -1;
}

// Skeleton::Joint
Skeleton::Joint::Joint(Stream *stream) : name(stream->readStr()), parent(stream->read<int>()), basis(stream->readQuat2()) {}

Skeleton::Joint::~Joint() {
	delete[] name;
}

// NavMesh
vec2 vecXZ(const vec3 &v) {
	return vec2(v.x, v.z);
}

float area(const vec3 &a, const vec3 &b, const vec3 &c) {
	vec2 ba = vecXZ(b - a);
	vec2 ca = vecXZ(c - a);
	return ba.cross(ca);
}

NavMesh::NavMesh(Stream *stream) {
	vertex	= stream->readArray<vec3>(vCount = stream->read<int>());
	face	= stream->readArray<Face>(fCount = stream->read<int>());
	edge	= stream->readArray<Edge>(eCount = stream->read<int>());

	stack  = new Index[fCount];
	parent = new Index[fCount];

	pathLen = 0;
}

NavMesh::~NavMesh() {
	delete[] vertex;
	delete[] face;
	delete[] edge;
	delete[] stack;
	delete[] parent;
}

Index NavMesh::getFaceIndex(const vec3 &pos) {
	for (int i = 0; i < fCount; i++) {
		Face &f  = face[i];
		vec3 &v0 = vertex[f.idx[0]];
		vec3 &v1 = vertex[f.idx[1]];
		vec3 &v2 = vertex[f.idx[2]];

		if (area(v0, v1, pos) <= 0 &&
			area(v1, v2, pos) <= 0 && 
			area(v2, v0, pos) <= 0)
			return i;
	}
	return 0xffff;
}

NavMesh::Path* NavMesh::findPath(const vec3 &start, const vec3 &end) {
// corridor
	int sFace = getFaceIndex(start);
	int eFace = getFaceIndex(end);

	if (sFace == 0xffff || eFace == 0xffff) 
		return NULL;

// clear parents links
	memset(parent, 0xff, fCount * sizeof(Index));

// find faces path
	int size = 0;
	stack[size++] = eFace;
	while (size > 0) {
		int cur = stack[0];
		size--;
		for (int i = 0; i < size; i++)
			stack[i] = stack[i + 1];

		if (cur == sFace) {
			size = 0;
			while (cur != eFace) {
				stack[size++] = cur;
				cur = parent[cur];
			}
			stack[size++] = cur;
			break;
		}
				
		for (int i = 0; i < 3; i++) {
			int n = face[cur].adj[i];
			if (n != 0xffff && parent[n] == 0xffff) {
				stack[size++] = n;
				parent[n] = cur;
			}
		}
	}

	if (size == 0)
		return NULL;

// find vertices path
	vec3 a, b, c, la, lb;		
	c = la = lb = start;

	int fa, fb;
	fa = fb = 0;

	Path *path = new Path(size + 2);

	path->add(start);
	for (int i = 0; i < size; i++) {
				
		if (i == size - 1) 
			a = b = end;
		else
			for (int j = 0; j < 3; j++) 
				if (face[stack[i]].adj[j] == stack[i + 1]) {
					a = vertex[face[stack[i]].idx[j]];
					b = vertex[face[stack[i]].idx[(j + 1) % 3]];
					vec3 d = (a - b).normal() * 0.5f;
					a = a - d;
					b = b + d;
					break;
				}
				
		if (path->count == size + 2) break;

		if (area(c, la, a) <= 0) {
			if (c == la || area(c, lb, a) > 0) {
				la = a;
				fa = i;
			} else {
				path->add(lb);
				c = la = lb;
				i = fa = fb;
				continue;
			}
		}

		if (area(c, lb, b) >= 0) {
			if (c == lb || area(c, la, b) < 0) {
				lb = b;
				fb = i;
			} else {
				path->add(la);
				c = lb = la;
				i = fb = fa;
				continue;
			}
		}

	}
	path->add(end);
	path->next();

	return path;
}

void NavMesh::collide(vec3 &pos, float radius) {
	float rq = radius * radius;

	for (int i = 0; i < eCount; i++) {
		vec3 a = vertex[edge[i].idx[0]];
		vec3 b = vertex[edge[i].idx[1]];

		vec3 ap = pos - a;
		vec3 ab = b - a;

		if (area(a, b, pos) > 0) continue;

		float t = clamp(ap.dot(ab) / ab.dot(ab), 0.0f, 1.0f);
		vec3 d = a + ab * t;

		vec2 h = vecXZ(pos - d);
		t = rq - h.length2();
		if (t > 0) {
			h = h.normal() * t;
			pos.x = pos.x + h.x;
			pos.z = pos.z + h.y;
		}
	}
}

/*
void NavMesh::debug(Index idx, NavMesh::Path *path) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glLineWidth(2.0f);
	glEnable(GL_LINE_SMOOTH);
	
	glDepthFunc(GL_LEQUAL);

// draw faces
	glBegin(GL_TRIANGLES);
		for (int i = 0; i < fCount; i++) {
			if (i == idx)
				glColor4f(0.6f, 0.5f, 0.3f, 0.5f);
			else
				glColor4f(0.3f, 0.5f, 0.6f, 0.5f);
			for (int j = 0; j < 3; j++)
				glVertex3fv((GLfloat*)&vertex[face[i].idx[j]]);
		}
	glEnd();

// path faces
	if (path) {
		glColor4f(0, 1, 0, 0.5f);
		glBegin(GL_TRIANGLES);
			for (int i = 0; i < pathLen; i++) {
				Face &f = face[stack[i]];
				for (int j = 0; j < 3; j++)
					glVertex3fv((GLfloat*)&vertex[f.idx[j]]);
			}
		glEnd();
	}

// draw face edges
	glColor4f(0.1f, 0.2f, 0.3f, 0.5f);
	glBegin(GL_LINES);
		for (int i = 0; i < fCount; i++) 
			for (int j = 0; j < 3; j++) {
				glVertex3fv((GLfloat*)&vertex[face[i].idx[j]]);
				glVertex3fv((GLfloat*)&vertex[face[i].idx[(j + 1) % 3]]);
			}
	glEnd();

// draw NavMesh edges
	glLineWidth(4.0f);
	glColor4f(1, 1, 1, 0.5f);
	glBegin(GL_LINES);
		for (int i = 0; i < eCount; i++)
			for (int j = 0; j < 2; j++)
				glVertex3fv((GLfloat*)&vertex[edge[i].idx[j]]);
	glEnd();

// draw path
	if (path) {
		glColor4f(0, 0, 1, 1);
		glBegin(GL_LINE_STRIP);
			glVertex3f(path->target.x, path->target.y + 0.7f, path->target.z);
			for (int i = 0; i < path->count; i++)
				glVertex3f(path->point[i].x, path->point[i].y + 0.7f, path->point[i].z);
		glEnd();
	}
}
*/

// NavMesh::Path
NavMesh::Path::Path(int count) : count(0), point(new vec3[count]) {}
NavMesh::Path::~Path() { delete[] point; }
			
void NavMesh::Path::add(const vec3 &p) {
	if (count && point[count - 1] == p) 
		return;
	point[count++] = p; 
}
			
bool NavMesh::Path::next() {
	if (!count) return false;

	target = point[0];
	count--;
	for (int i = 0; i < count; i++)
		point[i] = point[i + 1];
	return true;
}

Collider::Collider(Stream *stream) : basis(stream->readQuat2()), type(stream->read<int>()), maxRadius2(stream->read<float>()) {
	switch (type) {
		case BOX : 
			size = stream->read<vec3>();
			maxRadius2 = size.length2();
			break;
		case SPHERE : 
			radius = stream->read<float>();
			maxRadius2 = radius * radius;
			break;
		case MESH :
		case CONTOUR :
			indices = stream->readArray<Index>(iCount = stream->read<int>());
			vCount = stream->read<int>();
			if (type == MESH)
				mVertices = stream->readArray<vec3>(vCount);
			else
				cVertices = stream->readArray<vec2>(vCount);
			break;
	}
}

Collider::~Collider() {
	if (type == MESH || type == CONTOUR) {
		delete[] indices;
		if (type == MESH)		delete[] mVertices;
		if (type == CONTOUR)	delete[] cVertices;
	}
}

bool Collider::collide(const Sphere &sphere, vec3 &n, float &t) {
	// TODO: check max radius intersection

	switch (type) {
		case BOX :
			return Box(size).intersect(sphere, n, t);
		case SPHERE :
			return Sphere(radius).intersect(sphere, n, t);
		case MESH : {
			Sphere s(sphere);
			float tMax = -1.0f;
			vec3  nMax(0.0f);
			for (int i = 0; i < iCount; i += 3) {
				vec3 &a = mVertices[indices[i + 0]];
				vec3 &b = mVertices[indices[i + 1]];
				vec3 &c = mVertices[indices[i + 2]];
				Triangle tri(a, b, c);
				if (s.intersect(tri, n, t) && t > tMax) {
				//	s.center += n * t;
					tMax = t;
					nMax = n;
				}
			}
			
			if (tMax >= 0.0f) {
				n = nMax;
				t = tMax;
				return true;
			}
			break;
		}
		case CONTOUR : {
			vec2 sc = vec2(sphere.center.x, sphere.center.z);
			vec2 nn;

			Circle c(sc, sphere.radius);
			for (int i = 0; i < iCount; i += 2) {
				vec2 &a = cVertices[indices[i + 0]];
				vec2 &b = cVertices[indices[i + 1]];

				if (c.intersect(Segment2D(a, b), nn, t))
					c.center += nn * t;
			}
			
			if (sc != c.center) {
				sc = c.center - sc;
				n = vec3(sc.x, 0.0f, sc.y);
				t = n.length();
				n = n.normal();
				return true;
			}
			break;
		}
	}
	return false;
}


// Model::Resource
Model::Resource::Object::Object(Stream *stream) {
	stream->read(&type, (char*)&basis - (char*)&type);
	basis	= stream->readQuat2();
	scale	= stream->read<vec3>();
	name	= stream->readStr();
	
	if (type & OBJ_GEOMETRY) {
		stream->read(&bbox, sizeof(bbox));

		stream->read(&geom, sizeof(geom));
		stream->read(&tex, sizeof(tex));

		meshID = stream->read<int>();
		parts  = stream->readArray<MeshPart>(pCount = stream->read<int>());
		joints = stream->readArray<int>(jCount = stream->read<int>());

		colliders	= new Array<Collider>(stream);
	}

	if (type & OBJ_TRIGGER) {
		if (type & TRIG_BOX)	size = stream->read<vec3>();
		if (type & TRIG_SPHERE)	radius = stream->read<float>();
		links	= stream->readArray<int>(lCount = stream->read<int>());
		damage	= stream->read<int>();
		camera	= stream->read<int>();
	}

	if (type & OBJ_CAMERA) {
		fov		= stream->read<float>();
		znear	= stream->read<float>();
		zfar	= stream->read<float>();
		if (type & CAM_CURVE)
			curve = new Curve(stream);
	}

	if (type & OBJ_LIGHT) {
	//	vec3 p = basis.getPos();
	
	}
}

Model::Resource::Object::~Object() {
	delete[] name;
	if (type & OBJ_GEOMETRY) {
		delete[] parts;
		delete[] joints;
		delete colliders;
	}
	if (type & OBJ_TRIGGER)
		delete[] links;
	if (type & OBJ_CAMERA && type & CAM_CURVE)
		delete curve;
}

Model::Resource::Resource(Stream *stream, int param) : ResObject(stream, param), cameraID(stream->read<int>()), meshes(stream), materials(stream), animations(stream), objects(stream) {
	skeleton = stream->read<int>() ? new Skeleton(stream) : NULL;
	navmesh  = stream->read<int>() ? new NavMesh(stream) : NULL;
}

Model::Resource::~Resource() {
	delete skeleton;
	delete navmesh;
}

// Model::State
Model::State::State(Model::Resource *res) : objects(new Object[res->objects.count]), 
											animations(new Animation[res->animations.count]), 
											joints(res->skeleton ? new Joint[res->skeleton->joints.count] : NULL), 
											order(new int[res->objects.count]) {
	for (int i = 0; i < res->animations.count; i++)
		animations[i].id = i;

	for (int i = 0; i < res->objects.count; i++) {
		Model::State::Object &sobj = objects[i];
		Model::Resource::Object *obj = res->objects[i];
		obj->id = i;
		sobj.flags = obj->flags;
		sobj.basis = obj->basis;
		sobj.scale = obj->scale;
		sobj.time  = obj->time;
		order[i] = i;
	}
}

Model::State::~State() {
	delete[] objects;
	delete[] animations;
	delete[] joints;
	delete[] order;
}

// Model
Model::Model(const char *name) : res(Core::load<Resource>(name)), state(res), basis(quat2::IDENTITY) {}

Model::~Model() {
	Core::unload(res);
}

void Model::play(const char *name, float blendTime, float weight, bool loop) {
	for (int i = 0; i < res->animations.count; i++)		// TODO: animation stack index map
		if (String::equal(res->animations[state.animations[i].id]->name, name)) {
			State::Animation a = state.animations[i];
			if (i) {
			//	LOG("play anim: %s\n", name);
				a.time			= 0.0f;
				a.blendTimeMax	= blendTime;
				a.blendTimeCur	= blendTime;
				a.weight		= 0.0f;
				a.flags = Model::State::FLAG_PLAYING | (loop ? Model::State::FLAG_LOOP : 0);
				state.animations[0].blendTimeCur	= 0.0f;
				state.animations[0].weight			= 1.0f;
			}
			memmove(&state.animations[1], &state.animations[0], i * sizeof(a));
			state.animations[0] = a;
			break;
		}
}

#define FRAME_RATE (24.0f * 1.0f)

bool Model::isPlaying(float offset) {
	float maxTime = ((res->animations[state.animations[0].id]->frames - 1) * (1.0f / FRAME_RATE));
	return (state.animations[0].time + offset) < maxTime;
}

Model::Resource::Object* Model::getObject(const char *name) {
	for (int i = 0; i < res->objects.count; i++)
		if (String::equal(name, res->objects[i]->name))
			return res->objects[i];
	return NULL;
}

quat2& Model::getRelative(int jIndex) {
	float weight = 0.0f;
	vec3 scale;
	State::Joint& joint = state.joints[jIndex];

	for (int j = 0; j < res->animations.count && weight < 1.0f; j++) {
		Animation *animation = res->animations[state.animations[j].id];
		float time = state.animations[j].time * FRAME_RATE;

		quat2 dq;
		if (animation->getBasis(jIndex, time, dq, scale)) {
			float t = _min(1.0f - weight, state.animations[j].weight);
			if (weight == 0.0f)
				joint.relative = dq;
			else
				joint.relative = joint.relative.lerp(dq, t);
			weight = _min(1.0f, weight + state.animations[j].weight);
		}
	}
	
	if (weight < 1.0f)
		joint.relative = joint.relative.lerp(res->skeleton->joints[jIndex]->basis, 1.0f - weight);

	joint.updated = false;
	return joint.relative;
}

quat2& Model::getAbsolute(int jIndex) {
	State::Joint &joint = state.joints[jIndex];
	if (!joint.updated) {
		if (res->skeleton->joints[jIndex]->parent != -1)
			joint.absolute = getAbsolute(res->skeleton->joints[jIndex]->parent) * joint.relative;
		else
			joint.absolute = joint.relative;
		joint.updated = true;
	}
	return joint.absolute;
}

void Model::sort() {
	vec3 pos = Core::mView.inverse().getPos();
	vec3 dir = Core::mView.dir.xyz;

	for (int i = 0; i < res->objects.count; i++) {
		Model::Resource::Object *obj = res->objects[i];
		if ((obj->type & Model::Resource::Object::OBJ_GEOMETRY) && (res->materials[obj->parts[0].materialID]->options & (1 << oOpacity))) {
			obj->viewDist = (state.objects[i].basis * obj->bbox.getCenter() - pos).length2();
		//	obj->viewDist = dir.dot(pos - state.objects[i].basis * obj->bbox.getCenter());
		} else
			obj->viewDist = 1000000.0f;
	}

	int n = res->objects.count - 1;
	bool sorted = false;
	while (!sorted) {
		sorted = true;
		for (int i = 0; i < n; i++)
			if (res->objects[state.order[i]]->viewDist < res->objects[state.order[i + 1]]->viewDist) {
				int index = state.order[i];
				state.order[i] = state.order[i + 1];
				state.order[i + 1] = index;
				sorted = false;
			}
		n--;
	}
/*
	for (int i = res->objects.count - 1; i >= 0; i--)
		for (int j = 0; j < i; j++)
			if (res->objects[state.order[j]]->viewDist < res->objects[state.order[j + 1]]->viewDist) {
				int index = state.order[j];
				state.order[j] = state.order[j + 1];
				state.order[j + 1] = index;
			}
*/
}

void Model::update() {
	if (res->skeleton) {
		for (int i = 0; i < res->animations.count; i++) {
			State::Animation &a = state.animations[i];
			if (a.flags & State::FLAG_PLAYING) {

				a.time += Core::deltaTime;
				float maxTime = (res->animations[a.id]->frames - 1) * (1.0f / FRAME_RATE);

				if (!(a.flags & State::FLAG_LOOP)) {
					if (a.time > maxTime) {
						a.time = maxTime;
						a.flags &= ~State::FLAG_PLAYING;
					}
				} else
					if (a.time > maxTime)
						a.time -= maxTime;
					
				if (a.blendTimeCur > 0.0f) {
					a.blendTimeCur = _max(0.0f, a.blendTimeCur - Core::deltaTime);
					a.weight = 1.0f - a.blendTimeCur / a.blendTimeMax;
				} else
					a.weight = 1.0f;
			}
		}
	} else
		for (int i = 0; i < res->objects.count; i++) {
			State::Object &sobj = state.objects[i];
			Model::Resource::Object *obj = res->objects[i];
			
			if ((res->objects[i]->type & Model::Resource::Object::OBJ_TRIGGER) && sobj.time > 0.0f) {
				sobj.time -= Core::deltaTime;
				if (sobj.time <= EPS)
					sobj.time = -1.0f;	// trigger deactivate
			}

			if (obj->animationID > -1) {
				Animation *anim = res->animations[obj->animationID];

				if (sobj.flags & State::FLAG_PLAYING) {
					if (sobj.flags & State::FLAG_REWIND)
						sobj.time -= Core::deltaTime;
					else
						sobj.time += Core::deltaTime;

					float loopTime = (anim->frames) * (1.0f / FRAME_RATE);
					float maxTime = (anim->frames - 1) * (1.0f / FRAME_RATE);

					if (!(sobj.flags & State::FLAG_LOOP) || (sobj.flags & Model::State::FLAG_PONG)) {
					
						if ((sobj.flags & State::FLAG_REWIND) && sobj.time <= 0.0f) {
							sobj.time = 0.0f;
							sobj.flags &= ~State::FLAG_PLAYING;
						}

						if (!(sobj.flags & State::FLAG_REWIND) && sobj.time > maxTime) {
							sobj.time = maxTime;
							sobj.flags &= ~State::FLAG_PLAYING;
						}

						if (!(sobj.flags & State::FLAG_PLAYING) && (sobj.flags & Model::State::FLAG_PONG)) {
							if (sobj.flags & Model::State::FLAG_REWIND)
								sobj.flags &= ~Model::State::FLAG_REWIND;
							else
								sobj.flags |= Model::State::FLAG_REWIND;
							sobj.flags |= State::FLAG_PLAYING;
						}
					} else
						if (sobj.time < 0.0f)
							sobj.time += loopTime;
						else
							if (sobj.time > loopTime)
								sobj.time -= loopTime;
				};

				anim->getBasis(0, sobj.time * FRAME_RATE, sobj.basis, sobj.scale);

				if (!(anim->channels & (CHANNEL_POS | CHANNEL_ROT)))
					sobj.basis = obj->basis;
				else
					if (!(anim->channels & CHANNEL_POS))
						sobj.basis = quat2(quat(0, 0, 0, 1), obj->basis.getPos()) * sobj.basis;
					else
						if (!(anim->channels & CHANNEL_ROT))
							sobj.basis = quat2(obj->basis.getRot(), quat::ZERO) * sobj.basis;

				if (!(anim->channels & CHANNEL_SCALE))
					sobj.scale = obj->scale;
					
			} else {
				sobj.basis = obj->basis;
				sobj.scale = obj->scale;
			}
		}

	for (int i = 0; i < res->objects.count; i++)
		if (res->objects[i]->parentID > -1)
			state.objects[i].basis = state.objects[res->objects[i]->parentID].basis * state.objects[i].basis;

	if (res->skeleton) {
		for (int i = 0; i < res->skeleton->joints.count; i++)	
			getRelative(i);

		for (int i = 0; i < res->skeleton->joints.count; i++)
			getAbsolute(i);
	}
}

void Model::render() {
	for (int i = 0; i < res->objects.count; i++) {
		Resource::Object *obj = res->objects[state.order[i]];
		State::Object &sobj = state.objects[state.order[i]];

		if (!(res->objects[i]->type & Model::Resource::Object::OBJ_GEOMETRY))
			continue;

		if (!(sobj.flags & State::FLAG_VISIBLE))
			continue;

		ASSERT(obj->meshID > -1)

		Mesh *mesh = res->meshes[obj->meshID];

		for (int j = 0; j < obj->pCount; j++) {
			MeshPart &p = obj->parts[j];
			ASSERT(p.materialID > -1)
			
			Material *material = res->materials[p.materialID];

			if (!material->bind())
				continue;

			Shader *sh = material->shader[Core::mode];

			if (res->skeleton && obj->jCount > 0 && (material->options & (1 << oSkinning))) {

				quat2 joints[80];

				for (int i = 0; i < obj->jCount; i++) {
					int jIndex = obj->joints[i];
					joints[i] = state.joints[jIndex].absolute * res->skeleton->joints[jIndex]->basis.inverse();
				}

				sh->setParam(uJoint, *((vec4*)&joints[0]), obj->jCount * 2);
				Core::mModel = mat4(basis);
			} else
				if (String::equal(obj->name, "#wrench_weapon")) { // TODO
					int jIndex = res->skeleton->getJointIndex("weapon");
					if (jIndex != -1)
						Core::mModel = mat4(basis * state.joints[jIndex].absolute);
				} else {
					Core::mModel = mat4(basis * sobj.basis);
				/*
					if (sobj.scale != 1.0f) {
						Core::mModel.scale(vec3(sobj.scale));
						vec3 p = sobj.basis.getPos();
						LOG("%f %f %f\n", p.x, p.y, p.z);
					}
				*/
				}

			if (sobj.scale != vec3(1.0f))
				Core::mModel.scale(sobj.scale);

			sh->setParam(uModel, Core::mModel);

			Core::active.shader->setParam(uGeomScale, obj->geom.scale);
			Core::active.shader->setParam(uGeomBias, obj->geom.bias);
			Core::active.shader->setParam(uTexScale, obj->tex.scale);
			Core::active.shader->setParam(uTexBias, obj->tex.bias);


			if (material->options & (1 << oTwoSide)) {
				if ((material->options & (1 << oOpacity)) && Core::mode == rmNormal) {
					Core::setCulling(cfFront);
					mesh->render(p.iStart, p.iCount);
					Core::setCulling(cfBack);
					mesh->render(p.iStart, p.iCount);
				} else {
					Core::setCulling(cfNone);
					mesh->render(p.iStart, p.iCount);
				}
			} else {
				Core::setCulling(cfBack);
				mesh->render(p.iStart, p.iCount);
			}

		}


	}
}