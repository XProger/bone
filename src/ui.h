#ifndef H_UI
#define H_UI

namespace UI {
	extern void init();
	extern void free();
	extern void begin();
	extern void end();
	extern void drawQuad(float x, float y, float w, float h, float tx, float ty, float tw, float th);
	extern void flush();
}

#endif