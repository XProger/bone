#include <camera.h>

void Camera::setCamera(int type, const quat2 &basis, float fov, float znear, float zfar, float t, Curve *curve) {
	this->type	= type;
	basisLast	= this->basis;
	fovLast		= this->fov;
	basisBase	= basis;
	this->basis = basis;
	this->fov	= fov;
	this->znear	= znear;
	this->zfar	= zfar;
	this->t		= 1.0f;
	angle		= vec3::ZERO;
	pos			= basis.getPos();

	this->curve	= curve;
	time		= 0.0f;

	if (type == TYPE_CURVE)
		this->basis = mat4(curve->points[0], target.getPos(), vec3::Y).getBasis();
}

void Camera::update() {
	switch (type) {
		case TYPE_STATIC :
			basis = basisBase;
			break;
		case TYPE_TARGET :
			basis = mat4(basisBase.getPos(), target.getPos(), basisBase.getRot() * vec3::Y).getBasis();
			break;
		case TYPE_FOLLOW :
			basis = quat2(quat::IDENTITY, target.getPos()) * basisBase;
			break;
		case TYPE_CURVE  : {
			vec3 cp = basis.getPos();
			vec3 tp = target.getPos() + vec3::Y;

			targetTime = curve->closest(tp) - 0.05f;
			time = lerp(time, targetTime, Core::deltaTime * 10.0f);
			cp = curve->eval(time);

			/*
			vec3 tangent = curve->evalTangent(time).normal();
			vec3 dir = (tp - cp).normal();
			float dist = 3.0f;
			float s = (cp - tp).length() - dist;
			if (fabsf(s) > 1.0f) {
				time += tangent.dot(dir) * sign(s) * Core::deltaTime * 0.1f;
				time = clamp(time, 0.0f, 1.0f);
				cp = curve->eval(time);
			}
			*/

			/*
			float d = (tp - cp).length();
			float maxDist = 5.0f;

			float ct = time;
			float ttt = time;
			if (d < maxDist) {
				while (d < maxDist && ttt > 0.0f) {
					ttt = _max(0.0f, ttt - 0.00001f);
					vec3 p = bezier(points, pCount, ttt);
					float k = (tp - p).length();
					if (k > d) {
						d = k;
						cp = p;
						ct = ttt;
					}
				}
			} else 
				if (d > maxDist) {
					while (d > maxDist && ttt < 1.0f) {
						ttt = _min(1.0f, ttt + 0.00001f);
						vec3 p = bezier(points, pCount, ttt);
						float k = (tp - p).length();
						if (k < d) {
							d = k;
							cp = p;
							ct = ttt;
						}
					}
				}
			time = ct;
			*/
			basis = mat4(cp, tp, vec3::Y).getBasis();
			break;
		}
		case TYPE_FREE : {
			vec3 dir = basis.real * vec3(0, 0, -1);
			vec3 v = vec3(0, 0, 0);

			if (Input::down[ikW]) v += dir;
			if (Input::down[ikS]) v -= dir;
			if (Input::down[ikD]) v += dir.cross(vec3(0, 1, 0));
			if (Input::down[ikA]) v -= dir.cross(vec3(0, 1, 0));
			
			float speed = Input::down[ikSpace] ? 0.1f : 5.0f;

			pos += v.normal() * (Core::deltaTime * speed);

			basis = quat2(quat(vec3::Y, angle.y) * quat(vec3::X, angle.x), pos);
			break;
		}
	}

	if (t > 0.0f) {
		t -= Core::deltaTime;
		if (t > 0.0f)
			basis = basis.lerp(basisLast, t);
	}

	mView = mat4(basis.inverse());
	mProj = mat4(lerp(fov, fovLast, t), (float)Core::width / (float)Core::height, znear, zfar);
}

void Camera::setup() {
	Core::setMatrix(&mProj, &mView, NULL);
	Core::projRatio = vec3(1.0f - zfar / znear, 1.0f + zfar / znear, 0.0f) * 0.5f;
	Core::viewPos = Core::mView.inverse().getPos();
}

