#ifndef H_MATERIAL
#define H_MATERIAL

#include "utils.h"
#include "shader.h"
#include "texture.h"

struct Material {
	int		options;
	Shader	*shader[rmMAX];
	Texture	*texture[sMAX];

	struct {
		vec4 diffuse;
	} color;

	Material(Stream *stream);
	Material(const char *shaderName, const char *diffuseName);
	Material(const char *shaderName, Texture *color, Texture *normal, Texture *depth);
	~Material();
	bool bind();
};

#endif