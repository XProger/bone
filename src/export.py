import bpy
import struct
import bmesh
import math
import mathutils
import numpy
import os.path

from mathutils import Matrix
from mathutils import Vector
from bpy_extras.io_utils import axis_conversion

MatGL = Matrix(([-1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]))
#MatGL = axis_conversion('Y', 'Z', '-Z', 'Y').to_4x4()
InvMatGL = MatGL.inverted()

def add2dict(dict, value):
	if value == None:
		return -1;
	return dict.setdefault(value, len(dict))

def dict2list(dict):
	list = [None] * len(dict)
	for key in dict.keys():
		list[dict.get(key)] = key
	return list

def triangulate(mesh):
	bm = bmesh.new()
	bm.from_mesh(mesh)
	bmesh.ops.triangulate(bm, faces=bm.faces)
	tri_mesh = bpy.data.meshes.new(name="~temp~")
	#bm.transform(MatGL);
	bm.to_mesh(tri_mesh)
	tri_mesh.calc_tessface()
	tri_mesh.calc_tangents()
	bm.free()     
	return tri_mesh	

def writeInt(file, i):
	file.write(struct.pack('i', i))
	
def writeFloat(file, f):
	file.write(struct.pack('f', f))

def writeString(file, str):
	bstr = bytes(str, 'cp1252')
	file.write(struct.pack('B', len(bstr)))
	for b in bstr:
		file.write(struct.pack('B', b))

#def writeStringZ(file, str):
#	bstr = bytes(str, 'cp1252')
#	for b in bstr:
#		file.write(struct.pack('B', b))
#	file.write(struct.pack('B', 0))

def collectBones(armature):
	bones = {}
	boneByIndex = {} 
	boneByName = {} 
	i = 0
	for bone in armature.data.bones:
		if not bone.use_deform:
			continue	
	#	print(bone.name)
		bones[bone] = i
		boneByIndex[i] = bone
		boneByName[bone.name] = bone
		i = i + 1
	return bones, boneByIndex, boneByName

def clamp(x, a, b):
	if x < a:
		return a
	else:
		if x > b:
			return b
		else:
			return x

def getTransform(matrix):	
	m = MatGL * matrix * InvMatGL
		
	p = m.to_translation()
	r = m.to_quaternion()
	s = m.to_scale()
	r.normalize()
	if r.w < 0:
		r.x = -r.x
		r.y = -r.y
		r.z = -r.z
		
	return [r.x, r.y, r.z, p.x, p.y, p.z, s.x, s.y, s.z]

def writeTransform(file, matrix):
	qp = getTransform(matrix)
	for i in range(6):
		file.write(struct.pack('f', qp[i]))

class XMaterial:
	def __init__(self, object, material):
		self.object		= object
		self.material	= material
		has_light = False
		for uv in object.data.uv_textures:
			if uv.name.startswith('UVLight'):
				has_light = True
				break
		self.ambient = object.name + '_L' if has_light else None
	def __eq__(self, m):
		return isinstance(m, self.__class__) and self.object == m.object and self.material == m.material and self.ambient == m.ambient
	def __hash__(self):
		return hash((self.object, self.material, self.ambient))
	
	def write(self, file):
		material = self.material
		print('material: ', material.name)
			
		DEFS = {
			'skinning':		1,
			'shadow_cast':	2,
			'shadow_recv':	3,
			'env_sphere':	4,
			'opacity':		5,
			'twoside':		6,
			'diffuse':		7,
			'light':		8,
			'normal':		9,
		}

		defines = 0
		for prop in material.keys():
			d = DEFS.get(prop, -1)
			if d > -1:
				defines |= 1 << d;
				
		if material.use_cast_shadows:
			defines |= 1 << DEFS['shadow_cast']
		if material.use_shadows:
			defines |= 1 << DEFS['shadow_recv']
		
		#paths = [material.name.split('.', 1)[0] + '.xsh']
		paths = [material.get('fx', 'default') + '.xsh']
		
		colorDiffuse = (1, 1, 1, 1)

		for i in range(6):
			slot = material.texture_slots[i]
			if material.use_textures[i] and slot and slot.texture and slot.texture.type == 'IMAGE':# and slot.texture.image:
				path = slot.texture.image.filepath
				paths.append( bpy.path.basename(path)[0:-4] + '.pvr' )
				
				if slot.texture.use_normal_map:
					defines |= 1 << DEFS['normal']
			else:
				paths.append('')
		
		Diffuse_BSDF = material.node_tree.nodes.get('Diffuse BSDF') if material.node_tree else None
		if Diffuse_BSDF and len(Diffuse_BSDF.inputs) > 0:
			if len(Diffuse_BSDF.inputs[0].links) > 0:		
				path = Diffuse_BSDF.inputs[0].links[0].from_node.image.filepath
				paths[1] = bpy.path.basename(path)[0:-4] + '.pvr'
			colorDiffuse = Diffuse_BSDF.inputs[0].default_value

		if self.ambient:
			paths[2] = self.ambient + '.pvr'

		if paths[1] == '':
			defines |= 1 << DEFS['diffuse']
		
		if paths[4] != '':
			defines |= 1 << DEFS['light']
			defines &= ~(1 << DEFS['shadow_cast'])
			
		writeInt(file, defines);
		for f in colorDiffuse:
			file.write(struct.pack('B', int(f * 255) ))
			
		for path in paths:
			writeString(file, path)

class XMeshObject:
	def __init__(self, scale, bias, texScale, texBias, joints, parts):
		self.scale		= scale
		self.bias		= bias
		self.texScale	= texScale
		self.texBias	= texBias
		self.parts		= parts
		self.joints		= joints
		self.meshIndex	= -1

MAXVCACHE = 32

class XVertex:
	def __init__(self, index, data):
		self.index = index
		self.data = data
		self.uses = []
		self.score = 0.0
		self.index = -1
		self.cacherank = -1
		
	def calcScore(self):
		if self.uses:
			self.score = 2.0 * pow(len(self.uses), -0.5)
			if self.cacherank >= 3:
				self.score += pow(1.0 - float(self.cacherank - 3)/MAXVCACHE, 1.5)
			elif self.cacherank >= 0:
				self.score += 0.75
		else:
			self.score = -1.0;

class XMesh:
	def __init__(self):
		self.vertices	= []
		self.indices	= []
		self.meshes		= {}
		
	def append(self, object):
		vCount = len(self.vertices)
		mesh = object.data
	
		xobj = self.meshes.get(mesh, None)
		if xobj:
			return xobj
					
		if vCount + len(mesh.vertices) > 65536:
			return None
		
		parts = []
		for i in range(len(object.material_slots)):
			parts.append([])
		
		armature = object.parent if object.parent != None and object.parent.type == 'ARMATURE' else None
		obj = object
		groups = obj.vertex_groups
		print(obj.name, len(groups))
		
		if armature != None:
			bones, boneByIndex, boneByName = collectBones(armature)
		
		triMesh  = triangulate(mesh)
		triVert  = triMesh.vertices
		uvs		 = triMesh.tessface_uv_textures
		triUV0   = uvs[0].data if len(uvs) > 0 else None
		triUV1   = uvs[1].data if len(uvs) > 1 and object == None else None
		vertDict = {}
		
		tangents = [None] * len(triMesh.vertices)
		for loop in triMesh.loops:
			t = MatGL * loop.tangent
			tangents[loop.vertex_index] = (t.x, t.y, t.z, loop.bitangent_sign)
		
		for f in triMesh.tessfaces:
			for i in range(3):
				vidx = f.vertices[i]
				v  = triVert[vidx]
				vc = MatGL * v.co			# vertex coord
				vn = MatGL * v.normal		# vertex normal
				vt = tangents[vidx]
				t0 = triUV0[f.index].uv[i] if triUV0 else (0, 0)	# vertex texture coord 0
				t1 = triUV1[f.index].uv[i] if triUV1 else (0, 0)	# vertex texture coord 1

				bone1 = bone2 = 0
				weight = 1
				
				if armature != None:
					#divide = sum([w/255.0 for w in weights])                    
					#weights = [int(weight / divide) for weight in weights]
					
					vg = [g for g in v.groups if g.weight > 0]
					vg.sort(key = lambda g: g.weight, reverse = True)
					vg = vg[:2]
					
					if len(vg) > 0:
						bone1 = bone2 = vg[0].group
						if len(vg) == 1:
							weight = 1
						else:
							bone2 = vg[1].group
							weight = vg[0].weight / (vg[0].weight + vg[1].weight)
					else:
						raise TypeError('error: vertex has no weights')
						
				parts[f.material_index].append(add2dict(vertDict, (	vc.x, vc.y, vc.z, 
																	vn.x, vn.y, vn.z, 
																	vt[0], vt[1], vt[2], vt[3],
																	t0[0], t0[1], 
																	t1[0], t1[1], 
																	bone1, bone2, weight ) ))
		bpy.data.meshes.remove(triMesh)

		vertex = dict2list(vertDict)

		vx = [v[0] for v in vertex]
		vy = [v[1] for v in vertex]
		vz = [v[2] for v in vertex]
		bbox = (Vector((min(vx), min(vy), min(vz))), Vector((max(vx), max(vy), max(vz))))
		scale = (bbox[1] - bbox[0]) * 0.5 / 32767.0
		bias  = (bbox[1] + bbox[0]) * 0.5
			
		u1 = [v[10] for v in vertex]
		v1 = [v[11] for v in vertex]
		u2 = [v[12] for v in vertex]
		v2 = [v[13] for v in vertex]
		texBox = (Vector((min(u1), min(v1), min(u2), min(v2))), Vector((max(u1), max(v1), max(u2), max(v2))))
		texScale = (texBox[1] - texBox[0]) * 0.5 / 32767.0
		texBias  = (texBox[1] + texBox[0]) * 0.5
		
#		file.write(struct.pack('3f', scale.x, scale.y, scale.z))
#		file.write(struct.pack('3f', bias.x,  bias.y,  bias.z))	
#		file.write(struct.pack('4f', texScale.x, texScale.y, texScale.z, texScale.w))
#		file.write(struct.pack('4f', texBias.x,  texBias.y,  texBias.z,  texBias.w))	

#		file.write(struct.pack('I', len(index)))
#		for i in index:
#			file.write(struct.pack('H', i))

#		file.write(struct.pack('I', len(vertex)))
		for v in vertex:
	#		if armature != None:
			p = (	int((v[0] - bias.x) / scale.x) if scale.x != 0.0 else 0,
					int((v[1] - bias.y) / scale.y) if scale.y != 0.0 else 0,
					int((v[2] - bias.z) / scale.z) if scale.z != 0.0 else 0 ) 
			
			j0 = int(v[14]) * 2
			j1 = int(v[15]) * 2

			t = (	int((v[10] - texBias.x) / texScale.x) if texScale.x != 0.0 else 0,
					int((v[11] - texBias.y) / texScale.y) if texScale.y != 0.0 else 0,
					int((v[12] - texBias.z) / texScale.z) if texScale.z != 0.0 else 0,
					int((v[13] - texBias.w) / texScale.w) if texScale.w != 0.0 else 0 ) 
						
			nx = int((v[3] * 0.5 + 0.5) * 255.0)
			ny = int((v[4] * 0.5 + 0.5) * 255.0)
			nz = int((v[5] * 0.5 + 0.5) * 255.0)
			w  = int(v[16] * 255.0)
			
			tx = int((v[6] * 0.5 + 0.5) * 255.0)
			ty = int((v[7] * 0.5 + 0.5) * 255.0)
			tz = int((v[8] * 0.5 + 0.5) * 255.0)
			tw = int((v[9] * 0.5 + 0.5) * 255.0)
			
			self.vertices.append( (	p[0], p[1], p[2], j0 * 256 + j1,
									t[0], t[1], t[2], t[3],
									nx, ny, nz, w,
									tx, ty, tz, tw ) )

		joints = []
		for vg in groups:
			joints.append( bones[boneByName[vg.name]] )
		
	# add parts indices
		for p in range(len(parts)):
			iCount = len(self.indices)
			for i in range(len(parts[p])):
				self.indices.append( parts[p][i] + vCount )
			parts[p] = ( iCount, len(parts[p]), p )
		
	# remove empty parts
		p = len(parts)
		while p:
			p -= 1
			if parts[p][1] == 0:
				parts.pop(p)	
			
		xobj = XMeshObject(scale, bias, texScale, texBias, joints, parts)
		self.meshes[mesh] = xobj
		return xobj
	
	def optimize(self):
	# prepare
		self.verts = [None] * len(self.vertices)
		for i, v in enumerate(self.vertices):
			self.verts[i] = XVertex(i, v)
		
		self.tris = [] #[None] * (len(self.indices) // 3)
		for i in range(0, len(self.indices), 3):
			a = self.verts[self.indices[i + 0]]
			b = self.verts[self.indices[i + 1]]
			c = self.verts[self.indices[i + 2]]
			self.tris.append( (a, b, c)	)
	# Linear-speed vertex cache optimization algorithm by Tom Forsyth
		for v in self.verts:
			if v:
				v.index = -1
				v.uses = []
				v.cacherank = -1
		for i, (v0, v1, v2) in enumerate(self.tris):
			v0.uses.append(i)
			v1.uses.append(i)
			v2.uses.append(i)
		for v in self.verts:
			if v:
				v.calcScore()

		besttri = -1
		bestscore = -42.0
		scores = []
		for i, (v0, v1, v2) in enumerate(self.tris): 
			scores.append(v0.score + v1.score + v2.score)
			if scores[i] > bestscore:
				besttri = i
				bestscore = scores[i]

		vertloads = 0 # debug info
		vertschedule = []
		trischedule = []
		vcache = []
		while besttri >= 0:
			tri = self.tris[besttri]
			scores[besttri] = -666.0
			trischedule.append(tri)
			for v in tri:
				if v.cacherank < 0: # debug info
					vertloads += 1  # debug info
				if v.index < 0: 
					v.index = len(vertschedule)
					vertschedule.append(v)
				v.uses.remove(besttri)
				v.cacherank = -1
				v.score = -1.0
			vcache = [ v for v in tri if v.uses ] + [ v for v in vcache if v.cacherank >= 0 ]
			for i, v in enumerate(vcache):
				v.cacherank = i 
				v.calcScore()

			besttri = -1	
			bestscore = -42.0
			for v in vcache:
				for i in v.uses:
					v0, v1, v2 = self.tris[i]
					scores[i] = v0.score + v1.score + v2.score
					if scores[i] > bestscore:
						besttri = i
						bestscore = scores[i]
			while len(vcache) > MAXVCACHE:
				vcache.pop().cacherank = -1
			if besttri < 0:
				for i, score in enumerate(scores):
					if score > bestscore:
						besttri = i
						bestscore = score

		print('%s: %d verts optimized to %d/%d loads for %d entry LRU cache' % ('mesh', len(self.verts), vertloads, len(vertschedule), MAXVCACHE))
		#print('%s: %d verts scheduled to %d' % (self.name, len(self.verts), len(vertschedule)))
		self.verts = vertschedule
		# print('%s: %d tris scheduled to %d' % (self.name, len(self.tris), len(trischedule)))		 
		self.tris = trischedule
		
		self.indices = []
		for a, b, c in self.tris:
			self.indices.append(a.index)
			self.indices.append(b.index)
			self.indices.append(c.index)
			
		self.vertices = [None] * len(self.verts)
		for v in self.verts:
			self.vertices[v.index] = v.data
	
	def write(self, file):
	#	self.optimize()
		writeInt(file, len(self.indices))
		for i in self.indices:
			file.write(struct.pack('H', i))
		
		writeInt(file, len(self.vertices))
		for v in self.vertices:
			file.write(struct.pack('4h',  v[0],   v[1],  v[2],  v[3]))
			file.write(struct.pack('4h',  v[4],   v[5],  v[6],  v[7]))
			file.write(struct.pack('4B',  v[8],   v[9],  v[10], v[11]))
			file.write(struct.pack('4B',  v[12],  v[13], v[14], v[15]))
			

def writeAnimation(file, action, armature):
#	action = object.animation_data.action
	start, end = [int(x) for x in action.frame_range]
	if start < 1:
		start = 1		

	writeString(file, action.name)
	writeInt(file, end - start)

	btrans = []
				
	if armature != None:
		bones_sk, boneByIndex, boneByName = collectBones(armature)
		
		bones = []
		for group in action.groups:
			if armature.data.bones[group.name].use_deform:
				bones.append(group.name)
				btrans.append([])
		writeInt(file, len(bones_sk))

		oldAction = armature.animation_data.action
		armature.animation_data.action = action

		for frame in range(start, end):
			bpy.context.scene.frame_set(frame)
			bpy.context.scene.update()
			for i, name in enumerate(bones):
				bone = armature.pose.bones[name]
				m = bone.matrix.copy() # TODO: pm
				if bone.parent != None:
					pm = bone.parent.matrix.copy()
					pm.invert()
					m = pm * m
				btrans[i].append( getTransform(m) )
		armature.animation_data.action = oldAction
				
	else:
		writeInt(file, 1)
		btrans.append([])
		
		bpy.ops.mesh.primitive_cube_add()  
		obj = bpy.context.object
		obj.animation_data_create()
		obj.animation_data.action = action
		
		for frame in range(start, end):
			bpy.context.scene.frame_set(frame)
			bpy.context.scene.update()
		#	for i, group in enumerate(action.groups):
			btrans[0].append( getTransform(obj.matrix_local) )
		
		obj.animation_data.action = None
		bpy.context.scene.objects.unlink(obj)
		bpy.data.objects.remove(obj)
		
		bones = []
		bones.append(action.name)
		bones_sk = {}
		boneByIndex = {} 
		boneByName = {} 
		bones_sk[action] = 0
		boneByIndex[0] = action
		boneByName[action.name] = action

	if armature != None:		
		print('animation: ', action.name, 'frames:', end - start, 'bones:', len(btrans))
	else:
		print('animation: ', action.name, 'frames:', end - start)

	channels = 0

	for i in range(len(bones_sk)):
		index = -1
		name = boneByIndex[i].name
		for j, bone in enumerate(bones):
			if bone == name:
				index = j
				break

		if index == -1:
			writeInt(file, 0)
		else:
			trans = btrans[index]
		
			px = [qp[3] for qp in trans]
			py = [qp[4] for qp in trans]
			pz = [qp[5] for qp in trans]
			MinMax = (Vector((min(px), min(py), min(pz))), Vector((max(px), max(py), max(pz))))
			scale = (MinMax[1] - MinMax[0]) * 0.5 / 32767.0
			bias  = (MinMax[1] + MinMax[0]) * 0.5
			
			def cmp(a, b, rel_tol=1.0/32767.0, abs_tol=0.0):
			    return abs(a-b) <= rel_tol
			
			if cmp(MinMax[0].x, MinMax[1].x):
				scale.x = 0
				bias.x = MinMax[0].x
				
			if cmp(MinMax[0].y, MinMax[1].y):
				scale.y = 0
				bias.y = MinMax[0].y

			if cmp(MinMax[0].z, MinMax[1].z):
				scale.z = 0
				bias.z = MinMax[0].z

			for qp in trans:
				# TODO: qp[0..3] always positive, save as unsigned short (f * (64r - 1) + 0.5)
				qp[0] = int(qp[0] * 32767.0)
				qp[1] = int(qp[1] * 32767.0)
				qp[2] = int(qp[2] * 32767.0)
				qp[3] = int((qp[3] - bias.x) / scale.x) if scale.x != 0 else 0
				qp[4] = int((qp[4] - bias.y) / scale.y) if scale.y != 0 else 0
				qp[5] = int((qp[5] - bias.z) / scale.z) if scale.z != 0 else 0
						
			diff = []
			data = []
			lqp  = [0, 0, 0, 0, 0, 0, 1.0]
			for qp in trans:
				mask = 0
				for j in range(7):
					if abs(qp[j] - lqp[j]) > 0.0000001:
						mask |= 1 << j
						lqp[j] = qp[j]
						data.append(qp[j])
				diff.append(mask)		
				channels |= mask
			
			writeInt(file, len(trans))
			
			for mask in diff:
				file.write(struct.pack('B', mask))
				
			file.write(struct.pack('3f', scale.x, scale.y, scale.z))
			file.write(struct.pack('3f', bias.x,  bias.y,  bias.z))	
			for d in data:
				if d < -32767 or d > 32767:
					print('! short warning: ', d)
					if d < -32767:
						d = -32767
					else:
						d = 32767
				if isinstance(d, int):
					file.write(struct.pack('h', d))
				else:
					file.write(struct.pack('f', d))

	writeInt(file, channels)


def writeSkeleton(file, armature):
#	print('skeleton: ', armature.name)

	# get bones and indices
	bones, boneByIndex, boneByName = collectBones(armature)
	
	# saving bones names
	file.write(struct.pack('I', len(bones)))
	
	for i in range(len(bones)):
		bone = boneByIndex[i]
		if bone.parent == None:
			index = -1
		else:
			index = bones.get(bone.parent)
			if index == None:
				index = -1
				
		writeString(file, boneByIndex[i].name)
		writeInt(file, index)
		m = bone.matrix_local.copy()
		writeTransform(file, m);
#		writeTransformInvert(file, m);

	print('skeleton:', armature.name + '.' + armature.data.name, 'bones:', len(bones))

def writeNavMesh(file, navMesh):
	if navMesh == None:
		return

	mesh = navMesh.data
	mesh.calc_tessface()
	
	edges = []
	faces = []
	adj   = []
	
	def addEdge(edge):
		for e in edges:
			if e[0] == edge[1] and e[1] == edge[0]:
				edges.remove(e)
				return
		edges.append(edge)
		
	def getAdj(i0, i1):
		for f in range(len(faces)):
			for i in range(3):
				if faces[f][i] == i1 and faces[f][(i + 1) % 3] == i0:
					return f
		return 65535

	for f in mesh.tessfaces:
		idx = f.vertices
		faces.append([idx[0], idx[1], idx[2]])
		for i in range(3):
			addEdge([idx[i], idx[(i + 1) % 3]])
	
	for i in faces:
		adj.append([getAdj(i[0], i[1]), getAdj(i[1], i[2]), getAdj(i[2], i[0])])	
	
	file.write(struct.pack('i', len(mesh.vertices)))
	for v in mesh.vertices:
		p = MatGL * (navMesh.matrix_local * v.co)
		file.write(struct.pack('3f', p.x, p.y, p.z))
		
	file.write(struct.pack('i', len(faces)))
	for i in range(len(faces)):
		f = faces[i]
		a = adj[i]
		file.write(struct.pack('3H', f[0], f[1], f[2]))
		file.write(struct.pack('3H', a[0], a[1], a[2]))
		
	file.write(struct.pack('i', len(edges)))
	for e in edges:
		file.write(struct.pack('2H', e[0], e[1]))

	print('navmesh v:', len(mesh.vertices), 'f:', len(faces), 'e:', len(edges))

def getBoundBox(object):
	box = object.bound_box
	vx = []
	vy = []
	vz = []
	for v in box:
		p = MatGL * Vector(v)
		vx.append(p.x)
		vy.append(p.y)
		vz.append(p.z)
	return (min(vx), min(vy), min(vz), max(vx), max(vy), max(vz))

def getGeometry(mesh):
	indices		= []
	vertices	= {}
	for f in mesh.tessfaces:
		for i in range(3):
			v = mesh.vertices[f.vertices[i]].co
			indices.append(add2dict(vertices, (v.x, v.y, v.z) ))

	print('geometry: tris = ', len(indices) // 3, ' v = ', len(vertices))

	return indices, dict2list(vertices)

def getContour(mesh):
	edges		= {}
	
	def addEdge(a, b):
		if edges.get( (a, b) ):
			edges.pop( (a, b), None )
			return
		if edges.get( (b, a) ):
			edges.pop( (b, a), None )
			return
		edges[ (a, b) ] = True
	
	for f in mesh.tessfaces:
		addEdge(f.vertices[0], f.vertices[1])
		addEdge(f.vertices[1], f.vertices[2])
		addEdge(f.vertices[2], f.vertices[0])

	indices		= []
	vertices	= {}
	for e in edges:
		v = mesh.vertices[e[0]].co
		indices.append(add2dict(vertices, (v.x, v.y, v.z) ))
		v = mesh.vertices[e[1]].co
		indices.append(add2dict(vertices, (v.x, v.y, v.z) ))
	
	print('contour: edges = ', len(indices) // 2, ' v = ', len(vertices))
	
	return indices, dict2list(vertices)

def	writeCollider(file, collider):
	writeTransform(file, collider.matrix_local)

	TYPE_BOX		= 0
	TYPE_SPHERE		= 1
	TYPE_MESH		= 2
	TYPE_CONTOUR	= 3

	type = -1
	if collider.name.startswith('!box'):
		type = TYPE_BOX
	if collider.name.startswith('!sphere'):
		type = TYPE_SPHERE
	if collider.name.startswith('!mesh'):
		type = TYPE_MESH
	if collider.name.startswith('!contour'):
		type = TYPE_CONTOUR
		
	if type == -1:
		raise TypeError('collider "' + obj.name + '" has unknown type')
	
	file.write(struct.pack('if', type, 1000.0 * 1000.0))

	if type == TYPE_BOX:
		d = (MatGL * collider.dimensions) * 0.5
		file.write(struct.pack('3f', abs(d.x), abs(d.y), abs(d.z) ))
	
	if type == TYPE_SPHERE:
		d = collider.dimensions * 0.5
		file.write(struct.pack('f', max(d.x, d.y, d.z) ))
	
	if type == TYPE_MESH or type == TYPE_CONTOUR:
		mesh = triangulate(collider.data)
		indices = []
		vertices = []
		if type == TYPE_MESH:
			indices, vertices = getGeometry(mesh)
		if type == TYPE_CONTOUR:
			indices, vertices = getContour(mesh)

		#write indices	
		file.write(struct.pack('i', len(indices)))		
		for i in indices:
			file.write(struct.pack('H', i))
		
		#write vertices
		file.write(struct.pack('i', len(vertices)))		
		if type == TYPE_MESH:			
			for v in vertices:
				file.write(struct.pack('3f', v[0], v[1], v[2]))
		if type == TYPE_CONTOUR:
			for v in vertices:
				file.write(struct.pack('2f', v[0], v[2]))

#		vertex = mesh.vertices
#		s = MatGL * collider.scale
#		vx = [v.co.x * s.x for v in vertex]
		bpy.data.meshes.remove(mesh)

def getIndexByName(objects, name):
	for i, obj in enumerate(objects):
		if obj.name == name:
			return i
	return -1

def writeCurve(file, points):
	count = len(points)
	writeInt(file, count + count * 2 - 2)
	for i, p in enumerate(points):
	# left	
		if i > 0:
			n = MatGL * p.handle_left
			file.write(struct.pack('3f', n.x, n.y, n.z))				
	# center
		n = MatGL * p.co
		file.write(struct.pack('3f', n.x, n.y, n.z))
	# right
		if i < count - 1:
			n = MatGL * p.handle_right
			file.write(struct.pack('3f', n.x, n.y, n.z))

def writeObject(file, objects, object, xobject, materials, meshes, actionIndex, colliders):
	OBJ_GEOMETRY	= 0x1
	OBJ_TRIGGER		= 0x2
	OBJ_LIGHT		= 0x4
	OBJ_SOUND		= 0x8
	OBJ_CAMERA		= 0x10
	OBJ_CURVE		= 0x20
	
	TRIG_POINT		= 0x10000
	TRIG_BOX		= 0x20000
	TRIG_SPHERE		= 0x40000

	CAM_STATIC		= 0x10000
	CAM_TARGET		= 0x20000
	CAM_FOLLOW		= 0x40000
	CAM_CURVE		= 0x80000
	
	FLAG_VISIBLE	= 1
	FLAG_PLAYING	= 2
	FLAG_LOOP		= 4
	FLAG_PONG		= 8
	FLAG_REWIND		= 16
	FLAG_ACTION		= 32
	FLAG_DEACTIVATE = 64

	links	= []
	type	= -1
	names	= object.name.split('.')
	
	name = object.name if object.name[0] == '#' else ''

	if object.type == 'EMPTY':
		#if names[0] == 'trigger' or names[0] == '#trigger':
		type = OBJ_TRIGGER
		if object.empty_draw_type == 'CUBE':
			type |= TRIG_BOX
		else:
			if object.empty_draw_type == 'SPHERE':
				type |= TRIG_SPHERE
			else:
				type |= TRIG_POINT
				
		linkName = names[len(names) - 1]
		print('get links for trigger: ', linkName)
		for index, obj in enumerate(objects):
			if obj != object and obj.name.split('.')[0] == linkName:
				links.append(index)
				print("add link ", obj.name)
	else:
		if object.type == 'MESH':
			type = OBJ_GEOMETRY
		else:
			if object.type == 'LAMP':
				type = OBJ_LIGHT
			else:
				if object.type == 'SPEAKER':
					type = OBJ_SOUND
				else:
					if object.type == 'CAMERA':
						type = OBJ_CAMERA
						camType = object.get('type')
						if camType == 'target':
							type |= CAM_TARGET
						else:
							if camType == 'follow':
								if object.parent and object.parent.type == 'CURVE':
									type |= CAM_CURVE
								else:
									type |= CAM_FOLLOW									
							else:
								type |= CAM_STATIC
					else:
						type = OBJ_CURVE
		
	print('object: ', object.type, "\t", object.name, len(colliders))
	
	flags = FLAG_VISIBLE
	
	if object.get('autoplay'):
		flags |= FLAG_PLAYING
		
	if object.get('loop'):
		flags |= FLAG_LOOP
		
	if object.get('pong'):
		flags |= FLAG_PONG

	if object.get('rewind'):
		flags |= FLAG_REWIND

	if object.get('action'):
		flags |= FLAG_ACTION
		
	if object.get('deactivate'):
		flags |= FLAG_DEACTIVATE

	time = 0.0
	if object.get('start'):
		time = object.get('start') / 24.0
		
	parentIndex = -1
	if object.parent:
		for index, obj in enumerate(objects):
			if obj == object.parent:
				parentIndex = index
				break

	writeInt(file, type)
	writeInt(file, flags)	
	writeInt(file, parentIndex)
	writeInt(file, actionIndex)
	writeFloat(file, time)
	
	if type & OBJ_CAMERA and type & CAM_CURVE:
		m = object.matrix_world
	else:
		m = object.matrix_local
		
	if type & (OBJ_CAMERA | OBJ_LIGHT):
		m = m * axis_conversion('Y', 'Z', 'Z', 'Y').to_4x4()
	
	qp = getTransform(m)
	for f in qp:
		file.write(struct.pack('f', f))
	
	writeString(file, name)
	
	if type & OBJ_GEOMETRY:
	#	if meshIndex == -1:
	#		raise TypeError('object "' + object.name + '" has no mesh')
	#	if materialIndex == -1:
	#		raise TypeError('object "' + object.name + '" has no material')
		box = getBoundBox(object)
		for f in box:
			file.write(struct.pack('f', f))

		scale		= xobject.scale
		bias		= xobject.bias
		texScale	= xobject.texScale
		texBias		= xobject.texBias
		
		file.write(struct.pack('3f', scale.x, scale.y, scale.z))
		file.write(struct.pack('3f', bias.x,  bias.y,  bias.z))	
		file.write(struct.pack('4f', texScale.x, texScale.y, texScale.z, texScale.w))
		file.write(struct.pack('4f', texBias.x,  texBias.y,  texBias.z,  texBias.w))	

		writeInt(file, xobject.meshIndex)			
		writeInt(file, len(xobject.parts))
		for i, part in enumerate(xobject.parts):
			writeInt(file, materials[XMaterial(object, object.material_slots[part[2]].material)])	# material index
			writeInt(file, part[0] )				# start vertex index
			writeInt(file, part[1] )				# num vertex indices
			
		writeInt(file, len(xobject.joints))
		for j in xobject.joints:
			writeInt(file, j)
			
		writeInt(file, len(colliders))	
		for collider in colliders:
			writeCollider(file, collider)		
		
	if type & OBJ_TRIGGER:
		s = MatGL * (object.scale * object.empty_draw_size)
		s.x, s.y, s.z = abs(s.x), abs(s.y), abs(s.z)
		if type & TRIG_BOX:
			print('box')
			file.write(struct.pack('3f', s.x, s.y, s.z))
		if type & TRIG_SPHERE:
			print('sphere')
			file.write(struct.pack('f', max(s.x, s.y, s.z)))

		writeInt(file, len(links))	
		for link in links:
			writeInt(file, link)
			
		damage = object.get('damage')
		if damage == None:
			damage = 0
		writeInt(file, int(damage))
	
		camera = object.get('camera')
		if camera == None:
			camera = -1
		else:
			camera = getIndexByName(objects, camera)
		writeInt(file, int(camera))

	if type & OBJ_CAMERA:
		cam = object.data
		file.write(struct.pack('3f', cam.angle, cam.clip_start, cam.clip_end))
		if type & CAM_CURVE:
			writeCurve(file, object.parent.data.splines[0].bezier_points)

def getMesh(object):
	return object if object.type == 'MESH' else None

def getMaterial(object):
	if len(object.material_slots) == 0:
		return None
	print('add material for ', object.name)
	return XMaterial(object, object.material_slots[0].material)

def getAnimation(object):
	obj = object.parent if object.parent and object.parent.type == 'ARMATURE' else object
	return obj.animation_data.action if obj.animation_data else None

def bake(dir):
	scene = bpy.context.scene
	layer = scene.render.layers[0]
	layer.use_pass_diffuse_direct   = True
	layer.use_pass_diffuse_indirect = True
	layer.use_pass_diffuse_color    = False

	list = []
	for object in scene.objects:
		if object.type != 'MESH':
			continue
		has_light = False
		for uv in object.data.uv_textures:
			if uv.name.startswith('UVLight'):
				has_light = True
				break
		if not has_light:
			continue
		if os.path.exists(dir + object.name + '_L.png'):
			continue
		list.append(object)

	for i, object in enumerate(list):
		print('bake ', object.name, '(' + str(i + 1) + '/' + str(len(list)) + ')')
		
		#obj = bpy.data.objects['ground']
		object.select = True
		bpy.ops.object.duplicate()	
		object.select = False	
		obj = bpy.context.selected_objects[0]
		scene.objects.active = obj

		#scene.objects.active = obj
		obj.select = True

		for uv in obj.data.uv_textures:
			if uv.name.startswith('UVLight'):
				obj.data.uv_textures.active = uv
				break

		size = obj.data.uv_textures.active.name.split('.')
		if len(size) < 2:
			size = 128
		else:
			size = int(size[1])

		diffuse	= bpy.data.images.new('bake_diffuse', size, size)
		ambient = bpy.data.images.new('bake_ambient', size, size)
		light   = bpy.data.images.new('bake_light', size, size)

		mat = obj.material_slots[0].material

		mat.use_nodes = True
		nodes = mat.node_tree.nodes

		node = nodes.new(type="ShaderNodeTexImage")
		node.select = True
		nodes.active = node

		object.hide = True

		node.image = diffuse
		scene.update()
		bpy.ops.object.bake(type = 'DIFFUSE', use_selected_to_active=False, use_clear = True)
		
		node.image = ambient
		scene.update()
		bpy.ops.object.bake(type='AO', use_selected_to_active=False, use_clear = True)
		node.image = None
		
		object.hide = False

		#pixels = numpy.array(diffuse.pixels[:]) * numpy.array(ambient.pixels[:])
		pixels = numpy.array(ambient.pixels[:])

		light.pixels = pixels.tolist()

		light.file_format = 'PNG'
		light.filepath_raw = dir + object.name + '_L.png'
		light.save()

		node.select = False
		nodes.remove(node)
		nodes.active = None
		obj.data.uv_textures.active = obj.data.uv_textures[0]
		bpy.ops.object.delete()

		bpy.data.images.remove(diffuse)
		bpy.data.images.remove(ambient)
		bpy.data.images.remove(light)
		
	print('baking is complete!')

def exportModel(path):
	print("\nexport to ", path)

	for obj in bpy.context.selected_objects:
		obj.select = False

	area_type = bpy.context.area.type
#	bpy.context.area.type = 'VIEW_3D'
#	bpy.ops.view3d.snap_cursor_to_center()
	if bpy.context.scene.objects.active:
		bpy.ops.object.mode_set(mode = 'OBJECT')

	bake( os.path.dirname(path) + '/../conv/comp/' )
	
	materials = {}
	meshes = []
	animations = {}
	colliders = {}
	objects = []
	xobjects = {}
	navmesh = bpy.data.objects.get("Navmesh")


	armature = None
	# collect objects
	for obj in bpy.data.objects:#bpy.context.scene.objects:
#		, 'CURVE':6
		if {'NONE':0, 'EMPTY':1, 'MESH':2, 'LAMP':3, 'SPEAKER':4, 'CAMERA':5 }.get(obj.type, 0) == 0:
			continue
		if obj.name[0] == '!':
			if obj.parent:
				list = colliders.get(obj.parent.name)
				if not list:
					list = []
					colliders[obj.parent.name] = list
				list.append(obj)
			else:
				raise TypeError('collider "' + obj.name + '" has no parent')
				
		if not obj.is_visible(bpy.context.scene):
			continue
		if obj.parent != None and obj.parent.type == 'ARMATURE':
			if armature != None and armature.parent != obj.parent:
				raise TypeError('! more than one armature in scene')
			else:
				armature = obj
		
		if obj.type == 'MESH':
			xobj = None
			for i, mesh in enumerate(meshes):
				xobj = mesh.append(obj)
				if xobj:
					xobj.meshIndex = i
					break
				
			if xobj == None:
				mesh = XMesh()
				xobj = mesh.append(obj)
				if xobj == None:
					raise TypeError('! meshe have more than 65535 vertices ', obj.name)
				xobj.meshIndex = len(meshes)
				meshes.append(mesh)
		
			for part in xobj.parts:
				add2dict( materials, XMaterial(obj, obj.material_slots[part[2]].material) )

			xobjects[obj.name] = xobj

		objects.append(obj)

	for action in bpy.data.actions:
		if action.users and (action.frame_range[1] - action.frame_range[0]) > 1:
			add2dict(animations, action)

	# sort objects by name
	names = {}
	for obj in objects:
		names[obj.name] = obj;
	keys = sorted(names.keys())

	objects = []
	for key in keys:
		objects.append(names[key])

	file = open(path, "wb")
	
	if (bpy.context.scene.camera):
		writeInt(file, getIndexByName(objects, bpy.context.scene.camera.name))
	else:
		writeInt(file, -1)
		
	# write meshes
	writeInt(file, len(meshes))
	for mesh in meshes:
		mesh.write(file)

	# write materials
	list = dict2list(materials)
	writeInt(file, len(list))
	for material in list:
		material.write(file)

	# write animations
	curFrame = bpy.context.scene.frame_current
	list = dict2list(animations)
	writeInt(file, len(list))
	for animation in list:
		writeAnimation(file, animation, armature.parent if armature else None)
	bpy.context.scene.frame_set(curFrame)

	# write instances
	writeInt(file, len(objects))
	for obj in objects:
		writeObject(file, objects, obj,
			xobjects.get(obj.name, None),
			materials, 
			meshes,
			animations.get(getAnimation(obj), -1),
			colliders.get(obj.name, []))

	# write skeletons
	if armature != None:
		writeInt(file, 1)	# Character
		writeSkeleton(file, armature.parent)
	else:
		writeInt(file, 0)	# Level
		
	if navmesh != None:
		writeInt(file, 1)
		writeNavMesh(file, navmesh)
	else:
		writeInt(file, 0)

	file.close()
	
	print('\nTotal:')
	if armature != None:
		print('type: Character')
	else:
		print('type: Level')
	print('meshes     : ', len(meshes))
	print('materials  : ', len(materials))
	print('animations : ', len(animations))
	print('colliders  : ', len(colliders))
	print('objects    : ', len(objects))

	bpy.context.area.type = area_type

	
exportModel("D:/Projects/X5/bin/data/hero.xmd")