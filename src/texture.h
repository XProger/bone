#ifndef H_TEXTURE
#define H_TEXTURE

#include "core.h"

enum TextureFilter { tfNone, tfBilinear, tfTrilinear, tfAniso };
enum SamplerFormat { sfRGBA, sfRGB, sfDepth, sfShadow };

struct Texture : ResObject {
	GLuint	ID;
	int		width, height;
	bool	depth;

	Texture(Stream *stream, int param);
	Texture(int width, int height, SamplerFormat format = sfRGBA, void *data = NULL);
	virtual ~Texture();
	void bind(int sampler);
	void setFilter(TextureFilter filter);
};

#endif