#include "ui.h"
#include "core.h"

namespace UI {

	#define BUF_SIZE 4096

	struct Vertex {
		vec2 coord;
		vec2 texCoord;
		Vertex() {}
		Vertex(float x, float y, float tx, float ty) : coord(x, y), texCoord(tx, ty) {}
	} *vertices;

	int		count;
	Mesh	*buffer;

	void init() {
		vertices = new Vertex[BUF_SIZE];
		Index *indices  = new Index[BUF_SIZE / 4 * 6];
		Index *idx = indices;
		for (int i = 0; i < BUF_SIZE; i += 4) {
			*idx++ = i + 0;
			*idx++ = i + 1;
			*idx++ = i + 2;
			*idx++ = i + 0;
			*idx++ = i + 2;
			*idx++ = i + 3;
		}
		buffer = new Mesh(indices, NULL, BUF_SIZE / 4 * 6, BUF_SIZE, sizeof(Vertex));
		delete[] indices;
		count = 0;
	}

	void free() {
		delete buffer;
		delete[] vertices;
	}

	void begin() {
		glDisable(GL_DEPTH_TEST);

		mat4 mView, mProj(0.0f, (float)Core::width, (float)Core::height, 0.0f, -1.0f, 1.0f);
		mView.identity();
		Core::setMatrix(&mProj, &mView, NULL);
		Core::mModel.identity();

		buffer->bind();

		for (int at = aTexCoord + 1; at < aMAX; at++)
			glDisableVertexAttribArray(at);
	}

	void end() {
		flush();
		for (int at = aTexCoord + 1; at < aMAX; at++)
			glEnableVertexAttribArray(at);
		glEnable(GL_DEPTH_TEST);
	}

	void drawQuad(float x, float y, float w, float h, float tx, float ty, float tw, float th) {
		vertices[count++] = Vertex(x    , y + h, tx     , ty + th);
		vertices[count++] = Vertex(x + w, y + h, tx + tw, ty + th);
		vertices[count++] = Vertex(x + w, y    , tx + tw, ty     );
		vertices[count++] = Vertex(x    , y    , tx     , ty     );
		if (count == BUF_SIZE)
			flush();
	}

	void flush() {
		if (!count) return;
		
		buffer->update(NULL, vertices, 0, count);

		Vertex *v = NULL;
		glVertexAttribPointer(aCoord,    2, GL_FLOAT, false, sizeof(Vertex), &v->coord);
		glVertexAttribPointer(aTexCoord, 2, GL_FLOAT, false, sizeof(Vertex), &v->texCoord);
		glDrawElements(GL_TRIANGLES, count / 4 * 6, GL_UNSIGNED_SHORT, 0);

		count = 0;
	}
}
