#include "font.h"
#include "ui.h"

Font::Font(const char *name) {
	material = new Material("font.xsh", name);
	material->texture[sDiffuse]->setFilter(tfNone);
}

Font::~Font() {
	delete material;
}

float Font::getWidth(const char *text) {
	return text ? strlen(text) * 8.0f : 0.0f;
}

void Font::print(const vec2 &pos, const vec4 &color, const char *text) {
	material->color.diffuse = color;
	material->bind();

	float x = pos.x, y = pos.y;
	float w = 1.0f / (float)material->texture[sDiffuse]->width;
	float h = 1.0f / (float)material->texture[sDiffuse]->height;

	const char *c = text;
	while (*c) {
		if (*c == '\n') {
			x =  pos.x;
			y += 16.0f;
		} else {
			float tx = (*c % 16) * 8.0f * w;
			float ty = 1.0f - (*c / 16) * 16.0f * h;
			UI::drawQuad(x, y, 8, 16, tx, ty, 8.0f * w, -16.0f * h);
			x += 9.0f;
		}
		c++;
	}
	UI::flush();
}
