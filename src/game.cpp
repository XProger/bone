#include "core.h"
#include "level.h"
#include "enemy.h"
#include "physics.h"
#include "font.h"
#include "debug.h"

#define BLEND_TIME	0.5f

//#define TEST_PHYSICS
//#define TEST_VR
//#define USE_OPENVR

#ifdef TEST_VR
	#ifdef USE_OPENVR
		#include <openvr.h>
	#endif
#endif

namespace Game {

#ifdef TEST_PHYSICS
	Model *plane;
	Model *box, *cylinder, *sphere, *cone, *capsule;
	World *world;
	Texture *shadow;

	Font	*font;

	vec3 pos, rot;
	vec2 mLast;

	#define BOX_COUNT 10+9+8+7+6+5+4+3+2+1

	#define BOX_COLS 10
	#define BOX_ROWS 10

	struct GameObject {
		GameObject *next;
		Body body;

		~GameObject() {
			delete next;
		}
	};

	Body ground;

	List<GameObject> objects;

	GameObject *mObject;

	float dt;

	float getMass(const Body &body) {
		switch (body.type) {
			case Body::BOX		: return (body.size.x * 2.0f) * (body.size.y * 2.0f) * (body.size.z * 2.0f);
			case Body::CYLINDER : return PI * body.radius * body.radius * body.height * 2.0f;
			case Body::SPHERE	: return PI * body.radius * body.radius * body.radius * 4.0f / 3.0f;
			case Body::CONE		: return PI * body.radius * body.radius * body.height / 3.0f;
			case Body::CAPSULE	: return PI * body.radius * body.radius * (body.height * 2.0f + body.radius * 4.0f / 3.0f);
		}
		return 1.0f;
	};

	void init() {
		Core::init();

		font = new Font("font.pvr");

		pos = vec3(0.0f, 7.0f, 10.0f);
		rot = vec3(0.0f);

		plane = new Model("plane.xmd");
		box			= new Model("box.xmd");
		cylinder	= new Model("cylinder.xmd");
		sphere		= new Model("sphere.xmd");
		cone		= new Model("cone.xmd");
		capsule		= new Model("capsule.xmd");

		world = new World(vec3(0.0f, -9.81f, 0.0f));
		
		float dx = -BOX_COLS * 1.0f * 0.5f;
		int i = 0;
		for (int k = 0; k < BOX_ROWS; k++) {
			for (int j = 0; j < BOX_COLS - k; j++) {
				GameObject *obj = new GameObject();
				obj->body = Body::Box(quat2(quat::IDENTITY, vec3(dx + j, k + 0.5f, -5.0f)), vec3(0.5f));
				obj->body.setMass(getMass(obj->body));
				obj->body.userData = objects.add(obj);
				world->add(&obj->body);
				i++;
			}
			dx += 0.55f;
		}
		
		for (int i = 0; i < 10; i++) {
			GameObject *obj = new GameObject();
			obj->body = Body::Box(quat2(quat::IDENTITY, vec3(i * 3.0f - (5 * 3.0f) + 1.5f, 2.0f, 4.0f)), vec3(0.2f, 2.0f, 1.0f));
			obj->body.setMass(getMass(obj->body));
			obj->body.userData = objects.add(obj);
			world->add(&obj->body);
		}
		
		/*
		for (int i = 0; i < 10; i++) {
			GameObject *obj = new GameObject();
			obj->body = Body::Box(quat2(quat::IDENTITY, vec3(-0.5f, (float)i * 1.1f + 0.5f, -0.5f)), vec3(0.5f));
			obj->body.setMass(1.0f);
			objects.add(obj);
			world->add(&obj->body);
		}
		*/

		ground = Body::Box(quat2(quat::IDENTITY, vec3(0.0f, -0.5f, 0.0f)), vec3(20.0f, 0.5f, 20.0f));
		ground.setMass(0.0f);
		world->add(&ground);

		dt = 0.0f;

		mObject = NULL;

		shadow = new Texture(2048, 2048, true);
	}

	void free() {
		delete font;
		delete objects.first;
		delete world;
		delete plane;
		delete box;
		delete cylinder;
		delete sphere;
		delete cone;
		delete capsule;
		delete shadow;
		Core::free();
	}

	vec3 camDir() {
		return vec3(sinf(rot.y - PI) * cosf(-rot.x), -sinf(-rot.x), cosf(rot.y - PI) * cosf(-rot.x));
	}

	void camUpdate() {
		vec3 dir = camDir();
		vec3 v = vec3(0, 0, 0);

		if (Input::down[ikW]) v += dir;
		if (Input::down[ikS]) v -= dir;
		if (Input::down[ikD]) v += dir.cross(vec3(0, 1, 0));
		if (Input::down[ikA]) v -= dir.cross(vec3(0, 1, 0));

		pos += v.normal() * (Core::deltaTime * 10.0f);
	}

	void camSetup() {
		mat4 mView;
		mView.identity();// = mat4((target * basis).inverse());
		mView.rotateZ(-rot.z);
		mView.rotateX(-rot.x);
		mView.rotateY(-rot.y);
		mView.translate(-pos);

		mat4 mProj(90.0f, (float)Core::width / (float)Core::height, 0.1f, 100.0f);
		Core::setMatrix(&mProj, &mView, NULL);

		Core::setViewport(0, 0, Core::width, Core::height);
	}
	
	GameObject *getObject() {
		GameObject *obj = NULL;
		float minDist = 1000.0f * 1000.0f;
		float range = cosf(20.0f * DEG2RAD);
		vec3 dir = camDir();
		Body *body = world->list.first;
		while (body) {
			if (body->userData) {
				vec3  v = body->pos - pos;
				float d = v.length2();
				if (d < minDist && dir.dot(v.normal()) > range) {
					minDist = d;
					obj = (GameObject*)body->userData;
				}
			}
			body = body->next;
		}
		return obj;
	}

	#define STEP (1.0f / 60.0f)

	void update() {
		camUpdate();

		if (mObject) {
			vec3 p = pos + camDir() * 2.0f;
			if (mObject->body.pos != p) {
				mObject->body.velocity.angular = vec3(0.0f);
				mObject->body.velocity.linear = (p - mObject->body.pos) * 10.0f;
				mObject->body.sleeping = false;
			}
		}


		dt += Core::deltaTime;
		while (dt > STEP) {
			world->simulate(STEP);
			dt -= STEP;
		}

//		world->simulate(1.0f / 10000.0f);
	}

#ifdef WIN32
	void debug() {
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf((GLfloat*)&Core::mProj);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf((GLfloat*)&Core::mView);
		glLineWidth(2);
		glPointSize(8);
		glDepthFunc(GL_LEQUAL);

		glDisable(GL_DEPTH_TEST);
		glUseProgram(0);
		Core::active.shader = NULL;

		World::Arbiter *arb = world->arbiters.first;
		while (arb) {
			Body *a = arb->a;
			Body *b = arb->b;

			World::Constraint *c = arb->constraints.first;
			while (c) {

				vec3 pa = a->pos + a->rot * c->oa;
				vec3 pb = b->pos + b->rot * c->ob;
				vec3 na = pa - c->n * 0.5f;
				vec3 nb = pb + c->n * 0.5f;

				glBegin(GL_LINES);
					glColor3f(1, 0, 0);
					glVertex3f(pa.x, pa.y, pa.z);
					glVertex3f(pb.x, pb.y, pb.z);
					if (a->sleeping && b->sleeping)
						glColor3f(0, 0, 1);
					else
						glColor3f(0, 1, 0);
					glVertex3f(pa.x, pa.y, pa.z);
					glVertex3f(na.x, na.y, na.z);
				glEnd();
			
/*
				glBegin(GL_POINTS);
					glColor3f(0, 1, 0);
					glVertex3f(p.x, p.y, p.z);
					glColor3f(0, 0, 1);
					glVertex3f(ra.x, ra.y, ra.z);
					glVertex3f(rb.x, rb.y, rb.z);
				glEnd();
*/			
				c = c->next;
			}
			arb = arb->next;
		}
		glEnable(GL_DEPTH_TEST);

	}
#endif

	int visible, count, active;

	void renderPass() {
		GameObject *obj = objects.first;
		while (obj) {
			Model *model = NULL;
			switch (obj->body.type) {
				case Body::BOX		: model = box;		break;
				case Body::CYLINDER	: model = cylinder;	break;
				case Body::SPHERE	: model = sphere;	break;
				case Body::CONE		: model = cone;		break;
				case Body::CAPSULE	: model = capsule;	break;
			}

			if (!obj->body.sleeping)
				active++;

			model->basis = obj->body.getBasis();
			if (Core::isVisible(model->basis, obj->body.maxRadius)) {
				if (obj->body.type == Body::BOX)
					model->state.objects[0].scale = obj->body.size * 2.0f;
				model->render();
				visible++;
			}
			count++;

			obj = obj->next;
		}

		plane->basis = ground.getBasis();
		plane->render();
	}

	void setupLightCamera() {
		float SIZE = 20.0f * SQRT2;

		vec3 pos = vec3(0.0f);
		mat4 mView(Core::lights.direction * SIZE + pos, pos, vec3(0, 1, 0));
		mat4 mProj(-SIZE, SIZE, -SIZE, SIZE, 0.1f, 100.0f);
		Core::setMatrix(&mProj, &mView, NULL);

		mat4 bias;
		bias.identity();
		bias.e03 = bias.e13 = bias.e23 = bias.e00 = bias.e11 = bias.e22 = 0.5f;

		Core::lights.mViewProj = bias * mProj * mView;
	}

	void renderUI() {
		UI::begin();

		char str[128];
		str[0] = 0;
		sprintf(str, "objects : %d\nactive  : %d\nvisible : %d\n", count, active, visible);
		font->print(vec2(32.0f), vec4(0.0f, 0.0f, 0.0f, 1.0f), str);

		UI::end();
	}

	void render() {
		Core::clear(vec4(0.376f, 0.741f, 0.812f, 0.0f));
		Core::mode = rmNormal;
		Core::lights.direction = vec3(1.0f).normal();
		Core::lights.ambient = vec3(0.376f, 0.741f, 0.812f) * 0.5f;//vec3(0.2f, 0.3f, 0.4f);

		glDisable(GL_BLEND);
		Core::mode = rmShadow;
		Core::whiteTex->bind(sShadow);
		Core::setTarget(shadow);
		Core::setViewport(0, 0, shadow->width, shadow->height);
		Core::clear(vec4(1.0));
		setupLightCamera();
		renderPass();
		Core::setTarget(NULL);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		Core::mode = rmNormal;
		shadow->bind(sShadow);
		Core::setViewport(0, 0, Core::width, Core::height);
		Core::clear(vec4(0.376f, 0.741f, 0.812f, 0.0f));
		camSetup();
	//	Core::mViewProj = Core::lights.mViewProj;
		count = visible = active = 0;
		renderPass();
//		LOG("visible: %d\\%d\n", visible, count);


	#ifdef WIN32
	//	debug();
	#endif

		renderUI();
	}

	void input(InputKey key, InputState state) {
		if (state == isDown) {

			if (key == ikMouseL || key == ikMouseR) {
				mLast = Input::mouse.pos;

				if (key == ikMouseR && (mObject = getObject())) {
				//	mObject->body.velocity.linear  = vec3(0.0f);
				//	mObject->body.velocity.angular = vec3(0.0f);
				//	mObject->body.setMass(0.0f);
				}
				return;
			}


			Body body;
			quat2 dq = quat2(quat::IDENTITY, pos);
			float r = 0.5f + randf() * 0.5f;
			float h = 0.5f + randf();
			switch (key) {
				case ik1 : body = Body::Box(dq, vec3(0.5f));		break;
				case ik2 : body = Body::Cylinder(dq, 0.5f, 0.5f);	break;
				case ik3 : body = Body::Sphere(dq, 0.5f);			break;
				case ik4 : body = Body::Cone(dq, 0.5f, 1.0f);		break;
				case ik5 : body = Body::Capsule(dq, 0.5f, 0.5f);	break;
				default : return;
			}

			GameObject *obj = new GameObject();
			obj->body = body;
			obj->body.setMass(getMass(body));
			obj->body.velocity.linear = camDir() * 10.0f;
			obj->body.userData = objects.add(obj);
			world->add(&obj->body);
		} else
			if (state == isUp) {
				if (key == ikMouseR && mObject) {
					mObject->body.setMass(getMass(mObject->body));
					mObject->body.velocity.linear = camDir() * 20.0f;
					mObject->body.sleeping = false;
					mObject = NULL;
				}
			} else
				if (state == isMove && key == ikMouse && (Input::mouse.L.down || Input::mouse.R.down)) {
					vec2 delta = Input::mouse.pos - mLast;
					rot.x -= delta.y * 0.01f;
					rot.y -= delta.x * 0.01f;
					rot.x = _min(_max(rot.x, -PI * 0.5f + EPS), PI * 0.5f - EPS);
					mLast = Input::mouse.pos;
				}
	}

#elif defined(TEST_VR)
	Level	*level;
	Enemy	*tank, *wrench;

	Material *composer;
	Mesh *quad;

	#ifdef USE_OPENVR
	Texture	*eyes[2];
	vr::Texture_t eyeComp[2];
	vr::IVRSystem *hmd;
	vr::TrackedDevicePose_t tPose[vr::k_unMaxTrackedDeviceCount];

	mat4 VR_matrix(const vr::HmdMatrix44_t &m) {
		//return *(mat4*)&m.m;
		return mat4(m.m[0][0], m.m[1][0], m.m[2][0], m.m[3][0],
					m.m[0][1], m.m[1][1], m.m[2][1], m.m[3][1], 
					m.m[0][2], m.m[1][2], m.m[2][2], m.m[3][2], 
					m.m[0][3], m.m[1][3], m.m[2][3], m.m[3][3]);
	}
	
	mat4 VR_matrix(const vr::HmdMatrix34_t &m) {
		return mat4(
			m.m[0][0], m.m[1][0], m.m[2][0], 0.0f,
			m.m[0][1], m.m[1][1], m.m[2][1], 0.0f,
			m.m[0][2], m.m[1][2], m.m[2][2], 0.0f,
			m.m[0][3], m.m[1][3], m.m[2][3], 1.0f
		);
	}

	void VR_init() {
		vr::EVRInitError err = vr::VRInitError_None;
		hmd = vr::VR_Init(&err, vr::VRApplication_Scene);

		if (err != vr::VRInitError_None) {
			hmd = NULL;
			LOG("! VR: %s\n", vr::VR_GetVRInitErrorAsEnglishDescription(err));
			return;
		}

		if (!vr::VRCompositor()) {
			hmd = NULL;
			vr::VR_Shutdown();
			LOG("! VR: compositor initialization failed\n");
			return;
		}

		uint32_t width, height; 
		hmd->GetRecommendedRenderTargetSize(&width, &height);
		for (int i = 0; i < 2; i++)
			eyes[i] = new Texture(width, height);
	}

	void VR_free() {
		if (!hmd) return;
		vr::VR_Shutdown();
		for (int i = 0; i < 2; i++)
			delete eyes[i];
	}

	void VR_update() {
		if (!hmd) return;

		vr::VREvent_t event;
		while (hmd->PollNextEvent(&event, sizeof(event))) {
			switch(event.eventType) {
			case vr::VREvent_TrackedDeviceActivated: 
//				SetupRenderModelForTrackedDevice( event.trackedDeviceIndex );
			//	LOG("VR: attach %d\n", event.trackedDeviceIndex);
				break;
			case vr::VREvent_TrackedDeviceDeactivated:
			//	LOG("VR: detacch %d\n", event.trackedDeviceIndex);
				break;
			case vr::VREvent_TrackedDeviceUpdated:
			//	LOG("VR: update %d\n", event.trackedDeviceIndex);
				break;
			}
		}

		// Process SteamVR controller state
		for (vr::TrackedDeviceIndex_t i = 0; i < vr::k_unMaxTrackedDeviceCount; i++) {
			vr::VRControllerState_t state;
		//	if(hmd->GetControllerState(i, &state))
		//		Input::down[HMD + i] = state.ulButtonPressed == 0;
		}
	}
	#endif

	void init() {
		Core::init();

		#ifdef USE_OPENVR
		VR_init();
		#endif

		level	= new Level("sandbox_vr.xmd");

		level->camera.type = Camera::TYPE_FREE;
		
		tank	= new Enemy("tank.xmd");
		wrench	= new Enemy("wrench.xmd");

		tank->model->play("idle", 0.0f, 1.0f, true);
		wrench->model->play("idle1", 0.0f, 1.0f, true);

		tank->setBasis(quat2(quat(vec3::Y, 0),  vec3(0, 0,  2)));
		wrench->setBasis(quat2(quat(vec3::Y, PI), vec3(0, 0, -2)));
		
		level->add(tank);
		level->add(wrench);
		
		level->camera.znear = 0.01f;
		level->camera.zfar  = 50.0f;
		level->camera.pos   = vec3(5.0f);
		level->camera.angle = vec3(-PI * 0.15f, PI * 0.25f, 0);

		composer = new Material("composer.xsh",
								new Texture(1280, 720), // color
								new Texture(1280, 720), // normal
								new Texture(1280, 720, sfDepth)); // depth

		Index indices[] = { 0, 2, 1, 0, 3, 2 };
		Vertex2 vertices[4];
	
		quad = new Mesh(indices, vertices, 6, 4, sizeof(Vertex2));
	}

	void free() {
		#ifdef USE_OPENVR
		VR_free();
		#endif
		delete composer;
		delete quad;
		delete level;
		Core::free();
	}

	void update() {
		#ifdef USE_OPENVR
		VR_update();
		#endif
		level->update();
	}

	void render() {
		/*
		if (hmd) {
			mat4 mHead;
			if (tPose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
				mHead = VR_matrix(tPose[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking).inverse();
			else
				mHead.identity();

			level->camera.mView = VR_matrix(hmd->GetEyeToHeadTransform(vr::Eye_Right)).inverse() * mHead;
			level->camera.mProj = VR_matrix(hmd->GetProjectionMatrix(vr::Eye_Right, level->camera.znear, level->camera.zfar, vr::API_OpenGL));
		}
		*/

		Core::lights.pos = level->model->state.objects[level->model->getObject("#light_sphere")->id].basis.getPos();
			//level->camera.mView.inverse().getPos();

		mat4 camProj = level->camera.mProj;
		mat4 camView = level->camera.mView;

//		level->renderShadows();

	#ifdef USE_OPENVR
	// stereo frames
		if (hmd) {
			vr::VRCompositor()->WaitGetPoses(tPose, vr::k_unMaxTrackedDeviceCount, NULL, 0);
			mat4 mHead;
			if (tPose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
				mHead = VR_matrix(tPose[vr::k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking).inverse();
			else
				mHead.identity();

			for (int i = 0; i < 2; i++) {
				vr::EVREye eyeID = i ? vr::Eye_Right : vr::Eye_Left;

				Core::setViewport(0, 0, eyes[i]->width, eyes[i]->height);
				Core::setTarget(eyes[i]);
				Core::clear(vec4(0.376f, 0.741f, 0.812f, 0.0f));

				level->camera.mProj = VR_matrix(hmd->GetProjectionMatrix(eyeID, level->camera.znear, level->camera.zfar, vr::API_OpenGL));
				level->camera.mView = VR_matrix(hmd->GetEyeToHeadTransform(eyeID)).inverse() * mHead;

				level->render();
				Core::setTarget(NULL);

				eyeComp[i] = { (void*)eyes[i]->ID, vr::API_OpenGL, vr::ColorSpace_Gamma };
				vr::VRCompositor()->Submit(eyeID, &eyeComp[i]);
			}
		}
	#endif

	// spectator frame
		level->camera.mProj = camProj;
		level->camera.mView = camView;

		Core::setViewport(0, 0, Core::width, Core::height);

		Core::setTarget(composer->texture[sDiffuse], composer->texture[sNormal], composer->texture[sDepth]);
		Core::mode = rmDeferred;
		Core::clear(vec4(0.0f));
		level->render();
		Core::mode = rmNormal;
		Core::setTarget(NULL, NULL, NULL);

		Core::clear(vec4(0.376f, 0.741f, 0.812f, 1.0f));

		// calculate frustum corners

		mat4 m = camView;
		m.setPos(vec3::ZERO);
		m = (camProj * m).inverse();

		vec4 c[4];
		c[0] = m * vec4(-1, -1, 1, 1);
		c[1] = m * vec4( 1, -1, 1, 1);
		c[2] = m * vec4( 1,  1, 1, 1);
		c[3] = m * vec4(-1,  1, 1, 1);
		vec3 v[4];
		for (int i = 0; i < 4; i++)
			v[i] = c[i].xyz * (1.0f / c[i].w);// - Core::viewPos;

		Vertex2 vertices[] = {
			{v[0], {-1, -1}},
			{v[1], { 1, -1}},
			{v[2], { 1,  1}},
			{v[3], {-1,  1}},
		};

		//LOG("%f %f %f\n", Core::viewPos.x, Core::viewPos.y, Core::viewPos.z);
		m = Core::lights.mViewProj;
		Core::lights.mViewProj = Core::mViewProj.inverse();

		composer->bind();
		Core::setCulling(cfNone);
		quad->update(NULL, vertices, 0, 4);
		quad->render(0, quad->iCount);

		Core::lights.mViewProj = m;
		/*
		glDisable(GL_DEPTH_TEST);
		level->renderDebug();
		glEnable(GL_DEPTH_TEST);
		*/

		/*
		glEnable(GL_TEXTURE_2D);
		eyes[0]->bind(sDiffuse);
		glBegin(GL_QUADS);
			glColor3f(1, 1, 1);
			glTexCoord2f(0, 0); glVertex2f(-5, -5);
			glTexCoord2f(1, 0); glVertex2f( 5, -5);
			glTexCoord2f(1, 1); glVertex2f( 5,  5);
			glTexCoord2f(0, 1); glVertex2f(-5,  5);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		*/

/*
		Core::setTarget(test);
		Core::setViewport(0, 0, test->width, test->height);
		Core::clear(vec4(0, 1, 0, 1));
		level->render();
		Core::setTarget(NULL);
		
		level->camera.setup();
		Core::setViewport(0, 0, Core::width, Core::height);
		Core::clear(vec4(0.376f, 0.741f, 0.812f, 0.0f));
		level->renderDebug();

*/

	#ifdef USE_OPENVR
		if (hmd) {
			Debug::Draw::begin();
			for (int i = 0; i < vr::k_unMaxTrackedDeviceCount; i++) 
				if (tPose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid) {
					vr::ETrackedDeviceClass type = hmd->GetTrackedDeviceClass(i);
					if (type != vr::TrackedDeviceClass_Controller && type != vr::TrackedDeviceClass_HMD)
						continue;

					glPushMatrix();
					mat4 m = VR_matrix(tPose[i].mDeviceToAbsoluteTracking);					
					glMultMatrixf((GLfloat*)&m);
					Debug::Draw::axes(0.25f);
					glPopMatrix();
				}
			Debug::Draw::end();
		}
	#endif
	}

	void input(InputKey key, InputState state) {
		static vec2 mLast;
		if (state == isDown && key == ikMouseL) {
			mLast = Input::mouse.pos;
			return;
		}

		if (state == isMove && key == ikMouse && (Input::mouse.L.down || Input::mouse.R.down)) {
			vec2 delta = Input::mouse.pos - mLast;
			level->camera.angle.x -= delta.y * 0.01f;
			level->camera.angle.y -= delta.x * 0.01f;
			level->camera.angle.x = _min(_max(level->camera.angle.x, -PI * 0.5f + EPS), PI * 0.5f - EPS);
			mLast = Input::mouse.pos;
		}
	}
#else

	Level	*level;
	Player	*player;
	Tank	*tank;
	int		mode;

	void init() {
		Core::init();
		level	= new Level("level_demo.xmd");
	
		player	= new Player("primitive.xmd", "truncheon.xmd");
		player->pos = vec3(0.0f);
		//player->pos = vec3(-2.82f, -15.42f, 111.44f);
/*		
		tank	= new Tank("tank.xmd");
		tank->pos = vec3(0, 0, 3);
		tank->target = player;
		level->add(tank);
	*/	

		level->add(player);

		mode = 0;
	}

	void free() {
		delete level;
		Core::free();
	}

	void update() {
		level->update();
	}

	void render() {
		level->camera.target = quat2(quat(0, 0, 0, 1), player->pos);
	//	level->camPos	= player->pos + vec3(0.0f, 1.0f, 0.0f);
	//	angle += Core::deltaTime * 0.1f;
		//printf("%f %f %f\n", level->camPos.x, level->camPos.y, level->camPos.z);

//		level->camera.pos	= player->pos + vec3(0.0f, 3.8f, 0.0f);
//		level->camera.rot	= vec3(0 * DEG2RAD, PI + angle, 0);//vec3(-23.44f * DEG2RAD, PI, 0);
//		level->camera.dist	= 30.0f;//4.0f;

	//	level->camAngle	= vec3(-23.44f * DEG2RAD, PI, 0);
	//	level->camDist	= 4.0f;

		level->renderShadows();

		Core::setViewport(0, 0, Core::width, Core::height);
		Core::clear(vec4(0.376f, 0.741f, 0.812f, 1.0f));
		level->render();
	//	level->renderDebug();

	//	Debug::Draw::test1();
	}

	void input(InputKey key, InputState state) {
		if (state == isDown) {
			if (key == ikR) {
				mode ^= 1;
				level->setCameraFrame(mode);
			}

			if (key == ik1)	player->spawn(vec3::ZERO);
			if (key == ik2) player->spawn(vec3(-2.82f, -15.42f, 111.44f));
			if (key == ik3)	player->spawn(vec3(-2.82f, -15.42f, 130.44f));
		}
	}
#endif
}