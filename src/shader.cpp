#include "core.h"

const char *AttribName[aMAX]	= { "aCoord", "aTexCoord", "aNormal", "aTangent" };
const char *SamplerName[sMAX]	= { "sDiffuse", "sNormal", "sLight", "sDepth", "sTex4", "sTex5", "sShadow" };
const char *UniformName[uMAX]	= { "uViewProj", "uModel", "uLightViewProj", "uProjRatio", "uJoint", "uGeomScale", "uGeomBias", "uTexScale", "uTexBias", "uViewPos", "uLightDir", "uLightPos", "uAmbient", "uDiffuse" };
const char *OptionName[oMAX]	= { "MODE_SHADOW", "MAT_SKINNING", "MAT_SHADOW_CAST", "MAT_SHADOW_RECV", "MAT_ENV_SPHERE", "MAT_OPACITY", "MAT_TWO_SIDE", "MAT_DIFFUSE", "MAT_LIGHT", "MAT_NORMAL", "MODE_DEFERRED" };

#ifdef MOBILE
	#define GLSL_DEFINE "precision highp float;\n" "#define MOBILE\n"
#else
	#define GLSL_DEFINE	"#version 120\n"
#endif

char *getDefines(int options) {
	char *str = new char[256];
	str[0] = '\0';

	if (options) {
		if (options & ((1 << oShadowCast) | (1 << oShadowRecv))) {
			if (Core::support.shadowSampler) {
				#ifdef MOBILE
					strcat(str, "#extension GL_EXT_shadow_samplers : require\n");
				#endif
				strcat(str, "#define SHADOW_SAMPLER\n");
			} else
				if (Core::support.depthTexture)
					strcat(str, "#define SHADOW_DEPTH\n");
				else
					strcat(str, "#define SHADOW_COLOR\n");
		}

		for (int i = 0; i < oMAX; i++)
			if (options & (1 << i)) {
				strcat(str, "#define ");
				strcat(str, OptionName[i]);
				strcat(str, "\n");
			}
	}

	return str;
};

void shaderCheck(GLuint obj, bool isProgram) {
	GLchar info[255];

	if (isProgram)
		glGetProgramInfoLog(obj, sizeof(info), NULL, info);
	else
		glGetShaderInfoLog(obj, sizeof(info), NULL, info);

	if (info[0]) LOG("! shader: %s\n", info);
}


Shader::Shader(Stream *stream, int param) : ResObject(stream, param) {
	int size = stream->size;
	char *data = new char[size + 1];
	stream->read(data, size);
	data[size] = '\0';

	ID = glCreateProgram();

	char *options = getDefines(param);
	LOG("shader opt: %s\n", options);

	const int type[2] = { GL_VERTEX_SHADER, GL_FRAGMENT_SHADER };
	const char *code[2][3] = { 
					{ GLSL_DEFINE "#define VERTEX\n", options,   data }, 
					{ GLSL_DEFINE "#define FRAGMENT\n", options, data } 
				};

	for (int i = 0; i < 2; i++) {
		GLuint obj = glCreateShader(type[i]);
		glShaderSource(obj, 3, code[i], NULL);
		glCompileShader(obj);
		shaderCheck(obj, false);

		glAttachShader(ID, obj);
		glDeleteShader(obj);
	}

	delete[] options;
	delete[] data;

	for (int at = 0; at < aMAX; at++)
		glBindAttribLocation(ID, at, AttribName[at]);
	glLinkProgram(ID);
	shaderCheck(ID, true);

	glUseProgram(ID);
	for (int st = 0; st < sMAX; st++)
		glUniform1iv(getUniformIndex(SamplerName[st]), 1, &st);

	for (int ut = 0; ut < uMAX; ut++)
		uID[ut] = getUniformIndex(UniformName[ut]);
}

Shader::~Shader() {
	glDeleteProgram(ID);
}

void Shader::bind() {
	if (Core::active.shader != this) {
		Core::active.shader = this;

		glUseProgram(ID);
	}
}

GLint Shader::getUniformIndex(const char *name) {
	return glGetUniformLocation(ID, (GLchar*)name);
}

void Shader::setParam(UniformType uType, const vec3 &value, int count) {
	if (uID[uType] != -1)
		glUniform3fv(uID[uType], count, (GLfloat*)&value);
}

void Shader::setParam(UniformType uType, const vec4 &value, int count) {
	if (uID[uType] != -1)
		glUniform4fv(uID[uType], count, (GLfloat*)&value);
}

void Shader::setParam(UniformType uType, const mat4 &value, int count) {
	if (uID[uType] != -1)
		glUniformMatrix4fv(uID[uType], count, false, (GLfloat*)&value);
}