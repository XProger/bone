#include "core.h"
#include "game.h"

namespace Input {
	Joystick	joy;
	Mouse		mouse;

	bool down[ikMAX];

	void reset() {
		memset(down, 0, sizeof(down));
	}

	void setDown(int code, bool value) {
		static int keyCode[ikMAX] = {
#ifdef WIN32
			0, 0, 0, 0, 0, VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN, VK_SPACE, VK_RETURN, VK_SHIFT,
			0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
#elif __APPLE__
			0, 0, 0, 0, 0, 123, 124, 126, 125, 49, 36, 
			29, 18, 19, 20, 21, 23, 22, 26, 28, 25, shift,
			0x00,  0x0B, 0x08, 0x02, 0x0E, 0x03, 0x05, 0x04, 0x22, 0x26, 0x28, 0x25, 0x2E, 0x2D, 0x1F, 0x23, 0x0C, 0x0F, 0x01, 0x11, 0x20, 0x09, 0x0D, 0x07, 0x10, 0x06
#elif __EMSCRIPTEN__
			0, 0, 0, 0, 0, 0x25, 0x27, 0x26, 0x28, 0x20, 0x0D,
			0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, shift,
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
#endif
		};

      //  LOG("key: %d\n", code);
        
		for (int ik = ikLeft; ik < ikMAX; ik++)
			if (keyCode[ik] == code) {
				bool event = down[ik] != value;
				down[ik] = value;

				if (event)
					Game::input((InputKey)ik, value ? isDown : isUp);
				
				return;
			}
	}

	void setMouseDown(int idx, bool down) {
		Mouse::Pointer *btn = &mouse.L;
		btn += idx;

		bool event = btn->down != down;
		btn->down = down;
		if (event)
			Game::input((InputKey)(ikMouseL + idx), down ? isDown : isUp);

	}

	void setMouseMove(const vec2 &pos) {
		bool event = mouse.pos != pos;
		mouse.pos = pos;
		if (event)
			Game::input(ikMouse, isMove);
	}

}
