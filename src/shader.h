#ifndef H_SHADER
#define H_SHADER

#include "core.h"

enum AttribType		{ aCoord, aTexCoord, aNormal, aTangent, aMAX };
enum SamplerType	{ sDiffuse, sNormal, sLight, sDepth, sTex4, sTex5, sShadow, sMAX };
enum UniformType	{ uViewProj, uModel, uLightViewProj, uProjRatio, uJoint, uGeomScale, uGeomBias, uTexScale, uTexBias, uViewPos, uLightDir, uLightPos, uAmbient, uDiffuse, uMAX };
enum OptionType		{ oShadow, oSkinning, oShadowCast, oShadowRecv, oEnvSphere, oOpacity, oTwoSide, oDiffuse, oLight, oNormal, oDeferred, oMAX };

extern const char *AttribName[aMAX];
extern const char *SamplerName[sMAX];
extern const char *UniformbName[uMAX];

struct Shader : ResObject {
	GLuint	ID;
	GLint	uID[uMAX];

	Shader(Stream *stream, int param);
	virtual ~Shader();
	void bind();
	GLint getUniformIndex(const char *name);
	void setParam(UniformType uType, const vec3 &value, int count = 1);
	void setParam(UniformType uType, const vec4 &value, int count = 1);
	void setParam(UniformType uType, const mat4 &value, int count = 1);
};

#endif