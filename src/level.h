#ifndef H_LEVEL
#define H_LEVEL

#include "core.h"
#include "camera.h"

struct Level {

	struct Entity {
		Entity	*next;
		Level	*level;

		quat2	*ground;
		quat2	groundLast;
		vec3	groundNormal;


		quat rot, trot;
		vec3 pos, velocity, velocityCtrl;

		Entity();
		virtual ~Entity() {};
		virtual void spawn(const vec3 &pos);
		virtual void hit(int damage);
		virtual void update() {};
		virtual void render() {};
		virtual void setBasis(const quat2 &basis);
		virtual quat2 getBasis();
	};

	List<Entity>	entities;

	Model	*model;
	Texture	*shadow;

	int		cameraIndex;
	Camera	camera;

	Level(const char *name);
	~Level();

	Entity* add(Entity *entity);
	Entity* remove(Entity *entity);

	void activate(Entity *entity, int triggerIndex);
	void activate(Entity *entity, bool action);
	void deactivate();

	void move(Entity *entity);

	void setupLightCamera();
	void setCamera(int index);
	void setCameraFrame(int index);

	void update();
	void render();
	void renderShadows();
	void renderScene();
	void renderDebug();
	NavMesh::Path* findPath(Entity *from, Entity *target);
};

#endif