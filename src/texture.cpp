#include "core.h"

#define PVR_RGBA8	0x61626772
#define PVR_PVRTC4	0x00000002
#define PVR_ETC1	0x00000006
#define PVR_BC1		0x00000007
#define PVR_ALPHA	0x00000061

#define GL_COMPRESSED_RGB_S3TC_DXT1		0x83F0
#define	GL_ETC1_RGB8_OES				0x8D64
#define GL_COMPRESSED_RGB_PVRTC_4BPPV1	0x8C00

Texture::Texture(Stream *stream, int param) : ResObject(stream, param), width(0), height(0), depth(false) {
	struct {
		int	version;
		int	flags;
		long format;
		long ext;
		int color;
		int channel;
		int height;
		int width;
		int depth;
		int surfaces;
		int faces;
		int mipCount;
		int metaSize;
	} header;

	stream->read(&header, sizeof(header));
	stream->seek(header.metaSize);

	GLenum fmt, minSize;
	switch (header.format) {
		case PVR_ALPHA  : minSize = 1; fmt = GL_ALPHA; break;
		case PVR_RGBA8	: minSize = 1; fmt = GL_RGBA; break;
		case PVR_BC1	: minSize = 4; fmt = GL_COMPRESSED_RGB_S3TC_DXT1; break;
		case PVR_ETC1	: minSize = 4; fmt = GL_ETC1_RGB8_OES; break;
		case PVR_PVRTC4	: minSize = 8; fmt = GL_COMPRESSED_RGB_PVRTC_4BPPV1; break;
		default			: ASSERT(false);
	}

	int sizeX	= width = header.width;
	int sizeY	= height = header.height;

	glGenTextures(1, &ID);
	setFilter(tfAniso);

	char *data = new char[width * height * 4];
	for (int i = 0; i < header.mipCount; i++) {

		if (header.format == PVR_RGBA8 || header.format == PVR_ALPHA) {
			stream->read(data, sizeX * sizeY * (header.format == PVR_RGBA8 ? 4 : 1));
			glTexImage2D(GL_TEXTURE_2D, i, fmt, sizeX, sizeY, 0, fmt, GL_UNSIGNED_BYTE, data);
		} else {
			int size = (_max(sizeX, minSize) * _max(sizeY, minSize) * 4 + 7) / 8;
			stream->read(data, size);
			glCompressedTexImage2D(GL_TEXTURE_2D, i, fmt, sizeX, sizeY, 0, size, data);
		}

		sizeX >>= 1;
		sizeY >>= 1;
	}

	delete[] data;
}

Texture::Texture(int width, int height, SamplerFormat format, void *data) : ResObject(NULL, NULL), width(width), height(height) {
	glGenTextures(1, &ID);
	this->depth = format == sfDepth || format == sfShadow;
	if (depth && !Core::support.depthTexture)
		depth = false;

	if (depth) {
		if (Core::support.shadowSampler && format == sfShadow) {
			setFilter(tfBilinear);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		} else
			setFilter(tfNone);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, data);
	} else {
		setFilter(tfBilinear);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		GLint fmt = format == sfRGB ? GL_RGB : GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, fmt, width, height, 0, fmt, GL_UNSIGNED_BYTE, data);
	}
}

Texture::~Texture() {
	glDeleteTextures(1, &ID);
}

void Texture::bind(int sampler) {
	if (Core::active.texture[sampler] != this) {
		Core::active.texture[sampler] = this;

		glActiveTexture(GL_TEXTURE0 + sampler);
		glBindTexture(GL_TEXTURE_2D, ID);
	}
}

void Texture::setFilter(TextureFilter filter) {
	bind(sDiffuse);
	GLint fmag, fmin;

	switch (filter) {
		case tfNone :
			fmag = fmin = GL_NEAREST;
			break;
		case tfBilinear :
			fmag = fmin = GL_LINEAR;
			break;
		case tfTrilinear :
		case tfAniso	 :
			fmag = GL_LINEAR;
			fmin = GL_LINEAR_MIPMAP_LINEAR;
			break;
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, fmag);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, fmin);
	if (filter == tfAniso && Core::support.anisotropic)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Core::support.anisotropic);
}
