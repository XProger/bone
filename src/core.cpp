#include "core.h"

#if defined(_WIN32) || defined(LINUX)
	// Texture
	PFNGLACTIVETEXTUREPROC				glActiveTexture;
	PFNGLCOMPRESSEDTEXIMAGE2DPROC		glCompressedTexImage2D;
	// Shader
	PFNGLCREATEPROGRAMPROC				glCreateProgram;
	PFNGLDELETEPROGRAMPROC				glDeleteProgram;
	PFNGLLINKPROGRAMPROC				glLinkProgram;
	PFNGLUSEPROGRAMPROC					glUseProgram;
	PFNGLGETPROGRAMINFOLOGPROC			glGetProgramInfoLog;
	PFNGLCREATESHADERPROC				glCreateShader;
	PFNGLDELETESHADERPROC				glDeleteShader;
	PFNGLSHADERSOURCEPROC				glShaderSource;
	PFNGLATTACHSHADERPROC				glAttachShader;
	PFNGLCOMPILESHADERPROC				glCompileShader;
	PFNGLGETSHADERINFOLOGPROC			glGetShaderInfoLog;
	PFNGLGETUNIFORMLOCATIONPROC			glGetUniformLocation;
	PFNGLUNIFORM1IVPROC					glUniform1iv;
	PFNGLUNIFORM3FVPROC					glUniform3fv;
	PFNGLUNIFORM4FVPROC					glUniform4fv;
	PFNGLUNIFORMMATRIX4FVPROC			glUniformMatrix4fv;
	PFNGLBINDATTRIBLOCATIONPROC			glBindAttribLocation;
	PFNGLENABLEVERTEXATTRIBARRAYPROC	glEnableVertexAttribArray;
	PFNGLDISABLEVERTEXATTRIBARRAYPROC	glDisableVertexAttribArray;
	PFNGLVERTEXATTRIBPOINTERPROC		glVertexAttribPointer;
	// Mesh
	PFNGLGENBUFFERSARBPROC				glGenBuffers;
	PFNGLDELETEBUFFERSARBPROC			glDeleteBuffers;
	PFNGLBINDBUFFERARBPROC				glBindBuffer;
	PFNGLBUFFERDATAARBPROC				glBufferData;
	PFNGLBUFFERSUBDATAARBPROC			glBufferSubData;
	// FBO
	PFNGLGENFRAMEBUFFERSPROC			glGenFramebuffers;
	PFNGLBINDFRAMEBUFFERPROC			glBindFramebuffer;
	PFNGLFRAMEBUFFERTEXTURE2DPROC		glFramebufferTexture2D;
	PFNGLGENRENDERBUFFERSPROC			glGenRenderbuffers;
	PFNGLBINDRENDERBUFFERPROC			glBindRenderbuffer;
	PFNGLFRAMEBUFFERRENDERBUFFERPROC	glFramebufferRenderbuffer;
	PFNGLRENDERBUFFERSTORAGEPROC		glRenderbufferStorage;
	PFNGLCHECKFRAMEBUFFERSTATUSPROC		glCheckFramebufferStatus;
	PFNGLDRAWBUFFERSPROC				glDrawBuffers;

	void* _GetProcAddress(const char *name) {
		#ifdef _WIN32
			return (void*)wglGetProcAddress(name);
		#elif LINUX
			return (void*)glXGetProcAddress((GLubyte*)name);
		#endif
	}
#endif

ResObject::ResObject(Stream *stream, int param) : name(stream ? String::copy(stream->name) : NULL), refCount(1), param(param) {}

ResObject::~ResObject() {
	delete name;
}

namespace Core {
	List<ResObject> *list;
	int			width, height;
	float		deltaTime;
	mat4		mProj, mView, mViewProj, mModel;
	vec3		viewPos;
	vec3		projRatio;
	RenderMode	mode;
	Texture		*whiteTex;

	Support		support;
	Active		active;
	Lights		lights;

	GLuint		defRT, RT, defRB, RB;
	Plane		frustum[6];

	CullMode	cullMode;
	BlendMode	blendMode;

	ResObject *find(const char *name, int param) {
		ResObject *res = list->first;
		while (res) {
			if (String::equal(res->name, name) && res->param == param)
				return res;	
			res = res->next;
		}
		return NULL;
	}

	bool extSupport(const char *str, const char *ext) {
		return strstr(str, ext) != NULL;
	}

	void getInfo() {
        char brand[0x40];
        memset(brand, 0, sizeof(brand));

		#ifdef _WIN32
				int cpuInfo[4] = { -1 };
				__cpuid(cpuInfo, 0x80000002);
				memcpy(brand, cpuInfo, sizeof(cpuInfo));
				__cpuid(cpuInfo, 0x80000003);
				memcpy(brand + 16, cpuInfo, sizeof(cpuInfo));
				__cpuid(cpuInfo, 0x80000004);
				memcpy(brand + 32, cpuInfo, sizeof(cpuInfo));
		#elif __APPLE__
				size_t len = sizeof(brand);
				sysctlbyname("machdep.cpu.brand_string", &brand, &len, NULL, 0);
		#endif

		if (brand[0])
			LOG("CPU      : %s\n", brand);
		LOG("Vendor   : %s\n", glGetString(GL_VENDOR));
		LOG("Renderer : %s\n", glGetString(GL_RENDERER));
		LOG("Version  : %s\n", glGetString(GL_VERSION));
	}

	void init() {
		Input::reset();

#if defined(WIN32) || defined(LINUX)
		#if defined(_MSC_VER)
			#define GetProcOGL(x) *(void**)&x=(void*)_GetProcAddress(#x);
		#else
			#define GetProcOGL(x) x=(typeof(x))_GetProcAddress(#x);
		#endif

		#ifdef WIN32
			PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
			GetProcOGL(wglSwapIntervalEXT);
			wglSwapIntervalEXT(1);
		#endif
		GetProcOGL(glActiveTexture);
		GetProcOGL(glCompressedTexImage2D);
		GetProcOGL(glCreateProgram);
		GetProcOGL(glDeleteProgram);
		GetProcOGL(glLinkProgram);
		GetProcOGL(glUseProgram);
		GetProcOGL(glGetProgramInfoLog);
		GetProcOGL(glCreateShader);
		GetProcOGL(glDeleteShader);
		GetProcOGL(glShaderSource);
		GetProcOGL(glAttachShader);
		GetProcOGL(glCompileShader);
		GetProcOGL(glGetShaderInfoLog);
		GetProcOGL(glGetUniformLocation);
		GetProcOGL(glUniform1iv);
		GetProcOGL(glUniform3fv);
		GetProcOGL(glUniform4fv);
		GetProcOGL(glUniformMatrix4fv);
		GetProcOGL(glBindAttribLocation);
		GetProcOGL(glEnableVertexAttribArray);
		GetProcOGL(glDisableVertexAttribArray);
		GetProcOGL(glVertexAttribPointer);
		GetProcOGL(glGenBuffers);
		GetProcOGL(glDeleteBuffers);
		GetProcOGL(glBindBuffer);
		GetProcOGL(glBufferData);
		GetProcOGL(glBufferSubData);
		GetProcOGL(glGenFramebuffers);
		GetProcOGL(glBindFramebuffer);
		GetProcOGL(glFramebufferTexture2D);
		GetProcOGL(glGenRenderbuffers);
		GetProcOGL(glBindRenderbuffer);
		GetProcOGL(glFramebufferRenderbuffer);
		GetProcOGL(glRenderbufferStorage);
		GetProcOGL(glCheckFramebufferStatus);
		GetProcOGL(glDrawBuffers);
#endif
		char *ext = (char*)glGetString(GL_EXTENSIONS);
		//LOG("%s\n", ext);
		if (extSupport(ext, "_texture_filter_anisotropic"))
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &support.anisotropic);
		else
			support.anisotropic = 0.0f;	
		support.depthTexture = extSupport(ext, "_depth_texture");
		support.shadowSampler = extSupport(ext, "EXT_shadow_samplers") || extSupport(ext, "GL_ARB_shadow");

	//	support.depthTexture = false;
	//	support.shadowSampler = false;
		
		getInfo();
		LOG("supports:\n");
		LOG("	anisotropic	: %d\n", (int)support.anisotropic);
		LOG("	depth texture	: %s\n", support.depthTexture ? "true" : "false");
		LOG("	shadow sampler	: %s\n", support.shadowSampler ? "true" : "false");

		list = new List<ResObject>();

		for (int at = aCoord; at < aMAX; at++)
			glEnableVertexAttribArray(at);

		glGenFramebuffers(1, &RT);
		defRT = 0;

		glGenRenderbuffers(1, &RB);
		glBindRenderbuffer(GL_RENDERBUFFER, RB);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, 2048, 2048);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		defRB = 0;

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);		
		glDisable(GL_BLEND);

		cullMode = cfNone;
		blendMode = bmNone;

		mode = rmNormal;

		whiteTex = new Texture(4, 4);
		UI::init();
	}

	void free() {
		UI::free();
		delete whiteTex;

		ResObject *res = list->first;
		while (res) {
			LOG("! leak resource (%d) '%s'\n", res->refCount, res->name);
			res = res->next;
		}
		delete list;
	}

	void unload(ResObject *res) {
		if (!res) return;
		if (--res->refCount == 0)
			delete list->remove(res);
	}

	void clear(const vec4 &color) {
		glClearColor(color.x, color.y, color.z, color.w);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	
	void setViewport(int x, int y, int w, int h) {
		glViewport(x, y, w, h);
	}

	void setTarget(Texture *color, Texture *normal, Texture *depth) {
		if (!color || !normal || !depth)  {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			return;
		}

		glBindFramebuffer(GL_FRAMEBUFFER, RT);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color->ID, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normal->ID, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth->ID, 0);

		GLenum  buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		glDrawBuffers(2, buffers);
	}

	void setTarget(Texture *texture) {
/*		int d, r;
		glGetIntegerv(GL_DRAW_BUFFER, (GLint*)&d);
		glGetIntegerv(GL_READ_BUFFER, (GLint*)&r);
		LOG("%d %d\n", d, r);
		ASSERT(d == GL_BACK_LEFT);
		ASSERT(r == GL_BACK);
		*/
		if (!texture) {
//			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
//			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
//			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			#ifndef MOBILE
//				glDrawBuffer(GL_BACK_LEFT);
 //				glDrawBuffer(GL_BACK);
//				glReadBuffer(GL_BACK);
			#endif
			return;
		}
//		glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&defRT);
		
		glBindFramebuffer(GL_FRAMEBUFFER, RT);
		glFramebufferTexture2D(GL_FRAMEBUFFER, texture->depth ? GL_DEPTH_ATTACHMENT : GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture->ID, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, texture->depth ? GL_COLOR_ATTACHMENT0 : GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);

		if (texture->depth) {
//			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
			#ifndef MOBILE
				GLenum  buffers[] = { GL_DEPTH_ATTACHMENT };
				glDrawBuffers(1, buffers);
//				glDrawBuffer(GL_NONE);
//				glReadBuffer(GL_NONE);
			#endif
		} else {
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, RB);
			GLenum  buffers[] = { GL_COLOR_ATTACHMENT0 };
			glDrawBuffers(1, buffers);

//			glDrawBuffer(GL_BACK);
//			glReadBuffer(GL_BACK);
		}

//		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
//			LOG("error! %d\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
	}

	void setMatrix(mat4 *proj, mat4 *view, mat4 *model) {
		if (proj) mProj = *proj;
		if (view) mView = *view;
		if (model) mModel = *model;
		if (proj || view) {
			mViewProj = mProj * mView;
			mat4 &m = mViewProj;
			frustum[0] = Plane(m.e30 - m.e00, m.e31 - m.e01, m.e32 - m.e02, m.e33 - m.e03); // right
			frustum[1] = Plane(m.e30 + m.e00, m.e31 + m.e01, m.e32 + m.e02, m.e33 + m.e03); // left
			frustum[2] = Plane(m.e30 - m.e10, m.e31 - m.e11, m.e32 - m.e12, m.e33 - m.e13); // top
			frustum[3] = Plane(m.e30 + m.e10, m.e31 + m.e11, m.e32 + m.e12, m.e33 + m.e13); // bottom
			frustum[4] = Plane(m.e30 - m.e20, m.e31 - m.e21, m.e32 - m.e22, m.e33 - m.e23); // near
			frustum[5] = Plane(m.e30 + m.e20, m.e31 + m.e21, m.e32 + m.e22, m.e33 + m.e23); // far
		}
	}

	void setCulling(CullMode mode) {
		if (mode == cullMode) return;

		switch (mode) {
			case cfNone :
				glDisable(GL_CULL_FACE);
			case cfBack :
				glCullFace(GL_BACK);
				break;
			case cfFront :
				glCullFace(GL_FRONT);
				break;
		}

		if (cullMode == bmNone)
			glEnable(GL_CULL_FACE);

		cullMode = mode;
	}

	void setBlending(BlendMode mode) {
		if (mode == blendMode) return;

		switch (mode) {
			case bmNone :
				glDisable(GL_BLEND);
				break;
			case bmAlpha :
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				break;
			case bmAdd :
				glBlendFunc(GL_ONE, GL_ONE);
				break;
			case bmMultiply :
				glBlendFunc(GL_DST_COLOR, GL_ZERO);
				break;
			case bmScreen :
				glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
				break;
		}

		if (blendMode == bmNone)
			glEnable(GL_BLEND);

		blendMode = mode;
	}

	bool isVisible(const quat2 &basis, float radius) {
		const vec3 &p = basis.getPos();
		for (int i = 0; i < 6; i++)
			if (frustum[i].dist(p) < -radius)
				return false;
		return true;
	}

	bool isVisible(const quat2 &basis, const vec3 &size) {
	// TODO: apply basis
		for (int i = 0; i < 6; i++)
			if (frustum[i].dist( size) < 0 &&
				frustum[i].dist(vec3(-size.x,  size.y,  size.z)) < 0 &&
				frustum[i].dist(vec3( size.x, -size.y,  size.z)) < 0 &&
				frustum[i].dist(vec3(-size.x, -size.y,  size.z)) < 0 &&
				frustum[i].dist(vec3( size.x,  size.y, -size.z)) < 0 &&
				frustum[i].dist(vec3(-size.x,  size.y, -size.z)) < 0 &&
				frustum[i].dist(vec3( size.x, -size.y, -size.z)) < 0 &&
				frustum[i].dist(-size) < 0)
				return false;
		return true;
	}
}
