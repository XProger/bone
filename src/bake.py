import bpy
import numpy
import os.path

if len(bpy.context.selected_objects) > 0:
	bpy.ops.object.mode_set(mode = 'OBJECT')

scene = bpy.context.scene
layer = scene.render.layers[0]
layer.use_pass_diffuse_direct   = True
layer.use_pass_diffuse_indirect = True
layer.use_pass_diffuse_color    = False

SIZE = 256
diffuse	= bpy.data.images.new('bake_diffuse', SIZE, SIZE)
ambient = bpy.data.images.new('bake_ambient', SIZE, SIZE)
light   = bpy.data.images.new('bake_light', SIZE, SIZE)

for obj in bpy.context.selected_objects:
	obj.select = False

list = []
for object in scene.objects:
	if object.type != 'MESH':
		continue
	if object.data.uv_textures.get('UVLight') == None:
		continue
	filename = object.name + '_L.png'
	if os.path.exists('./' + filename):
		continue
	list.append(object)

for i, object in enumerate(list):
	print('bake ', object.name, '(' + str(i + 1) + '/' + str(len(list)) + ')')
	
	#obj = bpy.data.objects['ground']
	object.select = True
	bpy.ops.object.duplicate()	
	object.select = False	
	obj = bpy.context.selected_objects[0]
	scene.objects.active = obj

	#scene.objects.active = obj
	obj.select = True

	uv = obj.data.uv_textures
	uv.active = uv['UVLight']

	mat = obj.material_slots[0].material

	mat.use_nodes = True
	nodes = mat.node_tree.nodes

	node = nodes.new(type="ShaderNodeTexImage")
	node.select = True
	nodes.active = node

	object.hide = True

	node.image = diffuse
	scene.update()
	bpy.ops.object.bake(type = 'DIFFUSE', use_selected_to_active=False, use_clear = True)
	
	node.image = ambient
	scene.update()
	bpy.ops.object.bake(type='AO', use_selected_to_active=False, use_clear = True)
	node.image = None
	
	object.hide = False

	pixels = numpy.array(diffuse.pixels[:]) * numpy.array(ambient.pixels[:])

	light.pixels = pixels.tolist()

	light.file_format = 'PNG'
	light.filepath_raw = '//' + object.name + '_L.png'
	light.save()

	#node.image = light
	node.select = False
	nodes.remove(node)
	nodes.active = None
	uv.active = uv[0]
	#obj.select = False
	
	bpy.ops.object.delete()
	
print('baking is complete!')

bpy.data.images.remove(diffuse)
bpy.data.images.remove(ambient)
bpy.data.images.remove(light)
