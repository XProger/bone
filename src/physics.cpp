#include "physics.h"

void supportPoint(const Body &body, const vec3 &dir, vec3 &v) {
	v = vec3::ZERO;
}

void supportSegment(const Body &body, const vec3 &dir, vec3 &v) {
	v.x = v.z = 0.0f;
	v.y = sign(dir.y) * body.height;
}

void supportRect(const Body &body, const vec3 &dir, vec3 &v) {
	v.x = sign(dir.x) * body.size.x;
	v.y = sign(dir.y) * body.size.y;
	v.z = 0.0f;
}

void supportDisc(const Body &body, const vec3 &dir, vec3 &v) {
	v = vec3(dir.x, 0, dir.z).normal() * body.radius;
}

void supportSphere(const Body &body, const vec3 &dir, vec3 &v) {
	v = dir * (body.radius / dir.length());
}

void supportBox(const Body &body, const vec3 &dir, vec3 &v) {
	v = vec3(sign(dir.x), sign(dir.y), sign(dir.z)) * body.size;
}

void supportRound(const Body &body, const vec3 &dir, vec3 &v) {
	vec3 s;
	supportBox(body, dir, v);
	supportSphere(body, dir, s);
	v += s;
}

void supportCapsule(const Body &body, const vec3 &dir, vec3 &v) {
	vec3 s;
	supportSegment(body, dir, v);
	supportSphere(body, dir, s);
	v += s;
}

void supportCylinder(const Body &body, const vec3 &dir, vec3 &v) {
	vec3 s;
	supportSegment(body, dir, v);
	supportDisc(body, dir, s);
	v += s;
}

void supportCone(const Body &body, const vec3 &dir, vec3 &v) {
	vec3 s = vec3(0.0f, body.height, 0.0f);
	supportDisc(body, dir, v);
	v = dir.dot(v) > dir.dot(s) ? v : s;
}

void supportWheel(const Body &body, const vec3 &dir, vec3 &v) {
	vec3 s;
	supportDisc(body, dir, v);
	supportSphere(body, dir, s);
	v += s;
}

void supportConvex(const Body &body, const vec3 &dir, vec3 &v) {
	float dMax = -INF;
	for (int i = 0; i < body.count; i++) {
		float d = dir.dot(body.points[i]);
		if (d > dMax) {
			dMax = d;
			v = body.points[i];
		}
	}
}

Body::Body() {}
Body::Body(Type type, SupportFunc sf, const quat2 &basis, const vec3 &size) : type(type), supportFunc(sf), imass(0.0f), maxRadius(size.length()), size(size), sleeping(false), userData(NULL) { setBasis(basis); }
Body::Body(Type type, SupportFunc sf, const quat2 &basis, float radius, float height) : type(type), supportFunc(sf), imass(0.0f), maxRadius(radius + height), radius(radius), height(height), sleeping(false), userData(NULL) { setBasis(basis); }
Body::Body(Type type, SupportFunc sf, const quat2 &basis, int count, vec3 *points) : type(type), supportFunc(sf), imass(0.0f), count(count), points(points), sleeping(false), userData(NULL) {
	setBasis(basis);
	maxRadius = 0.0f;
	for (int i = 0 ; i < count; i++) {
		float d = points[i].length2();
		if (d > maxRadius)
			maxRadius = d;
	}
	maxRadius = sqrtf(maxRadius);
}

void Body::setMass(float m) {
	mass = m;
	imass = m == 0.0f ? 0.0f : (1.0f / mass);
}

void Body::setBasis(const quat2 &basis) {
	rot = basis.getRot();
	pos = basis.getPos();
	irot = basis.real.inverse();
}

quat2 Body::getBasis() const {
	return quat2(rot, pos);
}

void Body::support(const vec3 &dir, vec3 &v) const {
	vec3 d = irot * dir;
	supportFunc(*this, d, v);
	v = rot * v + pos;
}

Body Body::Sphere(const quat2 &basis, float radius) {
	Body b(SPHERE, supportSphere, basis, radius, 0);
	b.tensor = vec3(1.0f) / vec3(radius * radius * 2.0f / 5.0f);
	return b;
}

Body Body::Box(const quat2 &basis, const vec3 &size) {
	Body b(BOX, supportBox, basis, size);
	vec3 v = size * size;
	b.tensor = vec3(1.0f) / (vec3(v.y + v.z, v.x + v.z, v.x + v.y) / 3.0f);
	return b;
}

Body Body::Capsule(const quat2 &basis, float radius, float height) {
	Body b(CAPSULE, supportCapsule, basis, radius, height);
	float r = radius * radius;
	b.tensor.x = b.tensor.z = r / 4.0f + height * height / 3.0f;
	b.tensor.y = r / 2.0f;
	b.tensor = vec3(1.0f) / b.tensor;
	return b;
}

Body Body::Cylinder(const quat2 &basis, float radius, float height) {
	Body b(CYLINDER, supportCylinder, basis, radius, height);
	float r = radius * radius;
	b.tensor.x = b.tensor.z = r / 4.0f + height * height / 3.0f;
	b.tensor.y = r / 2.0f;
	b.tensor = vec3(1.0f) / b.tensor;
	return b;
}

Body Body::Cone(const quat2 &basis, float radius, float height) {
	Body b(CONE, supportCone, basis, radius, height);
	float r = radius * radius;
	float h = height * height * 4.0f;
	b.tensor.x = b.tensor.z = h / 10.0f + r * 3.0f / 20.0f;
	b.tensor.y = r * 3.0f / 10.0f;
	b.tensor = vec3(1.0f) / b.tensor;
	return b;
}

Body Body::Convex(const quat2 &basis, int count, vec3 *points) {
	return Body(CONVEX, supportConvex, basis, count, points);
}

struct Support {
	vec3 v, a, b;
};

void support(const Body &a, const Body &b, const vec3 &dir, Support &s) {
	a.support(-dir, s.a);
	b.support( dir, s.b);
	s.v = s.b - s.a;
};

bool Body::collide(const Body &body, Contact *contact) const {
	const Body &a = *this;
	const Body &b = body;

	Support v[5];

	v[0].a = a.pos;
	v[0].b = b.pos;
	v[0].v = v[0].b - v[0].a;

	if (v[0].v == vec3::ZERO) v[0].v = vec3(0.00001f, 0, 0);

	vec3 n = -v[0].v;
	::support(a, b, n, v[1]);
	if (v[1].v.dot(n) <= 0.0f) return false;

	n = v[1].v.cross(v[0].v);
	if (v[0].v == vec3::ZERO) {
		contact->a = v[1].a;
		contact->b = v[1].b;
		contact->n = (v[1].b - v[0].a).normal();
		return true;
	}

	::support(a, b, n, v[2]);
	if (v[2].v.dot(n) <= 0.0f) return false;

	n = (v[1].v - v[0].v).cross(v[2].v - v[0].v);
	float dist = n.dot(v[0].v);

	if (dist > 0.0f) {
		Support tmp = v[1];
		v[1] = v[2];
		v[2] = tmp;
		n = -n;
	}

	vec3 p1, p2;
	bool hit = false;
	while (!hit) {
		::support(a, b, n, v[3]);
		if (v[3].v.dot(n) <= 0.0f) return false;
		
		if (v[1].v.cross(v[3].v).dot(v[0].v) < 0.0f) {
			v[2] = v[3];
			n = (v[1].v - v[0].v).cross(v[3].v - v[0].v);
			continue;
		}

		if (v[3].v.cross(v[2].v).dot(v[0].v) < 0.0f) {
			v[1] = v[3];
			n = (v[3].v - v[0].v).cross(v[2].v - v[0].v);
			continue;
		}

		int pass = 0;
		while (1) {
			pass++;

			n = (v[2].v - v[1].v).cross(v[3].v - v[1].v).normal();
			float d = n.dot(v[1].v);

			if (d >= 0)
				hit = true;
			
			::support(a, b, n, v[4]);
			float delta = (v[4].v - v[3].v).dot(n);
			float depth = -n.dot(v[4].v);

			if (delta <= 0.001f || depth >= 0 || pass > 10) {
				if (!hit) return false;

				float b[4];
				b[0] = v[1].v.cross(v[2].v).dot(v[3].v);
				b[1] = v[3].v.cross(v[2].v).dot(v[0].v);
				b[2] = v[0].v.cross(v[1].v).dot(v[3].v);
				b[3] = v[2].v.cross(v[1].v).dot(v[0].v);

				float sum = b[0] + b[1] + b[2] + b[3];

				if (sum <= 0) {
					b[0] = 0.0f;
					b[1] = v[2].v.cross(v[3].v).dot(n);
					b[2] = v[3].v.cross(v[1].v).dot(n);
					b[3] = v[1].v.cross(v[2].v).dot(n);
					sum = b[1] + b[2] + b[3];
				}

				float inv = 1.0f / sum;

				p1 = (v[0].a * b[0] + v[1].a * b[1] + v[2].a * b[2] + v[3].a * b[3]) * inv;
				p2 = (v[0].b * b[0] + v[1].b * b[1] + v[2].b * b[2] + v[3].b * b[3]) * inv;

				break;
			}

			float d1 = v[4].v.cross(v[1].v).dot(v[0].v);
			float d2 = v[4].v.cross(v[2].v).dot(v[0].v);
			float d3 = v[4].v.cross(v[3].v).dot(v[0].v);

			if (d1 < 0)
				v[d2 < 0 ? 1 : 3] = v[4];
			else
				v[d3 < 0 ? 2 : 1] = v[4];
		}
	}

	Support s;
	::support(a, b, n, s);
	contact->a = n * (s.a - p1).dot(n) + p1;
	contact->b = n * (s.b - p2).dot(n) + p2;
	contact->n = n;
	return true;
}


// World::Constraint
World::Constraint::Constraint(const vec3 &oa, const vec3 &ob, const vec3 &p, const vec3 &n, int stamp) : next(NULL), oa(oa), ob(ob), p(p), n(n), stamp(stamp), lambdaCache(0) {}

World::Constraint::~Constraint() {
	delete next;
}

void World::Constraint::init(Body *a, Body *b, float stepTime) {
	ra = a->rot * oa;
	rb = b->rot * ob;
	vec3 pa = a->pos + ra;
	vec3 pb = b->pos + rb;

	float beta = 0.5f / stepTime;
	perr = _max(0.0f, n.dot(pb - pa) * beta);

	ia = a->tensor * ra.cross(n) * a->imass;
	ib = b->tensor * rb.cross(n) * b->imass;

	effm = 1.0f / ( a->imass + b->imass + n.dot(ia.cross(ra) + ib.cross(rb)) );

	a->velocity.linear  += n * (lambdaCache * a->imass);
	b->velocity.linear  -= n * (lambdaCache * b->imass);
	a->velocity.angular += ia * lambdaCache;
	b->velocity.angular -= ib * lambdaCache;
}

void World::Constraint::solve(Body *a, Body *b, float stepTime, bool pseudo) {
	vec3 rv = (b->velocity.linear + b->velocity.angular.cross(rb)) - 
			  (a->velocity.linear + a->velocity.angular.cross(ra));

	float verr = rv.dot(n);
	float deltaVelocity = verr + perr;
		
	float lambda = deltaVelocity * effm;
	lambdaCache += lambda;
	if (lambdaCache < 0.0f) {
		lambda -= lambdaCache;
		lambdaCache = 0.0f;
	}

	a->velocity.linear  += n * (lambda * a->imass);
	b->velocity.linear  -= n * (lambda * b->imass);
	a->velocity.angular += ia * lambda;
	b->velocity.angular -= ib * lambda;

	rv = (b->velocity.linear + b->velocity.angular.cross(rb)) - 
		 (a->velocity.linear + a->velocity.angular.cross(ra));
	
	vec3 tn = rv - n * rv.dot(n);
	if (tn == vec3::ZERO)
		return;
	tn.normalize();

	float tangentVelocityError = rv.dot(tn);

	vec3 tia = a->tensor * (ra.cross(tn)) * a->imass;
	vec3 tib = b->tensor * (rb.cross(tn)) * b->imass;

	float teffm = 1.0f / ( a->imass + b->imass + tn.dot(tia.cross(ra) + tib.cross(rb)) );

	float tangentDeltaVelocity = tangentVelocityError;

	float tangentLambda = tangentDeltaVelocity * teffm;

	tangentLambda = _min(tangentLambda, lambdaCache * 0.5f);

	// Exchange the correctional momentum between the bodies
	a->velocity.linear  += tn * (tangentLambda * a->imass);
	b->velocity.linear  -= tn * (tangentLambda * b->imass);
	a->velocity.angular += tia * tangentLambda;
	b->velocity.angular -= tib * tangentLambda;
}


// World::Arbiter
World::Arbiter::Arbiter(Body *a, Body *b) : next(NULL), a(a), b(b) {}

World::Arbiter::~Arbiter() {
	delete next;
	delete constraints.first;
}

bool World::Arbiter::add(const vec3 &pa, const vec3 &pb, const vec3 &n, int stamp) {
	vec3 p = (pa + pb) * 0.5f;
	Constraint *c = constraints.first;
	while (c) {
//		LOG("%d %f\n", constraints.count, (c->p - p).length());
		if ((c->p - p).length2() < 0.5f * 0.5f)
			break;
		c = c->next;
	}

	vec3 oa = a->irot * (pa - a->pos);
	vec3 ob = b->irot * (pb - b->pos);

	if (c) {
		c->oa = oa;
		c->ob = ob;
		c->n  = n;
	} else
		constraints.add(new Constraint(oa, ob, p, n, stamp));

	return c == NULL;
}

void World::Arbiter::init(float stepTime, int stamp) {
	Constraint *c = constraints.first;
	while (c)
		if (c->stamp != stamp && ((a->pos + a->rot * c->oa) - (b->pos + b->rot * c->ob)).length2() > 0.05f * 0.05f) {
			Constraint *n = c->next;
			delete constraints.remove(c);
			a->sleeping = false;
			b->sleeping = false;
			c = n;
		} else {
			c->init(a, b, stepTime);
			c = c->next;
		}
}

void World::Arbiter::solve(float stepTime, bool pseudo) {
	Constraint *c = constraints.first;
	while (c) {
		c->solve(a, b, stepTime, pseudo);
		c = c->next;
	}
}


// World
World::World(const vec3 &gravity) : gravity(gravity), stamp(0) {}
		
World::~World() {
	delete arbiters.first;
}

Body* World::add(Body *body) {
	return list.add(body);
}

Body* World::remove(Body *body) {
	return list.remove(body);
}

void World::simulate(float stepTime, World::Callback callback) {
	stamp++;

	Body *body = list.first;
	while (body) {
		if (body->imass > 0.0f && !body->sleeping)
			body->velocity.linear += gravity * stepTime;
		body = body->next;
	}

// check for collisions
	int collisions = 0;

	Contact contact;
	Body *a = list.first;
	while (a) {
		Body *b = a->next;
		while (b) {
			if (a != b && !(a->sleeping && b->sleeping)) {
				float maxRadius = a->maxRadius + b->maxRadius;
				maxRadius *= maxRadius;
				if ((b->pos - a->pos).length2() < maxRadius && a->collide(*b, &contact)) {
					Arbiter *arb = arbiters.first;
					while (arb) {
						if (arb->a == a && arb->b == b)
							break;
						arb = arb->next;
					}

					if (!arb)
						arb = arbiters.add(new Arbiter(a, b));

					//vec3 &n = contact.n;
					//LOG("%f %f %f\n", n.x, n.y, n.z);

					if (arb->add(contact.a, contact.b, contact.n, stamp) && callback)
						callback(a, b, contact.a, contact.b, contact.n);

					b->sleeping = false;
				}
			}
			b = b->next;
		}
		a = a->next;
	}
//	LOG("collisions: %d\n", collisions);

// solve constraints
	for (int i = 0; i < 4; i++) {
		Arbiter *arb = arbiters.first;
		while (arb) {
			if (!(arb->a->sleeping && arb->b->sleeping)) {
				if (i == 0)
					arb->init(stepTime, stamp);
				arb->solve(stepTime, false);
			}
			arb = arb->next;
		}
	}

// apply velocities
	body = list.first;
	while (body) {
		if (!body->sleeping) {
			if (body->cache.pos.equal(body->pos, 0.01f) && 
				body->cache.rot.equal(body->rot, 0.01f) && 
				body->velocity.linear.equal(vec3::ZERO, 0.01f) && 
				body->velocity.angular.equal(vec3::ZERO, 0.001f)) {
				body->sleeping = true;
			} else {
				body->pos += (body->velocity.linear + body->velocity.pseudo) * stepTime;
				body->velocity.pseudo = vec3::ZERO;

				vec3 &a = body->velocity.angular;
				body->rot = body->rot + (quat(a.x, a.y, a.z, 0.0f) * 0.5f * body->rot) * stepTime;
				body->rot.normalize();
				body->irot = body->rot.inverse();

				body->cache.pos = body->pos;
				body->cache.rot = body->rot;

				body->velocity.linear *= 0.995f;
				body->velocity.angular *= 0.995f;
			}
		}
		body = body->next;
	}
}