#ifndef H_CORE
#define H_CORE

#ifdef WIN32
	#include <windows.h>
	#include <gl/GL.h>
	#include <gl/glext.h>
	#include <gl/wglext.h>
	#include <intrin.h>
#elif LINUX
	#include <GL/gl.h>
	#include <GL/glx.h>
#elif MAC
	#include <OpenGL/gl.h>
	#include <OpenGL/glext.h>
	#include <AGL/agl.h>
	#include <sys/sysctl.h>
#elif ANDROID
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
	#define MOBILE
	#define GL_TEXTURE_COMPARE_MODE		0x884C
	#define GL_TEXTURE_COMPARE_FUNC		0x884D
	#define GL_COMPARE_REF_TO_TEXTURE	0x884E
#elif IOS
	#define MOBILE
#elif ARM
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
	#define MOBILE
#elif __EMSCRIPTEN__
	#include <emscripten.h>
	#include <html5.h>
	#include <GLES3/gl3.h>
	#include <GLES3/gl2ext.h>
	#define MOBILE
#endif

#include "utils.h"
#include "input.h"
#include "ui.h"

#ifdef WIN32
// Texture
extern PFNGLACTIVETEXTUREPROC				glActiveTexture;
extern PFNGLCOMPRESSEDTEXIMAGE2DPROC		glCompressedTexImage2D;
// Shader
extern PFNGLCREATEPROGRAMPROC				glCreateProgram;
extern PFNGLDELETEPROGRAMPROC				glDeleteProgram;
extern PFNGLLINKPROGRAMPROC					glLinkProgram;
extern PFNGLUSEPROGRAMPROC					glUseProgram;
extern PFNGLGETPROGRAMINFOLOGPROC			glGetProgramInfoLog;
extern PFNGLCREATESHADERPROC				glCreateShader;
extern PFNGLDELETESHADERPROC				glDeleteShader;
extern PFNGLSHADERSOURCEPROC				glShaderSource;
extern PFNGLATTACHSHADERPROC				glAttachShader;
extern PFNGLCOMPILESHADERPROC				glCompileShader;
extern PFNGLGETSHADERINFOLOGPROC			glGetShaderInfoLog;
extern PFNGLGETUNIFORMLOCATIONPROC			glGetUniformLocation;
extern PFNGLUNIFORM1IVPROC					glUniform1iv;
extern PFNGLUNIFORM3FVPROC					glUniform3fv;
extern PFNGLUNIFORM4FVPROC					glUniform4fv;
extern PFNGLUNIFORMMATRIX4FVPROC			glUniformMatrix4fv;
extern PFNGLBINDATTRIBLOCATIONPROC			glBindAttribLocation;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC		glEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC	glDisableVertexAttribArray;
extern PFNGLVERTEXATTRIBPOINTERPROC			glVertexAttribPointer;
// Mesh
extern PFNGLGENBUFFERSARBPROC				glGenBuffers;
extern PFNGLDELETEBUFFERSARBPROC			glDeleteBuffers;
extern PFNGLBINDBUFFERARBPROC				glBindBuffer;
extern PFNGLBUFFERDATAARBPROC				glBufferData;
extern PFNGLBUFFERSUBDATAARBPROC			glBufferSubData;
// FBO
extern PFNGLGENFRAMEBUFFERSPROC				glGenFramebuffers;
extern PFNGLBINDFRAMEBUFFERPROC				glBindFramebuffer;
extern PFNGLFRAMEBUFFERTEXTURE2DPROC		glFramebufferTexture2D;
extern PFNGLGENRENDERBUFFERSPROC			glGenRenderbuffers;
extern PFNGLBINDRENDERBUFFERPROC			glBindRenderbuffer;
extern PFNGLFRAMEBUFFERRENDERBUFFERPROC		glFramebufferRenderbuffer;
extern PFNGLRENDERBUFFERSTORAGEPROC			glRenderbufferStorage;
#endif

struct ResObject {
	ResObject	*next;
	char		*name;
	int			refCount;
	int			param;

	ResObject(Stream *stream, int param);
	virtual ~ResObject();
};

#include "texture.h"
#include "shader.h"
#include "mesh.h"

enum RenderMode { rmNormal, rmShadow, rmDeferred, rmMAX };
enum CullMode { cfNone, cfBack, cfFront };
enum BlendMode { bmNone, bmAlpha, bmAdd, bmMultiply, bmScreen };

namespace Core {
	extern List<ResObject>	*list;
	extern int				width, height;
	extern float			deltaTime;
	extern mat4				mProj, mView, mViewProj, mModel;
	extern vec3				viewPos;
	extern vec3				projRatio;
	extern RenderMode		mode;
	extern Texture			*whiteTex;

	struct Support {
		float	anisotropic;
		bool	depthTexture;
		bool	shadowSampler;
	};
	extern Support support;

	struct Active {
		Shader	*shader;
		Texture	*texture[sMAX];
		Mesh	*mesh;
	};
	extern Active active;

	struct Lights {
		mat4 mViewProj;
		vec3 direction;
		vec3 pos;
		vec3 ambient;
	};
	extern Lights	lights;

	extern ResObject *find(const char *name, int param);
	extern void init();
	extern void free();

	template <typename ResClass>
	ResClass *load(const char *name, int param = 0) {
		if (!name) return NULL;
		ResObject *res = find(name, param);
		if (res) {
			res->refCount++;
			return (ResClass*)res;
		}

		Stream stream(name);
		return (ResClass*)list->add(new ResClass(&stream, param));
	}

	extern void unload(ResObject *res);
	extern void clear(const vec4 &color);
	extern void setViewport(int x, int y, int w, int h);
	extern void setTarget(Texture *color, Texture *normal, Texture *depth);
	extern void setTarget(Texture *texture);
	extern void setMatrix(mat4 *proj, mat4 *view, mat4 *model);
	extern void setCulling(CullMode mode);
	extern void setBlending(BlendMode mode);
	extern bool isVisible(const quat2 &basis, float radius);
	extern bool isVisible(const quat2 &basis, const vec3 &size);
}

#include "model.h"

#endif