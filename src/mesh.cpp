#include "core.h"

Mesh::Mesh(Stream *stream) : stride(sizeof(Vertex)) {
	Index	*indices  = stream->readArray<Index>(iCount = stream->read<int>());
	Vertex	*vertices = stream->readArray<Vertex>(vCount = stream->read<int>());

	glGenBuffers(2, ID);		
	bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(Index), indices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	delete[] vertices;
	delete[] indices;
}

Mesh::Mesh(Index *indices, void *vertices, int iCount, int vCount, int stride) : iCount(iCount), vCount(vCount), stride(stride) {
	glGenBuffers(2, ID);
	bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iCount * sizeof(Index), indices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, vCount * stride, vertices, GL_STATIC_DRAW);
}

Mesh::~Mesh() {
	glDeleteBuffers(2, ID);
}

void Mesh::bind() {
	if (Core::active.mesh != this) {
		Core::active.mesh = this;

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID[0]);
		glBindBuffer(GL_ARRAY_BUFFER, ID[1]);
	}
}

void Mesh::update(Index *indices, void *vertices, int iCount, int vCount) {
	bind();
	if (indices)
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, iCount * sizeof(Index), indices);
	if (vertices)
		glBufferSubData(GL_ARRAY_BUFFER, 0, vCount * stride, vertices);
}

void Mesh::render(int iStart, int iCount) {
	bind();
	ASSERT(sizeof(Vertex) != sizeof(Vertex2));
	switch (stride) {
		case sizeof(Vertex) : {
			Vertex *v = NULL;
			glVertexAttribPointer(aCoord,    4, GL_SHORT, false, stride, &v->coord);
			glVertexAttribPointer(aTexCoord, 4, GL_SHORT, false, stride, &v->texCoord);
			glVertexAttribPointer(aNormal,   4, GL_UNSIGNED_BYTE, true, stride, &v->normal);
			glVertexAttribPointer(aTangent,  4, GL_UNSIGNED_BYTE, true, stride, &v->tangent);
			glDrawElements(GL_TRIANGLES, iCount, GL_UNSIGNED_SHORT, (GLvoid*)(iStart * sizeof(Index)) );
			break;
		}
		case sizeof(Vertex2) : {
			glDisableVertexAttribArray(aNormal);
			glDisableVertexAttribArray(aTangent);
			Vertex2 *v = NULL;
			glVertexAttribPointer(aCoord,    3, GL_FLOAT, false, stride, &v->coord);
			glVertexAttribPointer(aTexCoord, 2, GL_FLOAT, false, stride, &v->texCoord);
			glDrawElements(GL_TRIANGLES, iCount, GL_UNSIGNED_SHORT, (GLvoid*)(iStart * sizeof(Index)) );
			glEnableVertexAttribArray(aNormal);
			glEnableVertexAttribArray(aTangent);
			break;
		}
	}
}
