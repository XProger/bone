#include "level.h"
#include "enemy.h"
#include "debug.h"

// Level::Object
Level::Entity::Entity() : ground(NULL), rot(0, 0, 0, 1), trot(rot), pos(0.0f), velocity(0.0f), velocityCtrl(0.0f) {}

void Level::Entity::spawn(const vec3 &pos) {
	this->pos = pos;
	this->velocity = vec3::ZERO;
	this->ground = NULL;
}

void Level::Entity::hit(int damage) {
	//
}

void Level::Entity::setBasis(const quat2 &basis) {
	trot = basis.getRot();
	pos  = basis.getPos();
}

quat2 Level::Entity::getBasis() {
	return quat2(trot, pos);
}

// Level
Level::Level(const char *name) : model(new Model(name)) {
	model->basis = quat2(quat(0, 0, 0, 1), vec3(0, 0, 0));

	Core::lights.direction	= vec3(1.0f).normal();
	for (int i = 0; i < model->res->objects.count; i++) {
		Model::Resource::Object *obj = model->res->objects[i];
		if (obj->type & Model::Resource::Object::OBJ_LIGHT) {
			Core::lights.pos = obj->basis.getPos();
			Core::lights.direction = Core::lights.pos.normal();	
			break;
		}
	}

	Core::lights.ambient = vec3(0.1f, 0.2f, 0.3f); //vec3(0.376f, 0.741f, 0.812f) * 0.5f;

	shadow = new Texture(2048, 2048, sfShadow);

	cameraIndex = -1;
	setCamera(model->res->cameraID);
	camera.t = 0.0f;
}

Level::~Level() {
	delete shadow;
	delete model;
	while (entities.first)
		delete entities.remove(entities.first);
}

Level::Entity* Level::add(Entity *entity) {
	entity->level = this;
	return entities.add(entity);
}

Level::Entity* Level::remove(Entity *entity) {
	entity->level = this;
	return entities.remove(entity);
}

void Level::activate(Entity *entity, int triggerIndex) {
	if (model->state.objects[triggerIndex].time != 0.0f)
		return;

	Model::Resource::Object *obj = model->res->objects[triggerIndex];

	if (entity && obj->damage)
		entity->hit(obj->damage);

	if (obj->camera > -1)
		setCamera(obj->camera);

	int count = obj->lCount;
	while (count--) {
		Model::State::Object &state = model->state.objects[obj->links[count]];

		// TODO: move into Model::play
		if (state.flags & Model::State::FLAG_PLAYING)
			continue;
		state.flags |= Model::State::FLAG_PLAYING;
	}
}

void Level::activate(Entity *entity, bool action) {
	float dist = 0.0f, radius = 0.7f;
	vec3 n, pos = entity->pos;
	pos.y += radius;

	Array<Model::Resource::Object> &objects = model->res->objects;
	for (int i = 0; i < objects.count; i++) {
		Model::Resource::Object *obj = objects[i];
		if (!(obj->type & Model::Resource::Object::OBJ_TRIGGER))
			continue;
		
		if (!(obj->type & (Model::Resource::Object::TRIG_BOX |  Model::Resource::Object::TRIG_SPHERE)))
			continue;

		if (action != ((obj->flags & Model::State::FLAG_ACTION) > 0))
			continue;

		vec3 p = model->state.objects[i].basis.inverse() * pos;
		
		if (obj->type & Model::Resource::Object::TRIG_BOX)
			if (!Box(obj->size).intersect(Sphere(p, radius), n, dist))
				continue;

		if (obj->type & Model::Resource::Object::TRIG_SPHERE)
			if (!Sphere(obj->radius).intersect(Sphere(p, radius), n, dist))
				continue;

		bool ready = true;
		int count = obj->lCount;
		while (count--)
			if (model->state.objects[obj->links[count]].flags & Model::State::FLAG_PLAYING) {
				ready = false;
				break;
			}

		if (!ready)
			continue;

		activate(entity, i);

		if (obj->flags & Model::State::FLAG_DEACTIVATE)
			model->state.objects[i].time = 1.0f;
	}
}

void Level::move(Entity *entity) {
	if (entity->ground) {
		if (*entity->ground != entity->groundLast) {
			quat2 basis = *entity->ground * entity->groundLast.inverse() * entity->getBasis();
			basis.real.normalize();
			entity->setBasis(basis);
		}
		entity->ground	= NULL;
	}

	float radius = 0.5f, expansion = 0.1f;
	vec3 dest, cn, diff = (entity->velocity + entity->velocityCtrl) * Core::deltaTime;
	bool collide = false;

	dest = entity->pos; 

	while (diff.length() > EPS) {
		vec3 delta = diff.normal() * (radius * 0.5f);
		if (delta.length() > diff.length())
			delta = diff;
		diff -= delta;

		dest += delta;
		dest.y += radius;

		Array<Model::Resource::Object> &objects = model->res->objects;
		for (int i = 0; i < objects.count; i++) {
			Model::Resource::Object *obj = objects[i];
			if (!(obj->type & Model::Resource::Object::OBJ_GEOMETRY))
				continue;
			int count = obj->colliders->count;
			while (count--) {
				Collider *collider = (*obj->colliders)[count];
				quat2 basis = model->state.objects[i].basis * collider->basis;
/*
				Capsule capsule(dest, dest + vec3(0, 1.7f - radius - radius, 0), radius);
				capsule.a = invBasis * capsule.a;
				capsule.b = invBasis * capsule.b;

				float t;
				if (capsule.intersect(collider.box, n, t)) {
					n = basis.getRot() * n;
					dest += n * t;
					//if (abs(n.dot(vec3(0, 1, 0))) > 0.6f) {
					if (n.y > 0.0f) {
						entity->velocity.y = 0.0f;							
						entity->ground = &model->state.objects[i].basis;
						entity->groundLast = *(entity->ground);
					}

				}
*/			
				vec3 n;
				float t;
				if (collider->collide(Sphere(basis.inverse() * dest, radius + expansion), n, t)) {
					n = basis.getRot() * n;
					if (t >= expansion) {
						dest += n * (t - expansion);
						if (!collide)
							cn = n;

						collide = true;
					}
					
					//if (abs(n.dot(vec3(0, 1, 0))) > 0.6f) {
					if (n.y > 0.0f) {
						//LOG("%f %f %f\n", n.x, n.y, n.z);
						entity->ground = &model->state.objects[i].basis;
						entity->groundLast = *(entity->ground);
					}
				}
				
			}
		}
		dest.y -= radius;
	}
	
	if (collide) {
		vec3 &v = entity->velocity;
		entity->groundNormal = cn;
//		vec3 k = v;
		v = v - cn * cn.dot(v);
	//	vec3 p = v;//cn;//v.normal();
		//LOG("%f %f %f\t%f %f %f\n", k.x, k.y, k.z, p.x, p.y, p.z);
	//	v = v - n * n.dot(v);
	}

	entity->pos = dest;

	activate(entity, false);
}

void Level::deactivate() {
	Array<Model::Resource::Object> &objects = model->res->objects;
	for (int i = 0; i < objects.count; i++) {
		Model::Resource::Object *obj = objects[i];
		if (!(obj->type & Model::Resource::Object::OBJ_TRIGGER))
			continue;

		if (obj->flags & Model::State::FLAG_DEACTIVATE && model->state.objects[i].time == -1.0f) {
			model->state.objects[i].time = 0.0f;
			activate(NULL, i);
		}
	}
}

void Level::setupLightCamera() {
	float SIZE = 10.0f * SQRT2;

	vec3 pos = entities.first ? entities.first->pos : vec3::ZERO;
	Core::mView = mat4(Core::lights.direction * SIZE + pos, pos, vec3(0, 1, 0)).inverse();
	Core::mProj = mat4(-SIZE, SIZE, -SIZE, SIZE, camera.znear, camera.zfar);
	Core::mViewProj = Core::mProj * Core::mView;

	mat4 bias;
	bias.identity();
	bias.e03 = bias.e13 = bias.e23 = bias.e00 = bias.e11 = bias.e22 = 0.5f;

	Core::lights.mViewProj = bias * Core::mProj * Core::mView;
}

void Level::setCamera(int index) {
//	if (cameraIndex == index)
//		return;

	Model::Resource::Object *obj = model->res->objects[index];
	if (!(obj->type & Model::Resource::Object::OBJ_CAMERA)) {
		LOG("! object is not a camera");
		return;
	}

	cameraIndex = index;

//	const quat2 qGL = quat2(quat(0.0f, 0.707106769f, 0.707106769f, 0.0f), quat(0.0f, 0.0f, 0.0f, 0.0f));
//	mat4 m(qGL);
	camera.setCamera(obj->type >> 16, obj->basis, obj->fov * RAD2DEG, obj->znear, obj->zfar, 0.5f, obj->curve);
}

void Level::setCameraFrame(int index) {
	int camIndex = 1;
	for (int i = 0; i < model->res->objects.count; i++)
		if (model->res->objects[i]->type & Model::Resource::Object::OBJ_CAMERA && (camIndex-- <= 0) ) {
			Model::Resource::Object *obj = model->res->objects[i];
			quat2 basis;

			if (obj->animationID > -1) {
				Animation *anim = model->res->animations[obj->animationID];
				if (index < 0 || index >= anim->frames) {
					index = anim->frames - 1;
					LOG("! camera index out of range\n");
				}
				basis = anim->joints[0]->basis[index];
			} else {
				basis = obj->basis;
				LOG("! camera no frames\n");
			}
			//const quat2 qGL = quat2(quat(0.0f, 0.707106769f, 0.707106769f, 0.0f), quat(0.0f, 0.0f, 0.0f, 0.0f));
//			camera.basisTarget = quat2(quat(0, 0, 0, 1), vec3(0, 0, 10)) * basis;// * qGL; 
			//const quat2 qGL = quat2(quat(vec3(0, 0, 1), PI), quat(0.0f, 0.0f, 0.0f, 0.0f));

			camera.setCamera(obj->type >> 16, basis, obj->fov * RAD2DEG, obj->znear, obj->zfar, 0.5f);
			break;
		}
}


void Level::update() {
	model->update();
	Entity *entity = entities.first;
	while (entity) {
		entity->update();
		entity = entity->next;
	}

// collision check
	entity = entities.first;
	while (entity) {
		move(entity);
		entity = entity->next;
	}

	deactivate();

	camera.update();
}

static float f = 0.0f;

void Level::render() {
	shadow->bind(sShadow);
	camera.setup();
//	Core::mViewProj = Core::lights.mViewProj;
	model->sort();
	renderScene();
}

void Level::renderShadows() {
	Core::setBlending(bmNone);
	Core::mode = rmShadow;
	Core::whiteTex->bind(sShadow);
	Core::setTarget(shadow);
	Core::setViewport(0, 0, shadow->width, shadow->height);
	Core::clear(vec4(1.0));
	setupLightCamera();
	renderScene();
	Core::setTarget(NULL);
	Core::setBlending(bmAlpha);
	Core::mode = rmNormal;
}

void Level::renderScene() {
	Entity *entity = entities.first;
	while (entity) {
		entity->render();
		entity = entity->next;
	}
	model->render();
}

void Level::renderDebug() {
//#ifdef _DEBUG
	Debug::Draw::begin();
	Debug::Draw::axes(10.0f);

	Array<Model::Resource::Object> &objects = model->res->objects;
	for (int i = 0; i < objects.count; i++) {
		Model::Resource::Object *obj = objects[i];

		if (obj->type & Model::Resource::Object::OBJ_TRIGGER) {
			glLineWidth(3);
			glPushMatrix();

			mat4 m(model->state.objects[i].basis);
			glMultMatrixf((GLfloat*)&m);

			float dist = 0.0f, radius = 0.7f;
			vec3 pos = entities.first ? entities.first->pos : vec3::ZERO;
			pos.y += radius;

			vec3 n, p = model->state.objects[i].basis.inverse() * pos;

			if (obj->type & Model::Resource::Object::TRIG_BOX) {
				Box box(obj->size);
				if (box.intersect(Sphere(p, radius), n, dist))
					glColor3f(0, 1, 0);
				else
					if (obj->flags & Model::State::FLAG_ACTION)
						glColor3f(1, 1, 0);
					else
						glColor3f(1, 0.5f, 0);
				Debug::Draw::box(box);
			}

			if (obj->type & Model::Resource::Object::TRIG_SPHERE) {
				Sphere sphere(obj->radius);
				if (sphere.intersect(Sphere(p, radius), n, dist))
					glColor3f(0, 1, 0);
				else
					if (obj->flags & Model::State::FLAG_ACTION)
						glColor3f(1, 1, 0);
					else
						glColor3f(1, 0.5f, 0);
				Debug::Draw::sphere(sphere);
			}

			glPopMatrix();
		}
		
		if (obj->type & Model::Resource::Object::OBJ_GEOMETRY) {
			glLineWidth(2);

			int count = obj->colliders->count;
			while (count--) {
				glPushMatrix();

				Collider *collider = (*obj->colliders)[count];
				quat2 basis = model->state.objects[i].basis * collider->basis;
				mat4 m(basis);
				glMultMatrixf((GLfloat*)&m);
				
				
				glColor3f(1, 1, 1);
				if (collider->type == Collider::BOX)
					Debug::Draw::box(Box(collider->size));
				if (collider->type == Collider::SPHERE)
					Debug::Draw::sphere(Sphere(collider->radius));	
				if (collider->type == Collider::MESH)
					Debug::Draw::mesh(collider->mVertices, collider->indices, collider->iCount);
				//if (collider->type = Collider::CONTOUR)
				//	debug_wire_contour(collider->indices, collider->cVertices);

				glPopMatrix();
			}
		}

		if (obj->type & Model::Resource::Object::OBJ_CAMERA && obj->type & Model::Resource::Object::CAM_CURVE) {
			glColor3f(1, 1, 0.5);
			Debug::Draw::curve(obj->curve);
			
			glColor3f(1.0f, 0.0f, 1.0f);
		
			glBegin(GL_POINTS);
			//	float t = camera.curve->closest(camera.target.getPos()) - 0.05f;
			//	vec3 p = camera.curve->eval(t);
			//	glVertex3f(p.x, p.y, p.z);
			glEnd();
		}
	}
	Debug::Draw::end();
//#endif
}

NavMesh::Path* Level::findPath(Entity *from, Entity *target) {
	if (!model->res->navmesh)
		return NULL;
	return model->res->navmesh->findPath(from->pos, target->pos);
}
