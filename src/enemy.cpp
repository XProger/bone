#include "enemy.h"

#define GRAVITY		9.81f
#define JUMP_ACC	(GRAVITY * 0.6f)

// Enemy
Enemy::Enemy(const char *name) : Level::Entity(), state(STATE_IDLE1), model(new Model(name)), target(NULL), path(NULL), input(0), angle(0.0f), health(100), idleTimer(0.0f) {}

Enemy::~Enemy() {
	delete model;
	delete path;
}

void Enemy::spawn(const vec3 &pos) {
	Level::Entity::spawn(pos);
	health = 100;
	state = STATE_IDLE1;
}

void Enemy::updateInput() {
	input = 0;
}

void Enemy::update() {
	model->update();
}

void Enemy::render() {
	if (!(rot == trot))
		rot = rot.slerp(trot, 5.0f * Core::deltaTime);

	model->basis = quat2(rot, pos);
	model->render();
}

void Enemy::hit(int damage) {
	if (state == STATE_DEAD || hitTimer > 0.0f)
		return;
	health = _max(0, health - damage);
	hitTimer = 1.0f;
	state = STATE_DAMAGE;
}

// Player
Player::Player(const char *name, const char *weaponName) : Enemy(name), weapon(NULL) {
	model->play("idle", 0.0f, 1.0f, true);
	if (weaponName)
		weapon = new Model(weaponName);
}

Player::~Player() {
	delete weapon;
}

void Player::updateInput() {
	Enemy::updateInput();

	if (Input::down[ikA])				input |= DIR_L;
	if (Input::down[ikD])				input |= DIR_R;
	if (Input::down[ikW])				input |= DIR_U;
	if (Input::down[ikS])				input |= DIR_D;
	if (Input::joy.L.length2() > EPS)	input |= DIR_JOY;

	if (input & DIR) {
		if (input & DIR_JOY)
			inputDir = vec3(-Input::joy.L.x, 0.0f, -Input::joy.L.y);
		else {
			inputDir = vec3::ZERO;
			if (input & DIR_U) inputDir.z += 1.0f;
			if (input & DIR_L) inputDir.x += 1.0f;
			if (input & DIR_D) inputDir.z -= 1.0f;
			if (input & DIR_R) inputDir.x -= 1.0f;
		}
		inputDir.normalize();
	} else
		inputDir = vec3::ZERO;

	if (Input::down[ikSpace] || Input::joy.down[4])	input |= JUMP;
	if (Input::down[ikQ] || Input::joy.down[0])		input |= ATTACK_1;
	if (Input::down[ikE] || Input::joy.down[3])		input |= ATTACK_2;
	if (Input::down[ikEnter] || Input::joy.down[1])	input |= ACTION;

	if ( level->camera.type == Camera::TYPE_CURVE || inputDir.dot(inputDirLast) < 0.98f ) {
		vec3 o = this->level->camera.basis.getRot() * vec3(0, 0, 1);
		inputAngle = atan2(o.x, o.z);
		inputDirLast = inputDir;
	}
}

void Player::update() {
	updateInput();

	if (state != STATE_ATTACK1 && state != STATE_ATTACK2 && state != STATE_DAMAGE && state != STATE_JUMP && state != STATE_DEAD && state != STATE_ACTION) {
		if (input & DIR) {
			state = STATE_WALK;
			if (input & ATTACK_1) state = STATE_WALK_ATTACK1;
			if (input & ATTACK_2) state = STATE_WALK_ATTACK2;
		} else {
			if (state != STATE_IDLE2)
				state = STATE_IDLE1;
			if (input & ATTACK_1) state = STATE_ATTACK1;
			if (input & ATTACK_2) state = STATE_ATTACK2;
		}

		if (input & JUMP) {
			state = STATE_JUMP;
			velocity.y = JUMP_ACC;
		}

		if (!ground)
			state = STATE_JUMP;

		if (input & ACTION) {
			level->activate(this, true);
			state = STATE_ACTION;
		}
	}

	if (health <= 0)
		state = STATE_DEAD;

	if (hitTimer > 0.0f)
		hitTimer -= Core::deltaTime;

	if (state == STATE_IDLE1) {
		if (idleTimer > 0.0f) {
			idleTimer -= Core::deltaTime;
			if (idleTimer <= 0.0f)
				state = STATE_IDLE2;
		} else
			idleTimer = 2.0f + randf() * 3.0f;
	}

	switch (state) {
		case STATE_WALK_ATTACK1 :
			model->play("run_attack1", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_WALK;
			break;
		case STATE_WALK_ATTACK2 :
			model->play("run_attack2", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_WALK;
			break;
		case STATE_IDLE1 :
			model->play("idle", 0.2f, 1.0f, true);
			break;
		case STATE_IDLE2 :
			model->play("idle", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_WALK :
			model->play("run", 0.1f, 1.0f, true);
			break;
		case STATE_JUMP :
			if (ground)
				state = STATE_IDLE1;
			model->play("jump", 0.1f, 1.0f, true);
			break;
		case STATE_ATTACK1 :
			model->play("attack1", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_ATTACK2 :
			model->play("attack2", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_ACTION :
			model->play("idle", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_DAMAGE :
			model->play("damag", 0.1f, 1.0f, false);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_DEAD :
			model->play("death", 1.0f, 1.0f, false);
			break;
	}

	vec3 v(0.0f);

	if (state == STATE_WALK || state == STATE_JUMP || state == STATE_WALK_ATTACK1 || state == STATE_WALK_ATTACK2)
		if (inputDir.length2() > EPS) {
			vec3 dir = quat(vec3(0, 1, 0), inputAngle) * inputDir;

			angle = atan2(dir.x, dir.z);

			trot = quat(vec3(0, 1, 0), angle);
			v = trot * vec3(0.0f, 0.0f, 1.0f);
			if (ground) {
				vec3 r = groundNormal.cross(v);
				v = r.cross(groundNormal).normal();
			}
			v *= -5.5f;
		}

	velocityCtrl = v;

	velocity.x = 0.0f;
	velocity.z = 0.0f;
	velocity.y -= GRAVITY * Core::deltaTime;

//	LOG("%f %f %f\n", pos.x, pos.y, pos.z);
/*
	vec3 n = vec3(1.0f, 1.0f, 0.0f).normal();
	v = vec3(0.0f, -1.0f, 0.0f);
	v = v - n * n.dot(v);

	LOG("%f %f %f\n", v.x, v.y, v.z);
	*/

	Enemy::update();
}

void Player::render() {
	Enemy::render();
	
	if (weapon) {
		int jIndex = model->res->skeleton->getJointIndex("CTRL_wearpon");

		if (jIndex != -1)
			weapon->basis = model->basis * model->state.joints[jIndex].absolute;
		weapon->render();
	}
}


// Tank
Tank::Tank(const char *name) : Enemy(name) {
	model->play("idle", 0.2f, 1.0f, true);
}

void Tank::updateInput() {
	Enemy::updateInput();
}

void Tank::update() {
	vec3 dir;

	if (target) {
		dir = pos - target->pos;

		if (state != STATE_ATTACK1) {
			if (dir.length() > 3.0f) {
				state = STATE_WALK;
			} else
				if (dir.length() < 1.8f && target->health > 0) {
					trot = quat(vec3(0, 1, 0), atan2(dir.x, dir.z));
					if (state != STATE_ATTACK1) {
						state = STATE_ATTACK1;
						if (dir.length() < 1.5f)
							target->hit(10);
					}
				} else
					if (state != STATE_WALK)
						state = STATE_IDLE1;
		}
	} else
		dir = vec3(0.0f);

	delete path;
	path = NULL;

	if (state == STATE_WALK) {
		if (target) {
			path = level->findPath(this, target);
			if (path)
				path->next();
			else
				state = STATE_IDLE1;
		}

		if (dir.length() < 2.1f)
			state = STATE_IDLE1;
	}

	vec3 v(0.0f);
	switch (state) {
		case STATE_IDLE1 :
			model->play("idle", 0.2f, 1.0f, true);
			break;
		case STATE_ATTACK1 :
			model->play("attack", 0.2f, 1.0f, true);
			if (!model->isPlaying(0.2f))
				state = STATE_IDLE1;
			break;
		case STATE_WALK :
			dir = path->target - pos;
			dir.y = 0.0f;
			trot = quat(vec3(0, 1, 0), atan2(-dir.x, -dir.z));
			v = dir.normal() * 1.5f;
			model->play("walk", 0.1f, 1.0f, true);
			break;
		default : {}
	}


	velocity.x = v.x;
	velocity.z = v.z;
	velocity.y -= GRAVITY * Core::deltaTime;

	Enemy::update();
}
