#include "utils.h"
#include "minilzo/minilzo.h"

int					Stream::fCount;
FILE				*Stream::f;
Stream::FileItem	*Stream::table;

void Stream::init(const char *pack, int offset) {
	f = fopen(pack, "rb");
	ASSERT(f != NULL);
	fseek(f, offset, SEEK_SET);

	fread(&fCount, 1, 4, f);
	table = new FileItem[fCount];
	for (int i = 0; i < fCount; i++) {
		FileItem &item = table[i];

		fread(&item.size, 1, sizeof(int), f);
		fread(&item.csize, 1, sizeof(int), f);
		fread(&item.offset, 1, sizeof(int), f);

		item.offset += offset;
	// read name
		unsigned char len = 0;
		fread(&len, 1, sizeof(len), f);
		fread(item.name, 1, len, f);
		item.name[len] = 0;
	}
	lzo_init();
}

void Stream::free() {
	delete[] table;
	fclose(f);
}

Stream::Stream(const char *name) : name(String::copy(name)), buffer(NULL), pos(0), size(0) {
	LOG("load: %s\n", name);
	for (int i = 0; i < fCount; i++)
		if (String::equal(table[i].name, name)) {
			FileItem &item = table[i];

			fseek(f, item.offset, SEEK_SET);

			unsigned char *cdata = new unsigned char[item.csize];
			fread(cdata, 1, item.csize, f);
			if (item.csize != item.size) {
				buffer = new unsigned char[item.size];
				int r = lzo1x_decompress(cdata, item.csize, buffer, (lzo_uint*)&size, NULL);
				ASSERT(r == LZO_E_OK);
				delete[] cdata;
			} else
				buffer = cdata;

			size = item.size;

			return;
		}
	ASSERT(false); // file not found
}

Stream::~Stream() {
	delete[] name;
	delete[] buffer;
}

void Stream::seek(int offset) {
	pos += offset;
}

int Stream::read(void *data, int count) {
	memcpy(data, &buffer[pos], count);
	pos += count;
	ASSERT(pos <= size);
	return count;
}

char* Stream::readStr(char *buffer) {
	unsigned char len;
	read(&len, sizeof(len));
	if (!len)
		return NULL;
	read(buffer, len);
	buffer[len] = 0;
	return buffer;
}

char *Stream::readStr() {
	unsigned char len;
	read(&len, sizeof(len));
	if (!len)
		return NULL;
	char *str = new char[len + 1];
	read(str, len);
	str[len] = 0;
	return str;
}

quat2 Stream::readQuat2() {
	quat rot;
	vec3 pos;
	read(&rot, sizeof(rot.xyz));
	read(&pos, sizeof(pos));
	rot.recalc();
	return quat2(rot, pos);
}

#define INV_BYTE (1.0f / 255.0f)

vec4 Stream::readColor() {
	ubyte4 color;
	read(&color, sizeof(color));
	return vec4(color.x * INV_BYTE, color.y * INV_BYTE, color.z * INV_BYTE, color.w * INV_BYTE);
}