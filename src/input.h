#ifndef H_INPUT
#define H_INPUT

enum InputKey {	ikNone, 
				ikMouse, ikMouseL, ikMouseR, ikMouseM,
				ikLeft, ikRight, ikUp, ikDown, ikSpace, ikEnter, ikShift,
				ik0, ik1, ik2, ik3, ik4, ik5, ik6, ik7, ik8, ik9,
			//	ikF1, ikF2, ikF3, ikF4, ikF5, ikF6, ikF7, ikF8, ikF9, ikF10, ikF11, ikF12,
				ikA, ikB, ikC, ikD, ikE, ikF, ikG, ikH, ikI, ikJ, ikK, ikL, ikM, ikN, ikO, ikP, ikQ, ikR, ikS, ikT, ikU, ikV, ikW, ikX, ikY, ikZ,
				ikMAX };

enum InputState { isNone, isDown, isUp, isMove };

namespace Input {

	struct Mouse {
		vec2 pos;

		struct Pointer {
			vec2 start;
			bool down;
		} L, R, M;
	};

	struct Joystick {
		vec2 L, R;
		bool down[16];
	};

	extern Joystick joy;
	extern Mouse	mouse;
	extern bool down[ikMAX];
	extern void reset();
	extern void setDown(int code, bool value);
	extern void setMouseDown(int idx, bool down);
	extern void setMouseMove(const vec2 &pos);
}

#endif