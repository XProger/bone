#ifndef H_GAME
#define H_GAME

namespace Game {
	extern void init();
	extern void free();
	extern void update();
	extern void render();
	extern void input(InputKey key, InputState state);
}

#endif