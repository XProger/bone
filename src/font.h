#ifndef H_FONT
#define H_FONT

#include "utils.h"
#include "core.h"

struct Font {
	Material *material;

	Font(const char *name);
	~Font();
	float getWidth(const char *text);
	void print(const vec2 &pos, const vec4 &color, const char *text);
};

#endif