#include <stdio.h>
#include <EGL/egl.h>

#include "core.h"
#include "game.h"

int lastTime, fpsTime, FPS;
EGLDisplay display;
EGLSurface surface;
EGLContext context;

int getTime() {
	return (int)emscripten_get_now();
}

void main_loop() {
	int time = getTime();
	
	if (time - lastTime == 0)
		return;

	Core::deltaTime = _min(0.5f, (time - lastTime) / 1000.0f);
	lastTime = time;

	if (time > fpsTime) {
		fpsTime = time + 1000;
		LOG("FPS: %d\n", FPS);
		FPS = 0;
	}
	
	Game::update();
	Game::render();
	eglSwapBuffers(display, surface);

	FPS++;
}

bool initGL() {
	EGLint vMajor, vMinor, cfgCount;
	EGLConfig cfg;
	
	EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE, EGL_NONE };
	EGLint attribList[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_DEPTH_SIZE, 8,
		EGL_STENCIL_SIZE, EGL_DONT_CARE,
		EGL_SAMPLE_BUFFERS, EGL_DONT_CARE,
		EGL_NONE
	};	
	
	display = eglGetDisplay((EGLNativeDisplayType)EGL_DEFAULT_DISPLAY);
	eglInitialize(display, &vMajor, &vMinor);
	eglGetConfigs(display, NULL, 0, &cfgCount);
	eglChooseConfig(display, attribList, &cfg, 1, &cfgCount);
	surface = eglCreateWindowSurface(display, cfg, NULL, NULL);
	context = eglCreateContext(display, cfg, EGL_NO_CONTEXT, contextAttribs);
	eglMakeCurrent(display, surface, surface, context);
	return true;
}

void freeGL() {
	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);	
	eglDestroyContext(display, context);
	eglDestroySurface(display, surface);
	eglTerminate(display);
}

EM_BOOL keyCallback(int eventType, const EmscriptenKeyboardEvent *e, void *userData) {
	switch(eventType) {
		case EMSCRIPTEN_EVENT_KEYDOWN:
			Input::setDown(e->keyCode, true);
			break;
		case EMSCRIPTEN_EVENT_KEYUP:
			Input::setDown(e->keyCode, false);
			break;
		default:
			break;
	}
	return 1;
}

EM_BOOL resizeCallback(int eventType, const EmscriptenUiEvent *e, void *userData) {
//	Core::width = e->documentBodyClientWidth;
//	Core::height = e->documentBodyClientHeight;
	int f;
	emscripten_get_canvas_size(&Core::width, &Core::height, &f);
	LOG("resize %d x %d\n", Core::width, Core::height);
	return 1;
}

EM_BOOL touchCallback(int eventType, const EmscriptenTouchEvent *e, void *userData) {
	bool flag = false;
	for (int i = 0; i < e->numTouches; i++) {
		long x = e->touches[i].canvasX;
		long y = e->touches[i].canvasY;	
		if (x < 0 || y < 0 || x >= Core::width || y >= Core::height) continue;
		flag = true;
		
		switch (eventType) {
			case EMSCRIPTEN_EVENT_TOUCHSTART :
				if (x > Core::width / 2)
					Input::joy.down[(y > Core::height / 2) ? 1 : 4] = true;
				break;
			case EMSCRIPTEN_EVENT_TOUCHMOVE : 
				if (x < Core::width / 2) {
					vec2 center(Core::width * 0.25f, Core::height * 0.6f);
					vec2 pos(x, y);
					vec2 d = pos - center;

					Input::Joystick &joy = Input::joy;

					joy.L.x = d.x;
					joy.L.y = d.y;
					if (joy.L != vec2(0.0f))
						joy.L.normalize();
				}
				break;
			case EMSCRIPTEN_EVENT_TOUCHEND :
			case EMSCRIPTEN_EVENT_TOUCHCANCEL : {
				Input::joy.L = vec2(0.0f);
				Input::joy.down[1] = false;
				Input::joy.down[4] = false;
				break;
			}
		}
	}
	return flag;
}

EM_BOOL mouseCallback(int eventType, const EmscriptenMouseEvent *e, void *userData) {
	switch (eventType) {
		case EMSCRIPTEN_EVENT_MOUSEDOWN :
		case EMSCRIPTEN_EVENT_MOUSEUP : {
			int idx = e->button == 0 ? 0 : (e->button == 2 ? 1 : 0); 
			Input::mouse.pos = vec2((float)e->clientX, (float)e->clientY);
			bool down = eventType != EMSCRIPTEN_EVENT_MOUSEUP;
			Input::setMouseDown(idx, down);
			break;
		}
		case EMSCRIPTEN_EVENT_MOUSEMOVE :
			Input::setMouseMove(vec2((float)e->clientX, (float)e->clientY));
			break;
	}
	return 1;
}

int main() {
	LOG("init\n");
	initGL();
	
	emscripten_set_canvas_size(Core::width = 1280, Core::height = 720);
	
	emscripten_set_keydown_callback(0, 0, 1, keyCallback);
	emscripten_set_keyup_callback(0, 0, 1, keyCallback);	
	emscripten_set_resize_callback(0, 0, 1, resizeCallback);
	
	emscripten_set_touchstart_callback(0, 0, 1, touchCallback);
	emscripten_set_touchend_callback(0, 0, 1, touchCallback);
	emscripten_set_touchmove_callback(0, 0, 1, touchCallback);
	emscripten_set_touchcancel_callback(0, 0, 1, touchCallback);

	emscripten_set_mousedown_callback(0, 0, 1, mouseCallback);
	emscripten_set_mouseup_callback(0, 0, 1, mouseCallback);
	emscripten_set_mousemove_callback(0, 0, 1, mouseCallback);

	
	Stream::init("data.xpk", 0);
	Game::init();
	
	lastTime	= getTime();
	fpsTime		= lastTime + 1000;
	FPS			= 0;
	
	emscripten_set_main_loop(main_loop, 0, true);
	
	LOG("free\n");
	Game::free();
	Stream::free();
	freeGL();

	return 0;
}