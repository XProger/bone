#include "utils.h"

namespace String {

	bool equal(const char *str1, const char *str2) {
		if (str1 == str2) return true;
		if (str1 == NULL || str2 == NULL) return false;
		return strcmp(str1, str2) == 0;
	}

	int length(const char *str) {
		return str ? strlen(str) : 0;
	}

	char* copy(const char *str) {
		int len = length(str);
		if (!len) return NULL;
		char *res = new char[len + 1];
		strcpy(res, str);
		return res;
	}

}