#ifndef H_MESH
#define H_MESH

#include "core.h"

typedef unsigned short Index;

struct Vertex {
	short4	coord;		// (coord.xyz - geomBias) / geomScale, joint[0,1] * 2
	short4	texCoord;	// (texCoord - texBias) / texScale
	ubyte4	normal;		// (normal.xyz * 0.5 + 0.5) * 255, weight[0] * 255
	ubyte4	tangent;	// (tangent.xyz, binormal_sign) * 0.5 + 0.5 * 255
};

struct Vertex2 {
	vec3	coord;
	vec2	texCoord;
};

struct Mesh {
	GLuint	ID[2];

	int		iCount;
	int		vCount;
	int		stride;

	Mesh(Index *indices, void *vertices, int iCount, int vCount, int stride);
	Mesh(Stream *stream);
	virtual ~Mesh();
	void bind();
	void update(Index *indices, void *vertices, int iCount, int vCount);
	void render(int iStart, int iCount);
};

#endif