#include "core.h"

Material::Material(Stream *stream) : options(stream->read<int>()) {
	color.diffuse = stream->readColor();

	//options &= ~(1 << oShadowCast);
	//options &= ~(1 << oShadowRecv);

	memset(shader, 0, sizeof(shader));

	char str[255];
	shader[rmNormal] = Core::load<Shader>(stream->readStr(str), options);
	if (options & (1 << oShadowCast))
		shader[rmShadow] = Core::load<Shader>(str, options | (1 << oShadow));
	shader[rmDeferred]	= Core::load<Shader>(str, options | (1 << oDeferred));

	for (int st = sDiffuse; st < sShadow; st++)
		texture[st] = Core::load<Texture>(stream->readStr(str));
}

Material::Material(const char *shaderName, const char *diffuseName) {
	color.diffuse = vec4(1.0f);
	memset(shader, 0, sizeof(shader));
	memset(texture, 0, sizeof(texture));
	shader[rmNormal] = Core::load<Shader>(shaderName);
	texture[sDiffuse] = Core::load<Texture>(diffuseName);
}

Material::Material(const char *shaderName, Texture *color, Texture *normal, Texture *depth) {
	memset(shader, 0, sizeof(shader));
	memset(texture, 0, sizeof(texture));
	shader[rmNormal] = Core::load<Shader>(shaderName);
	texture[sDiffuse] = color;
	texture[sNormal] = normal;
	texture[sDepth] = depth;
};

Material::~Material() {
	for (int rm = 0; rm < rmMAX; rm++)
		Core::unload(shader[rm]);
	for (int st = sDiffuse; st < sShadow; st++)
		Core::unload(texture[st]);
}

bool Material::bind() {
	Shader *sh = shader[Core::mode];
	if (!sh) return false;

	sh->bind();
	sh->setParam(uViewProj, Core::mViewProj);
	sh->setParam(uLightViewProj, Core::lights.mViewProj);
	sh->setParam(uProjRatio, Core::projRatio);
	sh->setParam(uLightDir, Core::lights.direction);
	sh->setParam(uLightPos, Core::lights.pos);
	sh->setParam(uAmbient, Core::lights.ambient);
	sh->setParam(uViewPos, Core::viewPos);
	sh->setParam(uDiffuse, color.diffuse);

//	sh->setParam("mModel", Core::mModel);

	for (int st = sDiffuse; st < sShadow; st++) 
		if (texture[st])
			texture[st]->bind(st);

	return true;
}