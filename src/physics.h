#ifndef H_PHYSICS
#define H_PHYSICS

#include "utils.h"

struct Contact {
	vec3 a, b, n;
};

struct Body {

	enum Type { SPHERE, BOX, CAPSULE, CYLINDER, CONE, CONVEX, TRIANGLE };
	typedef void (*SupportFunc)(const Body &body, const vec3 &dir, vec3 &p);


	Body		*next;
	int			type;
	SupportFunc	supportFunc;
	quat		rot;
	quat		irot;
	vec3		pos;
	vec3		tensor;
	float		mass;
	float		imass;
	float		maxRadius;
	void		*userData;
	bool		sleeping;

	struct Cache {
		quat	rot;
		vec3	pos;
		Cache() : rot(0.0f), pos(0.0f) {}
	} cache;

	struct Velocity {
		vec3	linear;
		vec3	angular;
		vec3	pseudo;
		Velocity() : linear(vec3::ZERO), angular(vec3::ZERO), pseudo(vec3::ZERO) {}
	} velocity;

	struct Drag {
		float	linear;
		float	angular;
		Drag() : linear(0.9f), angular(0.9f) {}
	} drag;

	union {
		struct { vec3 size; };
		struct { float radius, height; };
		struct { int count; vec3 *points; };
	};

	Body();
	Body(Type type, SupportFunc sf, const quat2 &basis, const vec3 &size);
	Body(Type type, SupportFunc sf, const quat2 &basis, float radius, float height);
	Body(Type type, SupportFunc sf, const quat2 &basis, int count, vec3 *points);
	void  init();
	void  setMass(float mass);
	void  setBasis(const quat2 &basis);
	quat2 getBasis() const;
	void  support(const vec3 &dir, vec3 &v) const;
	bool  collide(const Body &body, Contact *contact) const;

	static Body Sphere(const quat2 &basis, float radius);
	static Body Box(const quat2 &basis, const vec3 &size);
	static Body Capsule(const quat2 &basis, float radius, float height);
	static Body Cylinder(const quat2 &basis, float radius, float height);
	static Body Cone(const quat2 &basis, float radius, float height);
	static Body Convex(const quat2 &basis, int count, vec3 *points);
};

struct World {

	struct Constraint {
		Constraint *next;
		vec3 oa, ob, p, n;
		int stamp;

		float perr, effm, lambdaCache;
		vec3 ra, rb, ia, ib;
		
		Constraint(const vec3 &oa, const vec3 &ob, const vec3 &p, const vec3 &n, int stamp);
		~Constraint();
		void init(Body *a, Body *b, float stepTime);
		void solve(Body *a, Body *b, float stepTime, bool pseudo);
	};

	struct Arbiter {
		Arbiter *next;
		Body *a, *b;
		List<Constraint> constraints;

		Arbiter(Body *a, Body *b);
		~Arbiter();
		bool add(const vec3 &pa, const vec3 &pb, const vec3 &n, int stamp);
		void init(float stepTime, int stamp);
		void solve(float stepTime, bool pseudo);
	};

	typedef void (*Callback)(const Body *a, const Body *b, const vec3 &pa, const vec3 &pb, const vec3 &n);

	List<Arbiter> arbiters;

	List<Body>	list;
	vec3		gravity;
	int			stamp;

	World(const vec3 &gravity);
	~World();

	Body* add(Body *body);
	Body* remove(Body *body);
	void simulate(float stepTime, World::Callback callback = NULL);
};


#endif