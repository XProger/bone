#ifndef H_UTILS
#define H_UTILS

#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

#ifdef _DEBUG
	#define debugBreak() _asm { int 3 }
	#define ASSERT(expr) if (expr) {} else { LOG("ASSERT %s in %s:%d\n", #expr, __FILE__, __LINE__); debugBreak(); }
#else
	#define ASSERT(expr)
#endif

#ifndef ANDROID
	#define LOG(...) printf(__VA_ARGS__)
#else
	#include <android/log.h>
	#define LOG(...) __android_log_print(ANDROID_LOG_INFO,"X5",__VA_ARGS__)
#endif

#define EPS		FLT_EPSILON
#define INF		INFINITY
#define PI		3.14159265358979323846f
#define PI2		(PI * 2.0f)
#define DEG2RAD	(PI / 180.0f)
#define RAD2DEG	(180.0f / PI)
#define SQRT2		sqrtf(2.0f)
#define INV_SQRT2	(1.0f/SQRT2)

#define X_AXIS vec3(1, 0, 0)
#define Y_AXIS vec3(0, 1, 0)
#define Z_AXIS vec3(0, 0, 1)

#define fzero(a) (fabsf(a) < EPS)
#define fcmp(a,b) (fabsf(a-b) < EPS)
#define randf() ((float)rand()/RAND_MAX)

typedef char			int8;
typedef	short			int16;
typedef int				int32;
typedef unsigned char	uint8;
typedef unsigned short	uint16;
typedef unsigned int	uint32;


/*
template <typename T>
inline const T& min(const T &a, const T &b) {
	return a < b ? a : b;
}

template <class T>
inline const T& max(const T &a, const T &b) {
	return a > b ? a : b;
}

template <class T>
inline const void swap(const T &a, const T &b) {
	T tmp(a);
	a = b;
	b = tmp;
}
*/

extern float _min(float a, float b);
extern float _max(float a, float b);
extern float sign(float x);
extern float hsign(float x);
extern float clamp(float x, float a, float b);

extern int _min(int a, int b);
extern int _max(int a, int b);
extern int sign(int x);
extern int clamp(int x, int a, int b);
extern float power(float x, float p);

extern float clampAngle(float a);
extern float shortAngle(float a, float b);
extern float lerpAngle(float a, float b, float t);
extern int int_floor(float x);
extern int swap(int x);
extern void swap(int &a, int &b);
extern void swap(float &a, float &b);
extern float lerp(float a, float b, float t);

extern int solve(float a, float b, float *x);
extern int solve(float a, float b, float c, float *x);
extern int solve(float a, float b, float c, float d, float *x);
extern int solve(float a, float b, float c, float d, float e, float *x);
extern int solve(float a, float b, float c, float d, float e, float f, int steps, float epsilon, float *x);

extern uint32 Morton2(uint32 x, uint32 y);
extern uint32 Morton3(uint32 x, uint32 y, uint32 z);

struct ubyte4 {
	uint8 x, y, z, w;
};

struct short2 {
	int16 x, y;
};

struct short4 {
	int16 x, y, z, w;
};

struct vec2 {
	float x, y;

	vec2();
	vec2(float value);
	vec2(float x, float y);

	bool operator == (const vec2 &v) const;
	bool operator != (const vec2 &v) const;

	float& operator [] (int index) const;
	vec2& operator += (const vec2 &v);
	vec2& operator -= (const vec2 &v);
	vec2& operator *= (const vec2 &v);
    vec2& operator /= (const vec2 &v);
	vec2& operator *= (const float s);
	vec2& operator /= (const float s);

	vec2 operator + (const vec2 &v) const;
	vec2 operator - (const vec2 &v) const;
	vec2 operator * (const vec2 &v) const;
	vec2 operator / (const vec2 &v) const;
	vec2 operator * (const float s) const;
	vec2 operator / (const float s) const;
	vec2 operator - () const;

	bool equal(const vec2 &v, float eps) const;
	void normalize();
	vec2 normal() const;
 	vec2 lerp(const vec2 &v, const float t) const;
	vec2 reflect(const vec2 &n) const;
	vec2 refract(const vec2 &n, float f) const;
	vec2 rotate(const float angle) const;
	vec2 min(const vec2 &v) const;
	vec2 max(const vec2 &v) const;

	float length2() const;
	float length() const;
	float cross(const vec2 &v) const;
	float dot(const vec2 &v) const;
	float angle() const { return atan2(y, x); }
};

struct vec3 {
	const static vec3 ZERO;
	const static vec3 HALF;
	const static vec3 ONE;
	const static vec3 X;
	const static vec3 Y;
	const static vec3 Z;

	float x, y, z;

	vec3();
	vec3(float value);
	vec3(float x, float y, float z);
	vec3(const vec2 &xy, float z);

	bool operator == (const vec3 &v) const;
	bool operator != (const vec3 &v) const;

	float& operator [] (int index) const;
	vec3& operator += (const vec3 &v);
	vec3& operator -= (const vec3 &v);
	vec3& operator *= (const vec3 &v);
    vec3& operator /= (const vec3 &v);
	vec3& operator *= (const float s);
	vec3& operator /= (const float s);

	vec3 operator + (const vec3 &v) const;
	vec3 operator - (const vec3 &v) const;
	vec3 operator * (const vec3 &v) const;
	vec3 operator / (const vec3 &v) const;
	vec3 operator * (const float s) const;
	vec3 operator / (const float s) const;
	vec3 operator - () const;

	bool equal(const vec3 &v, float eps) const;
	void normalize();
	vec3 normal() const;
	vec3 cross(const vec3 &v) const;
 	vec3 lerp(const vec3 &v, const float t) const;
	vec3 reflect(const vec3 &n) const;
	vec3 refract(const vec3 &n, float f) const;
	vec3 rotate(const float angle, const vec3 &axis) const;
	vec3 axis() const;
	vec3 min(const vec3 &v) const;
	vec3 max(const vec3 &v) const;

	float length2() const;
	float length() const;
	float dot(const vec3 &v) const;
};

struct vec4 {
	union {
		struct { float x, y, z, w; };
		struct { vec2 xy, zw; };
		struct { vec3 xyz; };
	};

	vec4();
	vec4(float value);
	vec4(float x, float y, float z, float w);
	vec4(const vec2 &xy, vec2 &zw);
	vec4(const vec3 &xyz, float w);

	float& operator [] (int index) const;
};

struct quat {
	const static quat IDENTITY;
	const static quat ZERO;

	union {
		struct { float x, y, z, w; };
		struct { vec2 xy; vec2 zw; };
		struct { vec3 xyz; };
		struct { vec4 xyzw; };
	};

	quat();
	quat(float x, float y, float z, float w);
	quat(const vec3 &axis, float angle);
	quat(const vec3 &angle);
	quat(float lat, float lng, float angle);
	quat(const vec3 &from, const vec3 &to);

	float& operator [] (int index) const;

	bool operator == (const quat &q) const;
	bool operator != (const quat &q) const;

	quat& operator += (const quat &q);
	quat& operator -= (const quat &q);
	quat& operator *= (const float s);

	quat operator - () const;
	quat operator + (const quat &q) const;
	quat operator - (const quat &q) const;
	quat operator * (const float s) const;
	quat operator * (const quat &q) const;
	vec3 operator * (const vec3 &v) const;

	bool equal(const quat &q, float eps) const;
	void normalize();
	quat normal() const;
	quat conjugate() const;
	quat inverse() const;
	quat lerp(const quat &q, const float t) const;
	quat slerp(const quat &q, float t) const;

	float dot(const quat &q) const;
	float length2() const;
	float length() const;
	void  recalc();
};

struct quat2 {
	const static quat2 IDENTITY;

	quat real, dual;

	quat2();
	quat2(const quat &real, const quat &dual);
	quat2(const quat &rot, const vec3 &pos);

	bool operator == (const quat2 &dq) const;
	bool operator != (const quat2 &dq) const;

	quat2 operator * (const quat2 &dq) const;
	vec3  operator * (const vec3 &v) const;
	quat2 inverse() const;
	quat2 lerp(const quat2 &dq, float t) const;

	quat getRot() const;
	void setRot(const quat &rot);
	vec3 getPos() const;
	void setPos(const vec3 &pos);
};

struct mat4 {
	union {
		struct {
			float e00, e10, e20, e30,
				  e01, e11, e21, e31,
				  e02, e12, e22, e32,
				  e03, e13, e23, e33;
		};
		struct { vec4 right, up, dir, offset; };
	};

	mat4();
	mat4(const quat &rot, const vec3 &pos);
	mat4(const quat2 &dq);
	mat4(float l, float r, float b, float t, float znear, float zfar);
	mat4(float FOV, float aspect, float znear, float zfar);
	mat4(const vec3 &from, const vec3 &at, const vec3 &up);
	mat4(float e00, float e10, float e20, float e30,
		 float e01, float e11, float e21, float e31,
		 float e02, float e12, float e22, float e32,
		 float e03, float e13, float e23, float e33);
	mat4(const vec4 &a, const vec4 &b, const vec4 &c, const vec4 &d); 

	vec4& operator [] (int index) const;
	mat4  operator * (const mat4 &m) const;
	vec3  operator * (const vec3 &v) const;
	vec4  operator * (const vec4 &v) const;

	void  identity();
	void  rotateX(float value);
	void  rotateY(float value);
	void  rotateZ(float value);
	void  translate(const vec3 &value);
	void  scale(const vec3 &value);
	float det() const;
	mat4  inverse() const;
	mat4  inverseFast() const;
	mat4  transpose() const;
	quat  getRot() const;
	void  setRot(const quat &rot);
	vec3  getPos() const;
	void  setPos(const vec3 &pos);
	quat2 getBasis();
};

/*
vec3 toEuler(const vec3 &axis, float angle) {
	float	s = sinf(angle),
			c = cosf(angle),
			t = 1 - c,
			d = axis.x * axis.y * t + axis.z * s;

	if (d >  0.998f)
		return vec3( PI * 0.5f,  2.0f * atan2(axis.x * sinf(angle * 0.5f), cosf(angle * 0.5f)), 0.0f);

	if (d < -0.998f)
		return vec3(-PI * 0.5f, -2.0f * atan2(axis.x * sinf(angle * 0.5f), cosf(angle * 0.5f)), 0.0f);

	return vec3(asin(d),
				atan2(axis.y * s - axis.x * axis.z * t, 1.0f - (axis.y * axis.y + axis.z * axis.z) * t),
				atan2(axis.x * s - axis.y * axis.z * t, 1.-f - (axis.x * axis.x + axis.z * axis.z) * t));
};
*/

// 2D primitives
struct Segment2D {
	vec2 a, b;
	Segment2D();
	Segment2D(const vec2 &a, const vec2 &b);

	bool intersect(const Segment2D &line, vec2 &p) const;
};

struct Circle {
	vec2 center;
	float radius;
	Circle();
	Circle(const vec2 &center, float radius);

	bool intersect(const Circle &circle, vec2 &n, float &t) const;
	bool intersect(const Segment2D &segment, vec2 &n, float &t) const;
};

// 3D primitives
struct Segment {
	vec3 a, b;
	Segment();
	Segment(const vec3 &a, const vec3 &b);

	vec3  closestPoint(const vec3 &p, float &t) const;
	float closestPoint(const Segment &segment, vec3 &p1, vec3 &p2) const;
	float dist2(const vec3 &p) const;
	float dist2(const Segment &segment) const;
};

struct Plane {
	vec3	n;
	float	d;
	Plane();
	Plane(float x, float y, float z, float w);
	Plane(const vec3 &n, const float d);
	Plane(const vec3 &a, const vec3 &b, const vec3 &c);

	float dist(const vec3 &p) const;
	vec3  closestPoint(const vec3 &p) const;
	bool  intersect(const vec3 &rayPos, const vec3 &rayDir, float &t) const;
	bool  intersect(const Segment &segment, float &t) const;
	bool  intersect(const Plane &plane, vec3 &rayPos, vec3 &rayDir) const;
};

struct Triangle {
	vec3 a, b, c;
	Triangle();
	Triangle(const vec3 &a, const vec3 &b, const vec3 &c);

	vec3& operator [] (int index) const;

	float area() const;
	vec3  normal() const;
	vec3  barycentric(const vec3 &p) const;
	vec3  closestPoint(const vec3 &p) const;
	float closestPoint(const Segment &segment, vec3 &p1, vec3 &p2) const;
	bool  intersect(const vec3 &p) const;
	bool  intersect(const vec3 &rayPos, const vec3 &rayDir, bool twoSide, float &u, float &v, float &t) const;
};

struct Sphere {
	vec3	center;
	float	radius;
	Sphere();
	Sphere(const vec3 &center, float radius);
	Sphere(float radius);

	bool intersect(const vec3 &p) const;
	bool intersect(const vec3 &rayPos, const vec3 &rayDir, float &tMin, float &tMax) const;
	bool intersect(const Plane &plane, const vec3 &vel, vec3 &p, float &t) const;
	bool intersect(const Triangle &triangle, vec3 &n, float &t) const;
	bool intersect(const Sphere &sphere) const;
	bool intersect(const Sphere &sphere, vec3 &n, float &t) const;
	bool intersect(const Sphere &sphere, const vec3 &vel, float &t) const;
};

struct Box {
	vec3 min, max;
	Box();
	Box(const vec3 &min, const vec3 &max);
	Box(const vec3 &size);

	float& operator [] (int index);

	vec3  getCenter() const;
	vec3  closestPoint(const vec3 &p) const;
//	float closestPoint(const Segment &segment, vec3 &cp1, vec3 &cp2) const;
	float dist2(const vec3 &p) const;
	bool  intersect(const vec3 &p) const;
	bool  intersect(const Box &box) const;
	bool  intersect(const Sphere &sphere, vec3 &n, float &t) const;
	bool  intersect(const quat2 &basis, const Sphere &sphere, vec3 &n, float &t) const;
	bool  intersect(const vec3 &rayPos, const vec3 &rayDir, float &t) const;
};

namespace String {
	extern bool  equal(const char *str1, const char *str2);
	extern int   length(const char *str);
	extern char* copy(const char *str);
}

struct Stream {
	static int	fCount;
	static FILE	*f;
	static struct FileItem {
		int		size, csize, offset;
		char	name[64];
	} *table;

	static void init(const char *pack, int offset);
	static void free();

	char			*name;
	unsigned char	*buffer;
	int				pos, size;

	Stream(const char *name);
	~Stream();
	void	seek(int offset);
	int		read(void *data, int count);
	char	*readStr(char *buffer);
	char	*readStr();
	quat2	readQuat2();
	vec4	readColor();

	template <typename T>
	T read() {
		T x;
		read(&x, sizeof(x));
		return x;
	}

	template <typename T>
	T read(T &x) {
		read(&x, sizeof(x));
		return x;
	}

	template <typename T>
	T* readArray(int count) {
		if (!count)
			return NULL;
		T *array = new T[count];
		read(array, count * sizeof(T));
		return array;
	}

	template <typename A, typename B>
	A* readArray(A *&array, B &count) {
		array = new A[stream->read(count)];
		read(array, count * sizeof(A));
		return array;
	}
};

template <typename T>
struct List {
	T *first;
	int count;
	List() : first(NULL), count(0) {}

	T* add(T *item) {
		count++;
		item->next = first;
		return first = item;
	}

	T* remove(T *item) {
		T *obj = first, *prev = NULL;
		while (obj) {
			if (obj == item) {
				if (prev)
					prev->next = item->next;
				else
					first = item->next;
				item->next = NULL;
				count--;
				break;
			}
			prev = obj;
			obj  = obj->next;
		}
 		return item;
	}
};

template <typename T, typename C = int>
struct Array {
	T	**items;
	C	count;

	Array() : items(NULL), count(0) {}

	Array(int count) : count(count) {
		if (count == 0) {
			items = NULL;
			return;
		}
		items = new T*[count];
		for (int i = 0; i < count; i++)
			items[i] = new T();
	}

	Array(Stream *stream) {
		stream->read(count);
		if (count == 0) {
			items = NULL;
			return;
		}
		items = new T*[count];
		for (int i = 0; i < count; i++)
			items[i] = new T(stream);
	}

	~Array() {
		for (int i = 0; i < count; i++)
			delete items[i];
		delete[] items;
	}

	T* operator[] (int index) const { 
		return items[index];
	}
};

template <typename T, typename C = int>
struct Vector {
	T	*items;
	C	count;

	Vector(Stream *stream) {
		stream->read(count);
		if (count <= 0) {
			items = NULL;
			return;
		}
		items = (T*)malloc((int)count * sizeof(T));
		stream->read(items, (int)count * sizeof(T));
	}

	Vector(Stream *stream, int count) : count(count) {
		if (count <= 0) {
			items = NULL;
			return;
		}
		items = (T*)malloc((int)count * sizeof(T));
		stream->read(items, (int)count * sizeof(T));
	}


	~Vector() {
		if (items) free(items);
	}

	T& operator[] (int index) const {
		ASSERT(index >= 0);
		ASSERT(index < count);
		return items[index];
	}
};


struct Curve {
	int		count;
	vec3	*points;

	Curve();
	Curve(Stream *stream);
	~Curve();

	vec3 eval(int index, float t);
	vec3 eval(float t);

	vec3 evalTangent(int index, float t);
	vec3 evalTangent(float t);

	float closest(int index, const vec3 &p);
	float closest(const vec3 &p);
	float offset(float t, float dist);
};


/*
typedef unsigned char	uint8;
typedef unsigned int	uint32;

static uint32 FNV1aHash (char *buf) {	
	uint32 hval = 0x811c9dc5;
	while (*buf) {
		hval ^= (uint32)*buf++;
		hval *= 0x01000193;
	}
	return hval;
}

static uint32 MurmurHash3(const uint8 *data, int len, uint32 seed) {
	const int nblocks = len / 4;

	uint32 h1 = seed;

	const uint32 c1 = 0xcc9e2d51;
	const uint32 c2 = 0x1b873593;

	const uint32 *blocks = (const uint32*)(data + nblocks*4);

	for (int i = -nblocks; i; i++) {
		uint32 k1 = blocks[i];
		k1 *= c1;
		k1 = _rotl(k1, 15);
		k1 *= c2;    
		h1 ^= k1;
		h1 = _rotl(h1,13); 
		h1 = h1 * 5 + 0xe6546b64;
	}

	const uint8 *tail = (const uint8*)(data + nblocks*4);

	uint32 k1 = 0;

	switch (len & 3) {
		case 3: k1 ^= tail[2] << 16;
		case 2: k1 ^= tail[1] << 8;
		case 1: k1 ^= tail[0];
				k1 *= c1; 
				k1 = _rotl(k1, 15); 
				k1 *= c2; 
				h1 ^= k1;
	};

	h1 ^= len;
	h1 ^= h1 >> 16;
	h1 *= 0x85ebca6b;
	h1 ^= h1 >> 13;
	h1 *= 0xc2b2ae35;
	h1 ^= h1 >> 16;
	return h1;
}

struct HashSet {
	struct Entry {
		uint32	hash;
		int		index;
		Entry	*next;
		Entry(int hash, int index, Entry *next) : hash(hash), index(index), next(next) {};
		~Entry() {
			delete next;
		}
	} **entry;

	int capacity;

	HashSet(int capacity) : capacity(capacity) {
		entry = new Entry*[capacity];
		memset(entry, 0, sizeof(entry[0]) * capacity);
	}

	~HashSet() {
		for (int i = 0; i < capacity; i++)
			delete entry[i];
		delete entry;
	}

	Entry* get(Entry *prev, uint32 hash) {		
		for (Entry *e = prev ? prev->next : entry[hash % capacity]; e; e = e->next)
			if (hash == e->hash)
				return e;
		return NULL;
	}

	void insert(uint32 hash, int index = -1) {
		int i = hash % capacity;		
		entry[i] = new Entry(hash, index, entry[i]);
	}

	void clear() {
		for (int i = 0; i < capacity; i++)
			delete entry[i];
		memset(entry, 0, sizeof(entry[0]) * capacity);
	}
};

template <typename T>
struct Array {
private:
	uint32 getHash(const T &item) {
		return MurmurHash3((uint8*)&item, sizeof(item), 666);
	}
	HashSet *set;

	int	capacity;
public:
	T	*items;
	int	count;

	Array(int hashMapSize = 0) : items(NULL), count(0), capacity(0), set(NULL) {
		if (hashMapSize)
			set = new HashSet(hashMapSize);
	}

	virtual ~Array() { 
		delete set;
		delete items;
	}

	virtual int add(const T &item) {		
		if (set) {
			HashSet::Entry *e = NULL;
			uint32 hash = getHash(item);
			while (e = set->get(e, hash))
				if (!memcmp(&items[e->index], &item, sizeof(item)))
					return e->index;				
			set->insert(hash, count);
		}

		if (count == capacity) {
			capacity += 1024;
			if (items)
				items = (T*)realloc(items, capacity * sizeof(item));
			else
				items = (T*)malloc(capacity * sizeof(item));
		}
		items[count] = item;
		return count++;
	}

	void clear() {
		count = 0;
		if (set) set->clear();
	}

	T& operator[] (int idx) {
		return items[idx]; 
	};
};
*/

#endif