#include <jni.h>
#include <android/log.h>
#include <sys/time.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "core.h"
#include "game.h"

int getTime() {
	timeval time;
	gettimeofday(&time, NULL);
	return (time.tv_sec * 1000) + (time.tv_usec / 1000);
}

extern "C" {

int lastTime, fpsTime, FPS;

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeInit(JNIEnv* env, jobject obj, jstring packName, jint packOffset) {
	const char* pack = env->GetStringUTFChars(packName, NULL);
	Stream::init(pack, packOffset);
	env->ReleaseStringUTFChars(packName, pack);
	Game::init();

	lastTime    = getTime();
	fpsTime     = lastTime + 1000;
	FPS         = 0;
#ifdef ANDROID
		LOG("init");
#endif
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeFree(JNIEnv* env) {
	Game::free();
	Stream::free();
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeReset(JNIEnv* env) {
//	core->reset();
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeUpdate(JNIEnv* env) {
    int time = getTime();
    if (time == lastTime)
        return;

    Core::deltaTime = (time - lastTime) / 1000.0f;
    lastTime = time;

    Game::update();
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeRender(JNIEnv* env) {
	Game::render();
	FPS++;
    int time = getTime();
    if (time > fpsTime) {
        fpsTime = time + 1000;
		#ifdef ANDROID
				LOG("FPS: %d\n", FPS);
		#endif
        FPS = 0;
    }
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeResize(JNIEnv* env, jobject obj, jint w, jint h) {
    Core::width  = w;
    Core::height = h;
}

float DeadZone(float x) {
	return x = fabsf(x) < 0.2f ? 0.0f : x;
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeTouch(JNIEnv* env, jobject obj, jint id, jint state, jfloat x, jfloat y) {
	if (id > 1) return;

	if (state < 0) {
		state = -state;
		Input::Joystick &joy = Input::joy;

		switch (state) {
			case 3 :
				joy.L.x = DeadZone(x);
				joy.L.y = DeadZone(y);
				break;
			case 4 :
				joy.R.x = DeadZone(x);
				joy.R.y = DeadZone(y);
				break;
			default:
				joy.down[(int)x] = state != 1;
		}
		return;
	}

	if (state == 3 && x < Core::width / 2) {
		vec2 center(Core::width * 0.25f, Core::height * 0.6f);
		vec2 pos(x, y);
		vec2 d = pos - center;

		Input::Joystick &joy = Input::joy;

		joy.L.x = DeadZone(d.x);
		joy.L.y = DeadZone(d.y);
		if (joy.L != vec2(0.0f))
			joy.L.normalize();
	}

	if (state == 2 && x > Core::width / 2)
		Input::joy.down[(y > Core::height / 2) ? 1 : 4] = true;

	if (state == 1) {
		Input::joy.L = vec2(0.0f);
		Input::joy.down[1] = false;
		Input::joy.down[4] = false;
	}
//	game->menu->touchInput(state, x, y);
}

JNIEXPORT void JNICALL Java_org_mentalx_x5_Wrapper_nativeSoundFill(JNIEnv* env, jobject obj, jshortArray buffer) {
	return;
	jshort *frames = env->GetShortArrayElements(buffer, NULL);
	jsize count = env->GetArrayLength(buffer) / 2;
//	sound->render((SoundFrame*)frames, count);
	env->ReleaseShortArrayElements(buffer, frames, 0);
}

}


