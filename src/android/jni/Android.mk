LOCAL_PATH := $(call my-dir)
SRC := ${LOCAL_PATH}/../../../

include $(CLEAR_VARS)

#-ffunction-sections -fdata-sections #-frtti #-fexceptions #

LOCAL_CFLAGS		:= -DANDROID -fvisibility=hidden
LOCAL_LDFLAGS		:= -Wl,--gc-sections
LOCAL_MODULE		:= game
LOCAL_SRC_FILES		:= main.cpp\
						$(SRC)minilzo/minilzo.c\
						$(SRC)core.cpp\
						$(SRC)enemy.cpp\
						$(SRC)game.cpp\
						$(SRC)input.cpp\
						$(SRC)level.cpp\
						$(SRC)math.cpp\
						$(SRC)material.cpp\
						$(SRC)mesh.cpp\
						$(SRC)model.cpp\
						$(SRC)shader.cpp\
						$(SRC)stream.cpp\
						$(SRC)texture.cpp\
						$(SRC)utils.cpp
LOCAL_C_INCLUDES	:= $(LOCAL_PATH)/../../
LOCAL_LDLIBS		:= -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)