#ifndef H_DEBUG
#define H_DEBUG

#include "utils.h"
#include "core.h"

namespace Debug {

	namespace Draw {
		extern void begin();
		extern void end();
		extern void box(const Box &box);
		extern void sphere(const Sphere &sphere);
		extern void mesh(vec3 *vertices, Index *indices, int iCount);
		extern void curve(Curve *curve);
		extern void axes(float size);
		extern void point(const vec3 &p, const vec3 &color);
		extern void test1();
	}
};

#endif