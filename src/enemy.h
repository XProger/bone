#ifndef H_ENEMY
#define H_ENEMY

#include "utils.h"
#include "core.h"
#include "level.h"

struct Enemy : Level::Entity {

	enum State { STATE_IDLE1, STATE_IDLE2, STATE_WALK, STATE_JUMP, STATE_ATTACK1, STATE_ATTACK2, STATE_WALK_ATTACK1, STATE_WALK_ATTACK2, STATE_ACTION, STATE_DAMAGE, STATE_DEAD } state;
	enum {	DIR_U = 0x1, DIR_D = 0x2, DIR_L = 0x4, DIR_R = 0x8, DIR_JOY = 0x10, DIR = ( DIR_U | DIR_D | DIR_L | DIR_R | DIR_JOY ),
			JUMP = 0x20, 
			ATTACK_1 = 0x40, ATTACK_2 = 0x80, ATTACK = (ATTACK_1 | ATTACK_2), 
			ACTION = 0x100 };

	Model			*model;
	Enemy			*target;
	NavMesh::Path	*path;

	int		input;
	float	inputAngle;
	vec3	inputDir, inputDirLast;

	float	angle;
	int		health;
	float	idleTimer;
	float	hitTimer;


	Enemy(const char *name);
	virtual ~Enemy();

	virtual void spawn(const vec3 &pos);
	virtual void hit(int damage);
	virtual void updateInput();
	virtual void update();
	virtual void render();
};

struct Player : Enemy {
	Model *weapon;

	Player(const char *name, const char *weaponName);
	~Player();
	virtual void updateInput();
	virtual void update();
	virtual void render();
};

struct Tank : Enemy {
	Tank(const char *name);
	virtual void updateInput();
	virtual void update();
};

#endif