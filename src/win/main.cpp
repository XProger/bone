#ifdef _DEBUG
	#include "crtdbg.h"
#endif

#include <stdio.h>
#include <windows.h>

#include "core.h"
//#include "game.h"
#include "game_tr.h"

// Timer
DWORD getTime() {
#ifdef MINIMAL
	return timeGetTime();
#else
	LARGE_INTEGER Freq, Count;
	QueryPerformanceFrequency(&Freq);
	QueryPerformanceCounter(&Count);
	return (DWORD)(Count.QuadPart * 1000L / Freq.QuadPart);
#endif
}

// Joystick
#define JOY_DEAD_ZONE 0.2f
DWORD joyCount;

void joyEnum() {
	JOYINFOEX info;
	info.dwSize = sizeof(info);
	info.dwFlags = JOY_RETURNALL;
	joyCount = joyGetPosEx(0, &info) == JOYERR_NOERROR ? 1 : 0;
}

float joyAxis(int x, int xMin, int xMax) {
	return ((x - xMin) / (float)(xMax - xMin)) * 2.0f - 1.0f;
}

vec2 joyDir(float ax, float ay) {
	vec2 dir = vec2(ax, ay);
	float dist = _min(1.0f, dir.length());
	if (dist < JOY_DEAD_ZONE) dist = 0;

	return dir.normal() * dist;
}

void joyUpdate() {
	memset(&Input::joy, 0, sizeof(Input::joy));
	if (!joyCount)
		return;

	JOYINFOEX info;
	info.dwSize = sizeof(info);
	info.dwFlags = JOY_RETURNALL;

	if (joyGetPosEx(0, &info) == JOYERR_NOERROR) {
		Input::Joystick &joy = Input::joy;
	
		JOYCAPS caps;
		joyGetDevCaps(0, &caps, sizeof(caps));

		joy.L = joyDir(joyAxis(info.dwXpos, caps.wXmin, caps.wXmax), 
					   joyAxis(info.dwYpos, caps.wYmin, caps.wYmax));

		if (caps.wCaps & JOYCAPS_HASR) {
			float rx = -1.0f;

		//	if (caps.wCaps & JOYCAPS_HASU) 
		//		rx = joyAxis(info.dwUpos, caps.wUmin, caps.wUmax);
		//	else
				if (caps.wCaps & JOYCAPS_HASZ)
					rx = joyAxis(info.dwZpos, caps.wZmin, caps.wZmax);
	
			if (rx > -1.0f)
				joy.R = joyDir(rx, joyAxis(info.dwRpos, caps.wRmin, caps.wRmax));
		}

		for (int i = 0; i < 16; i++)
			joy.down[i] = (info.dwButtons & (1 << i)) > 0;
	}
}


// OpenGL
HGLRC initGL(HDC hDC) {
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));
	pfd.nSize		= sizeof(pfd);
	pfd.nVersion	= 1;
	pfd.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_SWAP_EXCHANGE;
	pfd.cColorBits	= 32;
	pfd.cDepthBits	= 24;

	int format = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, format, &pfd);
	HGLRC hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);
	return hRC;
}

void freeGL(HGLRC hRC) {
	wglMakeCurrent(0, 0);
	wglDeleteContext(hRC);
}

// Window
static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		case WM_DEVICECHANGE :
			joyEnum();
			return 1; 
		case WM_KEYDOWN :
		case WM_KEYUP   :
			Input::setDown(wParam, msg == WM_KEYDOWN);
			break;
		case WM_LBUTTONDOWN   :
		case WM_LBUTTONUP     :
		case WM_LBUTTONDBLCLK :
		case WM_RBUTTONDOWN   :
		case WM_RBUTTONUP     :
		case WM_RBUTTONDBLCLK :
		case WM_MBUTTONDOWN   :
		case WM_MBUTTONUP     :
		case WM_MBUTTONDBLCLK : {
			int idx = 
				(msg == WM_LBUTTONDOWN || msg == WM_LBUTTONUP || msg == WM_LBUTTONDBLCLK) ? 0 :
				(msg == WM_RBUTTONDOWN || msg == WM_RBUTTONUP || msg == WM_RBUTTONDBLCLK) ? 1 : 2;	
			Input::mouse.pos = vec2((float)(short)LOWORD(lParam), (float)(short)HIWORD(lParam));
			bool down = msg != WM_LBUTTONUP && msg != WM_RBUTTONUP && msg != WM_MBUTTONUP;
			Input::setMouseDown(idx, down);
			if (down)
				SetCapture(hWnd);
			else
				ReleaseCapture();
			break;
		}
		case WM_MOUSEMOVE :
			Input::setMouseMove(vec2((float)(short)LOWORD(lParam), (float)(short)HIWORD(lParam)));
			break;
		case WM_SIZE :
			Core::width  = LOWORD(lParam);
			Core::height = HIWORD(lParam);
			break;
		case WM_DESTROY :
			PostQuitMessage(0);
			break;
		default :
			return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0;
}

int main() {
#ifdef _DEBUG
	_CrtMemState _ms;
	_CrtMemCheckpoint(&_ms);
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
#endif

#ifdef MINIMAL
	timeBeginPeriod(0);
#endif
	RECT rect = { 0, 0, 1280, 720 };
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false); 

	HWND hWnd = CreateWindow("static", "X5 Engine", WS_OVERLAPPEDWINDOW, 0, 0, rect.right - rect.left, rect.bottom - rect.top, 0, 0, 0, 0);
	HDC hDC = GetDC(hWnd);
	HGLRC hRC = initGL(hDC);

	joyCount = 0;
	joyEnum();

	Stream::init("data.xpk", 0);
	Game::init();

	SetWindowLong(hWnd, GWL_WNDPROC, (LONG)&WndProc);
	ShowWindow(hWnd, SW_SHOWDEFAULT);

	DWORD lastTime = getTime();
	DWORD fpsTime = lastTime + 1000;
	int	  FPS = 0;

	MSG msg;
	msg.message = WM_PAINT;
	while (msg.message != WM_QUIT) 
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			DWORD time = getTime();

			if (time - lastTime == 0)
				continue;

			Core::deltaTime = (time - lastTime) / 1000.0f;
			lastTime = time;

			//if (Core::deltaTime > 0.5f)
			//	continue;

			//Core::deltaTime  *= 0.05f;

			if (time > fpsTime) {
				fpsTime = time + 1000;
				LOG("FPS: %d\n", FPS);
				FPS = 0;
			}

			joyUpdate();
			Game::update();
			Game::render();
			SwapBuffers(hDC);
			//Sleep(100);

			FPS++;
		}

	Game::free();
	Stream::free();

	freeGL(hRC);
	ReleaseDC(hWnd, hDC);
	DestroyWindow(hWnd);

#ifdef _DEBUG
	_CrtMemDumpAllObjectsSince(&_ms);
	system("pause");
#endif

	return 0;
};