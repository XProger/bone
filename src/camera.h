#ifndef H_CAMERA
#define H_CAMERA

#include "core.h"

struct Camera {
	enum { TYPE_STATIC = 1, TYPE_TARGET = 2, TYPE_FOLLOW = 4, TYPE_CURVE = 8, TYPE_FREE = 16 };

	int		type;
	quat2	basis, target, basisLast, basisBase;
	float	fov, fovLast;
	float	znear, zfar;
	float	t;

	vec3	angle;
	vec3	pos;

	Curve	*curve;
	float	time, targetTime;

	mat4	mView, mProj;

	void	setCamera(int type, const quat2 &basis, float fov, float znear, float zfar, float t, Curve *curve = NULL);
	void	update();
	void	setup();
};

#endif